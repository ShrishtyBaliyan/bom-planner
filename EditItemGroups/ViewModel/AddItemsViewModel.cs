﻿using CadData;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Input;

namespace EditItemGroup.ViewModel
{
    public class AddItemsViewModel : INotifyPropertyChanged, ICloseWindow
    {

        public List<string> cmbParts;
        public List<string> CmbParts
        {
            get => SQLConnection.Item_Group.Split(',').ToList();
            set
            {
                this.cmbParts = value;

                OnPropertyChanged("CmbTypes");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private string selectedPart;
        public string SelectedPart
        {
            get => this.selectedPart;
            set
            {
                this.selectedPart = value;
                OnPropertyChanged("SelectedPart");
            }
        }


        /// <summary>
        /// 
        /// </summary>
        public string txtPart;
        public string TxtPart
        {
            get => this.txtPart;
            set
            {
                this.txtPart = value;

                OnPropertyChanged("TxtPart");
            }
        }



        /// <summary>
        /// 
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string PropertyName)
        {
            if (this.PropertyChanged == null)
                return;
            this.PropertyChanged((object)this, new PropertyChangedEventArgs(PropertyName));
        }


        /// <summary>
        /// Add PartTypes
        /// </summary>
        private ICommand addCommand;
        public ICommand AddCommand
        {
            get
            {
                if (this.addCommand == null)
                    this.addCommand = (ICommand)new ICommandInterface(new Action<object>(this.executeAdd), new Func<object, bool>(this.canExecuteAdd));
                return this.addCommand;
            }
        }

        private bool canExecuteAdd(object arg) => true;

        private void executeAdd(object parameter)
        {
            if (!String.IsNullOrWhiteSpace(this.txtPart))
            {
                CheckExistence(this.txtPart);

                CloseWindow();
            }
            else
            {
                MessageBox.Show("Missing Part Type Details");
            }
            // this.TxtPart = "";
        }



        /// <summary>
        /// Remove PartType
        /// </summary>
        private ICommand removeCommand;
        public ICommand RemoveCommand
        {
            get
            {
                if (this.removeCommand == null)
                    this.removeCommand = (ICommand)new ICommandInterface(new Action<object>(this.executeRemove), new Func<object, bool>(this.canExecuteRemove));
                return this.removeCommand;
            }
        }

        private bool canExecuteRemove(object arg) => true;

        private void executeRemove(object parameter)
        {
            if (String.IsNullOrWhiteSpace(this.selectedPart))
            {
                MessageBox.Show("Please Select a Item Group!");
            }
            else
            {
                RemoveExistingPart(this.selectedPart);
                MessageBox.Show("Item Group Removed Successfully!!!");
                CloseWindow();
            }
        }


        /// <summary>
        /// Close Window
        /// </summary>
        private ICommand cancelCommand;
        public ICommand CancelCommand
        {
            get
            {
                if (this.cancelCommand == null)
                    this.cancelCommand = (ICommand)new ICommandInterface(new Action<object>(this.executeCancel), new Func<object, bool>(this.canExecuteCancel));
                return this.cancelCommand;
            }
        }

        private bool canExecuteCancel(object arg) => true;

        private void executeCancel(object parameter)
        {
            this.txtPart = null;
            CloseWindow();
        }


        /// <summary>
        /// 
        /// </summary>
        void CloseWindow()
        {
            Close?.Invoke();
        }



        /// <summary>
        /// 
        /// </summary>
        /// <param name="partToAdd"></param>
        public void CheckExistence(string partToAdd)
        {
            var Parts = SQLConnection.Item_Group.Split(',').ToList<string>();

            if (!Parts.Contains(partToAdd))
            {
                SQLConnection write = new SQLConnection();
                write.SQLConnectionAddItem(partToAdd, true);
                MessageBox.Show("Item Group Added Successfully!!!");
                return;
            }

            MessageBox.Show("Item Group Already Exists!");

        }



        /// <summary>
        /// Function to remove parts
        /// </summary>
        /// <param name="partToAdd"></param>
        public void RemoveExistingPart(string partToRemove)
        {
            var Parts = SQLConnection.Item_Group.Split(',').ToList<string>();
            Parts.Remove(partToRemove);

            //for converting list to comma separated string
            var Partlist = ReadListItem(Parts);

            SQLConnection write = new SQLConnection();
            write.SQLConnectionItem(Partlist, true);
        }



        /// <summary>
        /// convert list to comma separated string
        /// </summary>
        /// <param name="prtLst"></param>
        /// <returns></returns>
        public string ReadListItem(List<string> prtLst)
        {
            string str = string.Empty;
            foreach (var item in prtLst)
                str = str + item + ",";

            str = str.Remove(str.Length - 1);
            return str;
        }


        /// <summary>
        /// 
        /// </summary>
        public Action Close { get; set; }
    }

}
