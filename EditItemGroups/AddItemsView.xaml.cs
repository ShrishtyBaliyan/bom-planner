﻿using CadData;
using System.Windows;

namespace EditItemGroup
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class AddItemsView : Window
    {
        public AddItemsView()
        {
            InitializeComponent();
            Loaded += TypeView_Loaded;
        }
        private void TypeView_Loaded(object sender, RoutedEventArgs e)
        {
            if (DataContext is ICloseWindow vm)
            {
                vm.Close += () =>
                {
                    this.Close();
                };
            }
        }
    }
}
