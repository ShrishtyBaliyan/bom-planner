﻿using CadData;
using System.Windows;

namespace ReleaseBOM
{
    /// <summary>
    /// Interaction logic for DesignView.xaml
    /// </summary>
    public partial class ReleaseView : Window
    {
        public ReleaseView()
        {
            InitializeComponent();
            Loaded += Design_Loaded;
        }

        private void Design_Loaded(object sender, RoutedEventArgs e)
        {
            if (DataContext is ICloseWindow vm)
            {
                vm.Close += () =>
                {
                    this.Close();
                };
            }
        }

    }
}
