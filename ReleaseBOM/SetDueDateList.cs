﻿using CadData;
using ReleaseBOM.Sql;
using ReleaseBOM.ViewModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Windows;

namespace ReleaseBOM
{
    /// <summary>
    /// 
    /// </summary>
    public class SetDueDateList
    {
        [DisplayName("S No")]
        public string SNO { get; set; }

        [DisplayName("Part Number")]
        public string PartNumber { get; set; } = string.Empty;

        [DisplayName("Item Name")]
        public string ItemName { get; set; } = string.Empty;

        [DisplayName("Unit")]
        public string UnitQty { get; set; }

        [DisplayName("Set Due Date")]
        public string SetDueDate { get; set; } = string.Empty;

        [DisplayName("Assign To")]
        public string AssignTo { get; set; } = string.Empty;
    }



    /// <summary>
    /// 
    /// </summary>
    public class DueDate
    {
        List<SetDueDateList> info = new List<SetDueDateList>();
        List<SetDueDateList> DataList = new List<SetDueDateList>();
        AssemblyInfo baseInfo;
        string duedate;
        string assignTo;

        public void SaveDueDate(AssemblyInfo baseInfo)
        {
            this.baseInfo = baseInfo;
            UpdateDataGrid(info, "1", baseInfo);
            if (!UpdateDGList(info, DataList))
            {
                return;
            }


            //passing Assembly details as a list to the view!!
            SetDueDateViewModel DueDateViewModel = new SetDueDateViewModel(this.baseInfo, DataList);
            var dueDateView = new SetDueDate();
            dueDateView.DataContext = DueDateViewModel;
            //show Preview
            dueDateView.ShowDialog();

        }


        /// <summary>
        /// for update the datagrid values 
        /// </summary>
        /// <param name="info"></param>
        /// <param name="sno"></param>
        /// <param name="asmInfo"></param>
        /// <param name="prtType"></param>
        /// <param name="isModel"></param>
        private void UpdateDataGrid(List<SetDueDateList> info, string sno, AssemblyInfo asmInfo)
        {
            if (!asmInfo.childs.Any())
            {
                return;
            }

            var desc = asmInfo.PartNum;
            info.Add(new SetDueDateList()
            {
                SNO = "",
                PartNumber = desc,
                ItemName = asmInfo.ItemName,
                UnitQty = asmInfo.qty.ToString(),
                SetDueDate = duedate,
                AssignTo = assignTo
            });

            sno = "1";
            if (asmInfo.topLevelAsm.Any())
            {
                FillDataGrid(info, sno, asmInfo.topLevelAsm, asmInfo.PartNum);

            }
            FillDataGrid(info, sno, asmInfo.childs, asmInfo.PartNum);
        }



        /// <summary>
        /// for fill the datagrid values
        /// </summary>
        /// <param name="info"></param>
        /// <param name="sno"></param>
        /// <param name="childs"></param>
        /// <param name="prtType"></param>
        /// <param name="isPriority"></param>
        void FillDataGrid(List<SetDueDateList> info, string sno, List<AssemblyInfo> childs, string parent)
        {
            foreach (var child in childs)
            {
                if (child.ItemGroup.Equals("BOP", StringComparison.InvariantCultureIgnoreCase) || child.ItemGroup.Equals("Hardware", StringComparison.InvariantCultureIgnoreCase))
                {
                    continue;
                }

                info.Add(new SetDueDateList()
                {
                    SNO = sno,
                    PartNumber = child.PartNum,
                    ItemName = child.ItemName,
                    UnitQty = child.qty.ToString(),
                    SetDueDate = duedate,
                    AssignTo = assignTo
                });

                var Sno = Convert.ToInt32(sno);
                Sno++;
                sno = Sno.ToString();
            }
            info.Add(new SetDueDateList());
            foreach (var child in childs)
            {
                UpdateDataGrid(info, sno, child);
            }
        }



        /// <summary>
        /// update the values in datagrid list according to the values that are stored in database
        /// </summary>
        /// <param name="info"></param>
        /// <param name="DataList"></param>
        private bool UpdateDGList(List<SetDueDateList> info, List<SetDueDateList> DataList)
        {
            var parentName = string.Empty;
            foreach (var item in info)
            {
                if (String.IsNullOrWhiteSpace(item.SNO) && (!item.PartNumber.Equals(baseInfo.PartNum, StringComparison.InvariantCultureIgnoreCase)))
                {
                    parentName = item.PartNumber;
                    DataList.Add(new SetDueDateList()
                    {
                        SNO = item.SNO,
                        PartNumber = item.PartNumber,
                        ItemName = item.ItemName,
                        UnitQty = item.UnitQty,
                        SetDueDate = "",
                        AssignTo = ""
                    });
                    continue;
                }
                else if (String.IsNullOrWhiteSpace(item.SNO) && (item.PartNumber.Equals(baseInfo.PartNum, StringComparison.InvariantCultureIgnoreCase)))
                {
                    parentName = item.PartNumber;
                }
                else if (String.IsNullOrWhiteSpace(item.SNO) && String.IsNullOrWhiteSpace(item.PartNumber))
                {
                    DataList.Add(new SetDueDateList()
                    {
                        SNO = item.SNO,
                        PartNumber = item.PartNumber,
                        ItemName = item.ItemName,
                        UnitQty = item.UnitQty,
                        SetDueDate = item.SetDueDate,
                        AssignTo = item.AssignTo
                    });
                }
                var Asm_Child_id = ReleaseBOMSqlQueries.GetAsmChildId(parentName, item.PartNumber);
                if (Asm_Child_id < 0)
                {
                    MessageBox.Show("Please first save Assembly!!!");
                    return false;
                }

                FillFinalList(Asm_Child_id, DataList, item);
            }
            return true;
        }



        /// <summary>
        /// fill the values in datagrid list according to the values that are stored in database
        /// </summary>
        /// <param name="ReleaseBOMid"></param>
        /// <param name="DataList"></param>
        /// <param name="item"></param>
        public void FillFinalList(int Asm_Child_id, List<SetDueDateList> DataList, SetDueDateList item)
        {
            DataTable ReleaseBOM = ReleaseBOMSqlQueries.GetDueDate(Asm_Child_id);

            if (ReleaseBOM != null)
            {
                duedate = ReleaseBOM.Rows[0][0].ToString();
                assignTo = ReleaseBOM.Rows[0][1].ToString();
            }

            DataList.Add(new SetDueDateList()
            {
                SNO = item.SNO,
                PartNumber = item.PartNumber,
                ItemName = item.ItemName,
                UnitQty = item.UnitQty,
                SetDueDate = duedate,
                AssignTo = assignTo
            });

        }

    }
}
