﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReleaseBOM
{
    public class ReleaseModel
    {
        [DisplayName("S No")]
        public string SNO { get; set; }

        [DisplayName("Part Number")]
        public string PartNumber { get; set; } = string.Empty;

        [DisplayName("Item Name")]
        public string ItemName { get; set; } = string.Empty;

        [DisplayName("Unit")]
        public string UnitQty { get; set; }

        [DisplayName("Required Quantity")]
        public string RequiredQty { get; set; } = string.Empty;

        [DisplayName("Total Quantity")]
        public string TotalQty { get; set; }

        [DisplayName("Release Quantity")]
        public string ReleaseQty { get; set; } = string.Empty;

        [DisplayName("Release Date")]
        public string ReleaseDate { get; set; } = string.Empty;

        [DisplayName("Release By")]
        public string ReleaseBy { get; set; } = string.Empty;

        [DisplayName("Total Released Quantity")]
        public string TotalReleasedQty { get; set; } = string.Empty;

        [DisplayName("Previous Released Quantity")]
        public string PreviousReleasedQty { get; set; } = string.Empty;

        [DisplayName("Balance Quantity")]
        public string BalanceQty { get; set; } = string.Empty;

        [DisplayName("Custom Release")]
        public string CustomRelease { get; set; } = string.Empty;

        [DisplayName("Due Date")]
        public string DueDate { get; set; } = string.Empty;

        [DisplayName("Receive By")]
        public string ReceiveBy { get; set; } = string.Empty;

        [DisplayName("Receive Date")]
        public string ReceiveDate { get; set; } = string.Empty;

        [DisplayName("Last Received By")]
        public string LastReceivedBy { get; set; } = string.Empty;

        [DisplayName("Last Received Date")]
        public string LastReceivedDate { get; set; } = string.Empty;

        [DisplayName("Last Released By")]
        public string PreviousReleasedBy { get; set; } = string.Empty;

        [DisplayName("Last Released Date")]
        public string PreviousReleasedDate { get; set; } = string.Empty;
    }
}
