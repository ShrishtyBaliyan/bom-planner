﻿using BOMPlanner.SQL;
using CadData;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReleaseBOM.Sql
{
    public static class ReleaseBOMSqlQueries
    {
        private static SqlConnection con;
        private static SqlDataAdapter Adapter;
        private static DataTable dt;


        private static DateTime relDate;


        /// <summary>
        ///  
        /// </summary>
        public static void SaveReleaseBOM(List<ReleaseModel> releaseList, AssemblyInfo baseInfo)
        {
            var parentName = string.Empty;
            foreach (var item in releaseList)
            {

                if (String.IsNullOrWhiteSpace(item.SNO) && (!item.PartNumber.Equals(baseInfo.PartNum, StringComparison.InvariantCultureIgnoreCase)))
                {
                    parentName = item.PartNumber;
                    continue;
                }
                else if (String.IsNullOrWhiteSpace(item.SNO) && (item.PartNumber.Equals(baseInfo.PartNum, StringComparison.InvariantCultureIgnoreCase)))
                {
                    parentName = item.PartNumber;
                }

                var itemId = SQLQueries.GetAsm_id(item.PartNumber);
                var verdt = SQLQueries.GetLatestVersion(itemId);
                var verId = Convert.ToInt32(verdt.Rows[verdt.Rows.Count-1][0]);

                var asm_child_id = GetAsmChildId(parentName, item.PartNumber);
                DataTable dt = GetDueDate(asm_child_id);
                if(dt == null)
                {
                    var rDt = ReleaseBOMSqlQueries.GetAsmId(asm_child_id);
                    var parent = Convert.ToInt32(rDt.Rows[0][0]);
                    var child = Convert.ToInt32(rDt.Rows[0][1]);
                    var childHisId = ReleaseBOMSqlQueries.GetAsmchildHistory(parent, child);
                    if (childHisId > -1)
                    {
                        dt = ReleaseBOMSqlQueries.GetDueDate(childHisId);
                    }
                }
                var dueDate = dt.Rows[0][0].ToString();
                //DeleteRecentRel();
                AddRelBOM(asm_child_id, item, dueDate, verId);
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="releaseList"></param>
        /// <param name="asmInfo"></param>
        /// <param name="parent"></param>
        public static void AddRelBOM(int id, ReleaseModel child, string dueDate, int verId)
        {
            int inserted = 0;
            int updated = 0;
            const string sql = @"INSERT INTO Release_BOM
           ([Asm_Child_Id]
      ,[Required_Qty]
      ,[Total_Qty]
      ,[Release_Qty]
      ,[Release_Date]
      ,[Release_By]
      ,[TotalReleasedQty]
      ,[BalanceQty]
      ,[CustomRelease]
      ,[DueDate]
      ,[Receive_By]
      ,[Receive_Date]
      ,[LastReceivedBy]
      ,[LastReceivedDate]
      ,[LastReleasedBy]
      ,[LastReleasedDate]
      ,[Version_id])
     VALUES
           (@Asm_Child_Id
      ,@Required_Qty
      ,@Total_Qty
      ,@Release_Qty
      ,@Release_Date
      ,@Release_By
      ,@TotalReleasedQty
      ,@BalanceQty
      ,@CustomRelease
      ,@DueDate
      ,@Receive_By
      ,@Receive_Date
      ,@LastReceivedBy
      ,@LastReceivedDate
      ,@LastReleasedBy
      ,@LastReleasedDate
      ,@Version_id)";

            using (SqlConnection conn = new SqlConnection(SQLConnection.connection_string))
            {
                conn.Open();
                //var totalRelQty = (Convert.ToInt32(child.TotalReleasedQty) + Convert.ToInt32(child.ReleaseQty)).ToString();

                using (SqlCommand cmd = new SqlCommand(sql, conn))
                {
                    cmd.Parameters.AddWithValue("@Asm_Child_Id", id);
                    cmd.Parameters.AddWithValue("@Required_Qty", child.RequiredQty);
                    cmd.Parameters.AddWithValue("@Total_Qty", child.TotalQty);
                    cmd.Parameters.AddWithValue("@Release_Qty", child.ReleaseQty);
                    cmd.Parameters.AddWithValue("@Release_Date", child.ReleaseDate);
                    cmd.Parameters.AddWithValue("@Release_By", child.ReleaseBy);
                    cmd.Parameters.AddWithValue("@TotalReleasedQty", child.TotalReleasedQty);
                    cmd.Parameters.AddWithValue("@BalanceQty", child.BalanceQty);
                    cmd.Parameters.AddWithValue("@CustomRelease", child.CustomRelease);
                    cmd.Parameters.AddWithValue("@DueDate", dueDate);
                    cmd.Parameters.AddWithValue("@Receive_By", child.ReceiveBy);
                    cmd.Parameters.AddWithValue("@Receive_Date", child.ReceiveDate);
                    cmd.Parameters.AddWithValue("@LastReceivedBy", child.LastReceivedBy);
                    cmd.Parameters.AddWithValue("@LastReceivedDate", child.LastReceivedDate);
                    cmd.Parameters.AddWithValue("@LastReleasedBy", child.PreviousReleasedBy);
                    cmd.Parameters.AddWithValue("@LastReleasedDate", child.PreviousReleasedDate);
                    cmd.Parameters.AddWithValue("@Version_id", verId);

                    cmd.ExecuteNonQuery();
                    inserted++;
                }
                conn.Close();

            }

        }




        /// <summary>
        /// 
        /// </summary>
        /// <param name="parentAsm"></param>
        /// <param name="childAsm"></param>
        /// <returns></returns>
        public static int GetAsmChildId(string parentAsm, string childAsm)
        {
            string getRel = @"select [id] from [dbo].[Assembly_Child_Info] where assembly_id = (select id from [dbo].[Assembly_info] where part_number = '" + parentAsm + "') " +
                "and child_id = (select id from [dbo].[Assembly_info] where part_number = '" + childAsm + "') ";

            using (con = new SqlConnection(SQLConnection.connection_string))
            {
                using (SqlCommand cmd = new SqlCommand(getRel, con))
                {
                    cmd.CommandType = CommandType.Text;
                    using (Adapter = new SqlDataAdapter(cmd))
                    {
                        using (dt = new DataTable())
                        {
                            Adapter.Fill(dt);
                        }
                    }

                    if (dt.Rows.Count == 0)
                    {
                        return -1;
                    }
                    else
                    {
                        return Convert.ToInt32(dt.Rows[0][0]);
                    }
                } // using
            } // using
        }




        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static DataTable GetReleaseBOMinfo(int asm_child_id)
        {
            var relDateDt = GetReleaseDate(asm_child_id);

            if (relDateDt != null)
            {
                for (int i = 0; i < relDateDt.Rows.Count; i++)
                {
                    var Row = relDateDt.Rows[i][0].ToString();
                    bool newdate = relDate <= (Convert.ToDateTime(Row));
                    if (newdate == true)
                    {
                        relDate = Convert.ToDateTime(Row);
                    }
                }
            }

            var newRelDate = relDate.ToString();
            string getRel = @"select [Required_Qty],[TotalReleasedQty],[CustomRelease],[Receive_By],[Receive_Date],[Release_By],[Release_Qty]
from [dbo].[Release_BOM] where Asm_Child_Id = '" + asm_child_id + "' and [Release_Date] = '" + newRelDate + "' ";
            using (con = new SqlConnection(SQLConnection.connection_string))
            {
                using (SqlCommand cmd = new SqlCommand(getRel, con))
                {
                    cmd.CommandType = CommandType.Text;
                    using (Adapter = new SqlDataAdapter(cmd))
                    {
                        using (dt = new DataTable())
                        {
                            Adapter.Fill(dt);
                        }
                    }

                    if (dt.Rows.Count == 0)
                    {
                        return null;
                    }
                    else
                    {
                        return dt;
                    }
                } // using
            } // using
        }



        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static DataTable GetReleaseDate(int asm_child_id)
        {
            string getRel = @"select [Release_Date] from [dbo].[Release_BOM] where Asm_Child_Id = '" + asm_child_id + "' ";
            using (con = new SqlConnection(SQLConnection.connection_string))
            {
                using (SqlCommand cmd = new SqlCommand(getRel, con))
                {
                    cmd.CommandType = CommandType.Text;
                    using (Adapter = new SqlDataAdapter(cmd))
                    {
                        using (dt = new DataTable())
                        {
                            Adapter.Fill(dt);
                        }
                    }

                    if (dt.Rows.Count == 0)
                    {
                        return null;
                    }
                    else
                    {
                        return dt;
                    }
                } // using
            } // using
        }


        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static DataTable GetAsmId(int asm_child_id)
        {
            string getRel = @"select [assembly_id],[child_id] from [dbo].[Assembly_Child_Info] where id = '" + asm_child_id + "' ";
            using (con = new SqlConnection(SQLConnection.connection_string))
            {
                using (SqlCommand cmd = new SqlCommand(getRel, con))
                {
                    cmd.CommandType = CommandType.Text;
                    using (Adapter = new SqlDataAdapter(cmd))
                    {
                        using (dt = new DataTable())
                        {
                            Adapter.Fill(dt);
                        }
                    }

                    if (dt.Rows.Count == 0)
                    {
                        return null;
                    }
                    else
                    {
                        return dt;
                    }
                } // using
            } // using
        }


        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static int GetAsmchildHistory(int parent, int child)
        {
            string getRel = @"select [asmChild_id] from [dbo].[Asm_ChildInfo_History] where assembly_id = '" + parent + "' and child_id = '" + child + "' ";
            using (con = new SqlConnection(SQLConnection.connection_string))
            {
                using (SqlCommand cmd = new SqlCommand(getRel, con))
                {
                    cmd.CommandType = CommandType.Text;
                    using (Adapter = new SqlDataAdapter(cmd))
                    {
                        using (dt = new DataTable())
                        {
                            Adapter.Fill(dt);
                        }
                    }

                    if (dt.Rows.Count == 0)
                    {
                        return -1;
                    }
                    else
                    {
                        return Convert.ToInt32(dt.Rows[dt.Rows.Count-1][0]);
                    }
                } // using
            } // using
        }



        /// <summary>
        /// 
        /// </summary>
        /// <param name="asmChildId"></param>
        /// <returns></returns>
        public static DataTable GetDueDate(int asmChildId)
        {
            string getDueDate = @"select [DueDate],[AssignTo] from [dbo].[DueDateRecord] where Asm_Child_Id = '" + asmChildId + "' ";
            using (con = new SqlConnection(SQLConnection.connection_string))
            {
                using (SqlCommand cmd = new SqlCommand(getDueDate, con))
                {
                    cmd.CommandType = CommandType.Text;
                    using (Adapter = new SqlDataAdapter(cmd))
                    {
                        using (dt = new DataTable())
                        {
                            Adapter.Fill(dt);
                        }
                    }

                    if (dt.Rows.Count == 0)
                    {
                        return null;
                    }
                    else
                    {
                        return dt;
                    }
                } // using
            } // using
        }



        /// <summary>
        /// 
        /// </summary>
        public static void SaveDueDate(List<SetDueDateList> dateList, AssemblyInfo baseInfo)
        {
            var parentName = string.Empty;
            foreach (var item in dateList)
            {
                if (String.IsNullOrWhiteSpace(item.SNO) && String.IsNullOrWhiteSpace(item.PartNumber))
                {
                    continue;
                }

                if (String.IsNullOrWhiteSpace(item.SNO) && (!item.PartNumber.Equals(baseInfo.PartNum, StringComparison.InvariantCultureIgnoreCase)))
                {
                    parentName = item.PartNumber;
                    continue;
                }
                else if (String.IsNullOrWhiteSpace(item.SNO) && (item.PartNumber.Equals(baseInfo.PartNum, StringComparison.InvariantCultureIgnoreCase)))
                {
                    parentName = item.PartNumber;
                }

                var asm_child_id = GetAsmChildId(parentName, item.PartNumber);
                //DeleteRecentRel();
                SaveDate(asm_child_id, item);
            }
        }



        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="item"></param>
        public static void SaveDate(int id, SetDueDateList child)
        {

            int inserted = 0;
            int updated = 0;
            const string sql = @"INSERT INTO DueDateRecord
           (Asm_Child_Id
      ,DueDate
      ,AssignTo)
     VALUES
           (@Asm_Child_Id
      ,@DueDate
      ,@AssignTo)";

            const string sqlUpdate = @"UPDATE [DueDateRecord]
   SET [Asm_Child_Id] = @Asm_Child_Id
      ,[DueDate] = @DueDate
      ,[AssignTo] = @AssignTo
 WHERE [Asm_Child_Id] = @Asm_Child_Id";

            const string sqlSelect = @"SELECT [id] FROM [dbo].[DueDateRecord] WHERE [Asm_Child_Id] = @Asm_Child_Id";

            using (SqlConnection conn = new SqlConnection(SQLConnection.connection_string))
            {
                conn.Open();

                using (SqlCommand cmdSelect = new SqlCommand(sqlSelect, conn))
                {
                    cmdSelect.CommandType = CommandType.Text;
                    cmdSelect.Parameters.AddWithValue("@Asm_Child_Id", id);
                    using (SqlDataAdapter sda = new SqlDataAdapter(cmdSelect))
                    {
                        using (DataTable dt = new DataTable())
                        {
                            sda.Fill(dt);
                            if (dt.Rows.Count > 0)
                            {
                                using (SqlCommand updateCmd = new SqlCommand(sqlUpdate, conn))
                                {

                                    updateCmd.Parameters.AddWithValue("@Asm_Child_Id", id);
                                    updateCmd.Parameters.AddWithValue("@DueDate", child.SetDueDate);
                                    updateCmd.Parameters.AddWithValue("@AssignTo", child.AssignTo);

                                    updateCmd.ExecuteNonQuery();
                                    updated++;
                                }

                                return;
                            }
                        }
                    }
                }

                using (SqlCommand cmd = new SqlCommand(sql, conn))
                {
                    cmd.Parameters.AddWithValue("@Asm_Child_Id", id);
                    cmd.Parameters.AddWithValue("@DueDate", child.SetDueDate);
                    cmd.Parameters.AddWithValue("@AssignTo", child.AssignTo);

                    cmd.ExecuteNonQuery();
                    inserted++;
                }
                conn.Close();

            }
        }
    }
}