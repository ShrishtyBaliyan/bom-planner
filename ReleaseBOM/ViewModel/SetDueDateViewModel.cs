﻿using CadData;
using ReleaseBOM.Sql;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Input;

namespace ReleaseBOM.ViewModel
{
    public class SetDueDateViewModel : INotifyPropertyChanged, ICloseWindow
    {

        AssemblyInfo baseInfo;


        /// <summary>
        /// stores list values after refresing the Release BOM Window
        /// </summary>
        public List<SetDueDateList> dgList = new List<SetDueDateList>();

        /// <summary>
        /// List Binded to DataGrid
        /// </summary>
        private List<SetDueDateList> dataList;
        public List<SetDueDateList> DataList
        {
            get => this.dataList;
            set
            {
                this.dataList = value;
                OnPropertyChanged("DataList");
            }
        }


        /// <summary>
        /// stores the Name of Main Assembly, used for heading of Release BOM window
        /// </summary>
        private string label;
        public string Label
        {
            get => this.label;
            set
            {
                this.label = value;
                OnPropertyChanged("Label");
            }
        }

        /// <summary>
        /// stores boolean value of same values checkbox
        /// </summary>
        private bool sameRelease;
        public bool Same
        {
            get => this.sameRelease;
            set
            {
                this.sameRelease = value;
                OnPropertyChanged("SameRelease");
            }
        }



        /// <summary>
        /// stores boolean value of values checkbox
        /// </summary>
        private bool diffRelease;
        public bool Diff
        {
            get => this.diffRelease;
            set
            {
                this.diffRelease = value;
                OnPropertyChanged("DiffRelease");
            }
        }


        /// <summary>
        /// Property Changed Event Handler
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string PropertyName)
        {
            if (this.PropertyChanged == null)
                return;
            this.PropertyChanged((object)this, new PropertyChangedEventArgs(PropertyName));
        }



        public SetDueDateViewModel(AssemblyInfo baseInfo, List<SetDueDateList> info)
        {
            this.DataList = info;
            this.baseInfo = baseInfo;
            
            this.label = baseInfo.PartNum + "-" + baseInfo.ItemName;
        }


        /// <summary>
        /// Command for Save
        /// </summary>
        private ICommand saveCommand;
        public ICommand SaveCommand
        {
            get
            {
                if (this.saveCommand == null)
                    this.saveCommand = (ICommand)new ICommandInterface(new Action<object>(this.executeSave), new Func<object, bool>(this.canExecuteSave));
                return this.saveCommand;
            }
        }
        private bool canExecuteSave(object arg) => true;
        private void executeSave(object parameter)
        {
            if (this.diffRelease == false && this.sameRelease == false || this.diffRelease == true && this.sameRelease == true)
            {
                MessageBox.Show("Please Select Either 'Same Release' or 'Different Release'");
                return;
            }

            if (!RefreshDataGrid())
            {
                return;
            }
            ReleaseBOMSqlQueries.SaveDueDate(dgList, this.baseInfo);
            MessageBox.Show("Due Date And Assign To Set Successfully!!");
        }





        /// <summary>
        /// Command for close window
        /// </summary>
        private ICommand closeCommand;
        public ICommand CloseCommand
        {
            get
            {
                if (this.closeCommand == null)
                    this.closeCommand = (ICommand)new ICommandInterface(new Action<object>(this.executeClose), new Func<object, bool>(this.canExecuteClose));
                return this.closeCommand;
            }
        }
        private bool canExecuteClose(object arg) => true;
        private void executeClose(object parameter)
        {
            CloseWindow();
        }


        /// <summary>
        /// func for close window
        /// </summary>
        void CloseWindow()
        {
            Close?.Invoke();
        }

        public Action Close { get; set; }




        /// <summary>
        /// function for Refresh the values of DataGrid
        /// </summary>
        public bool RefreshDataGrid()
        {
            try
            {
                if (!RefreshDataGrid(baseInfo))
                {
                    return false;
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }

            return true;
        }




        /// <summary>
        ///  function for Refresh Values of DataGrid
        /// </summary>
        /// <param name="info"></param>
        /// <param name="sno"></param>
        /// <param name="asmInfo"></param>
        /// <param name="prtType"></param>
        /// <param name="isModel"></param>
        private bool RefreshDataGrid(AssemblyInfo asmInfo, bool isModel = false)
        {
            if (!asmInfo.childs.Any())
            {
                return false;
            }

            List<SetDueDateList> clonnedDgList = new List<SetDueDateList>();

            var duedate = string.Empty;
            var assignTo = string.Empty;

            foreach (var item in this.DataList)
            {
                if (String.IsNullOrWhiteSpace(item.PartNumber) || (String.IsNullOrWhiteSpace(item.SNO) && (!item.PartNumber.Equals(baseInfo.PartNum, StringComparison.InvariantCultureIgnoreCase))))
                {
                    clonnedDgList.Add(new SetDueDateList()
                    {
                        SNO = item.SNO,
                        PartNumber = item.PartNumber,
                        ItemName = item.ItemName,
                        UnitQty = item.UnitQty,
                        SetDueDate = item.SetDueDate,
                        AssignTo = item.AssignTo
                         
                    });
                }

                else
                {
                    if (this.sameRelease)
                    {
                        duedate = this.DataList[0].SetDueDate;
                        if (string.IsNullOrWhiteSpace(duedate))
                        {
                            MessageBox.Show("Please Set Due Date of Main Assembly!");
                            return false;
                        }
                        assignTo = this.DataList[0].AssignTo;
                    }

                    else if (this.diffRelease)
                    {
                        duedate = item.SetDueDate;
                        assignTo = item.AssignTo;
                    }


                    if (string.IsNullOrWhiteSpace(duedate) || string.IsNullOrWhiteSpace(assignTo))
                    {
                        MessageBox.Show("'Set Due Date' and 'Assign To' fields can not be Empty!");
                        return false;
                    }

                    clonnedDgList.Add(new SetDueDateList()
                    {
                        SNO = item.SNO,
                        PartNumber = item.PartNumber,
                        ItemName = item.ItemName,
                        UnitQty = item.UnitQty,
                        SetDueDate = duedate,
                        AssignTo = assignTo
                    });

                }

            }
            this.dgList.Clear();
            this.dgList = clonnedDgList;
            return true;
        }

    }
}
