﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadData
{
  public interface ICADDataReader
  {
    /// <summary>
    /// 
    /// </summary>
    /// <param name="path"></param>
    /// <returns></returns>
    void ReadAssembly(dynamic CADObj, AssemblyInfo parentInfo, int level);

    /// <summary>
    /// 
    /// </summary>
    /// <param name="path"></param>
    /// <returns></returns>
    AssemblyInfo GetCustomProperties(dynamic propertyMngr);
  }
}
