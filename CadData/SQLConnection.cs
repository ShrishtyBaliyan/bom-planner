﻿using System.Reflection;

namespace CadData
{
    public class SQLConnection
    {
        private const string SectionToRead = "LOCALDATA";
        private const string KeyToReadItem = "Item_Group";
        private const string DefaultValue = "";
        private static string IniFilePath;
        public static string export_images;
        private const string keyToReadIMG = "export_images";
        public static string Item_Group;
        private const string KeyToReadCon = "connection_string";
        public static string connection_string;
        private const string IniFileName = "setting.ini";
  

        static SQLConnection()
        {
            IniFilePath = System.IO.Path.Combine(System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location),
              IniFileName);
            Item_Group = (IniFileHelper.ReadValue(SectionToRead, KeyToReadItem, IniFilePath, DefaultValue));
            connection_string = (IniFileHelper.ReadValue(SectionToRead, KeyToReadCon, IniFilePath, DefaultValue));
        }
       

        public void SQLConnectionExprtImg(string doExportImage, bool write)
        {
            //export_images = (IniFileHelper.ReadValue(SectionToRead, keyToReadIMG, IniFilePath, DefaultValue));
            IniFilePath = System.IO.Path.Combine(System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location),
             IniFileName);
            if (write)
            {
                IniFileHelper.WriteValue(SectionToRead, keyToReadIMG, doExportImage, IniFilePath);
            }
            export_images = (IniFileHelper.ReadValue(SectionToRead, keyToReadIMG, IniFilePath, DefaultValue));
        }



        public void SQLConnectionAddItem(string AddItem, bool write)
        {
            //export_images = (IniFileHelper.ReadValue(SectionToRead, keyToReadIMG, IniFilePath, DefaultValue));
            IniFilePath = System.IO.Path.Combine(System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location),
             IniFileName);
            Item_Group = (IniFileHelper.ReadValue(SectionToRead, KeyToReadItem, IniFilePath, DefaultValue));

            if (write)
            {
                if (Item_Group != "")
                { IniFileHelper.WriteValue(SectionToRead, KeyToReadItem, Item_Group + "," + AddItem, IniFilePath); }
                else
                {
                    IniFileHelper.WriteValue(SectionToRead, KeyToReadItem, AddItem, IniFilePath);
                }
            }
            Item_Group = (IniFileHelper.ReadValue(SectionToRead, KeyToReadItem, IniFilePath, DefaultValue));

        }


        public void SQLConnectionItem(string removeItem, bool write)
        {
            //export_images = (IniFileHelper.ReadValue(SectionToRead, keyToReadIMG, IniFilePath, DefaultValue));
            IniFilePath = System.IO.Path.Combine(System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location),
             IniFileName);
            Item_Group = (IniFileHelper.ReadValue(SectionToRead, KeyToReadItem, IniFilePath, DefaultValue));

            if (write)
            {
                IniFileHelper.WriteValue(SectionToRead, KeyToReadItem, removeItem.ToString(), IniFilePath);
            }
            Item_Group = (IniFileHelper.ReadValue(SectionToRead, KeyToReadItem, IniFilePath, DefaultValue));

        }


        public SQLConnection()
        {

        }
    }
}
