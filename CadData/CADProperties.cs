﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadData
{
    public class CADProperties
    {
        #region Variables

        /// <summary>
        /// Stores the list of attributes read from the xml file
        /// </summary>
        private List<AttributeData> attributes = new List<AttributeData>();

       
        /// <summary>
        /// Stores the value of tafg in the xml file for the current properties
        /// </summary>
        private string xmlTag = string.Empty;

        #endregion

        #region Properties

        /// <summary>
        /// Indexer for getting and setting the atribute values
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public AttributeData this[string name]
        {
            get
            {
                return attributes.FirstOrDefault(att => att.Name.Equals(name, StringComparison.InvariantCultureIgnoreCase));
            }

            set
            {
                var stock = attributes.FirstOrDefault(stk => stk.Name.Equals(name, StringComparison.InvariantCultureIgnoreCase));
                if (stock != null)
                {
                    var index = attributes.IndexOf(stock);
                    if (index > -1)
                    {
                        attributes[index] = value;
                    }
                }
                else
                {
                    attributes.Add(value);
                }
            }
        }

        public string PartNum { get; set; }
        public IEnumerable<object> props { get; set; }

        #endregion

        #region Methods

        public void AddAttribute(string attName)
        {

        }



        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public List<AttributeData> GetAttributes()
        {
            return this.attributes.ToList();
        }
        #endregion
    }
}
