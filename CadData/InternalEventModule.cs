﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadData
{
    public class InternalEventModule
    {

        public Action<AssemblyInfo, string> QRGenerate { get; set; }

        public Action<AssemblyInfo, string> PdfGenerate { get; set; }
    }
}
