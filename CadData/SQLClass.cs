﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadData
{
    public static class SQLClass
    {
        private static SqlConnection con;
        private static SqlDataAdapter Adapter;
        private static DataTable RelDateDt;

        /// <summary>
        /// getting encripted code of description from database 
        /// </summary>
        /// <param name="des"></param>
        /// <returns></returns>
        public static DataTable GetDesCode(string user)
        {
            string getRelDate = @"SELECT [Release_Date], [BalanceQty] FROM [dbo].[Release_BOM] where Release_By ='" + user + "' ";

            using (con = new SqlConnection(SQLConnection.connection_string))
            {
                using (SqlCommand cmd = new SqlCommand(getRelDate, con))
                {
                    cmd.CommandType = CommandType.Text;
                    using (Adapter = new SqlDataAdapter(cmd))
                    {
                        using (RelDateDt = new DataTable())
                        {
                            Adapter.Fill(RelDateDt);
                        }
                        if (RelDateDt.Rows.Count == 0)
                        {
                            return null;
                        }
                        return RelDateDt;
                    }
                }
            }
        }
    }
}
