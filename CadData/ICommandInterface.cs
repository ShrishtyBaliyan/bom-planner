﻿using System;
using System.Windows.Input;

namespace CadData
{
    public class ICommandInterface : ICommand
    {
        private Action<object> _execute;
        private Func<object, bool> canExecute;

        public ICommandInterface(Action<object> execute, Func<object, bool> canExecute)
        {
            this._execute = execute;
            this.canExecute = canExecute;
        }

        public event EventHandler CanExecuteChanged
        {
            add => CommandManager.RequerySuggested += value;
            remove => CommandManager.RequerySuggested -= value;
        }

        public bool CanExecute(object parameter) => this.canExecute == null || this.canExecute(parameter);

        public void Execute(object parameter) => this._execute(parameter);
    }
}
