﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadData
{
    /// <summary>
    /// Class stores the properties for the attribute of the cad document
    /// </summary>
    public class AttributeData
    {
        Image img;

        /// <summary>
        /// stores the name of the attribute
        /// </summary>
        string name = string.Empty;

        /// <summary>
        /// stores the display name
        /// </summary>
        string displayName = string.Empty;

        /// <summary>
        /// Stores the order in all the available attributes
        /// </summary>
        int order;

      
        /// <summary>
        /// Stores value indicating whether the attribute value is General or not
        /// </summary>
        bool isDesign;

        /// <summary>
        /// Stores value indicating whether the attribute value is General or not
        /// </summary>
        bool isSave;

        /// <summary>
        /// Stores the string value of the attribute
        /// </summary>
        string strValue = string.Empty;

        bool isdate;

        /// <summary>
        /// Stores the numeric value of the attribute
        /// </summary>
        double dValue;

       

        /// <summary>
        /// 
        /// </summary>
        public AttributeData()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="AttributeXML"></param>
        public AttributeData(AttributeData AttributeXML)
        {
            this.Name = AttributeXML.Name;
            this.DisplayName = AttributeXML.DisplayName;
            this.Order = AttributeXML.Order;
            this.IsDesign = AttributeXML.IsDesign;
            this.IsSave = AttributeXML.IsSave;
            this.StrValue = AttributeXML.StrValue;
            this.DValue = AttributeXML.DValue;
            this.IsDate = AttributeXML.IsDate;
            this.Img = AttributeXML.Img;
        }
        public bool IsSave
        {
            get { return isSave; }
            set { isSave = value; }
        }

        public bool IsDesign
        {
            get { return isDesign; }
            set { isDesign = value; }
        }

        public bool IsDate
        {
            get { return isdate; }
            set { isdate = value; }
        }


        public string Name
        {
            get => name;
            set => name = value;
        }

        public Image Img { get => img; set => img = value; }
        public string DisplayName { get => displayName; set => displayName = value; }
        public int Order { get => order; set => order = value; }
        public string StrValue { get => strValue; set => strValue = value; }
        public double DValue { get => dValue; set => dValue = value; }
    }
}
