﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadData
{
  /// <summary>
  /// 
  /// </summary>
  public interface IReportGenerator
  {
        /// <summary>
        /// True if template needs to be used, else false
        /// </summary>
        bool DoUseTemplate { get; set; }

        /// <summary>
        /// Stores value for template name
        /// </summary>
        string TemplateName { get; set; }

        void Generate();
    }
}
