﻿using CadData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CaDData
{
    public class FillWeightInfo
    {
        public List<WeightInfo> GetWeightInfo(AssemblyInfo asmInfo)
        {
            var info = new List<WeightInfo>();
            var tuple = new List<Tuple<string, double>>();

            GetWeight(asmInfo, tuple);
            var group = tuple.GroupBy(g => g.Item1);

            foreach (var gr in group)
            {
                var wInfo = new WeightInfo();
                wInfo.materialName = gr.Key;
                wInfo.tWeight = gr.Sum(v => v.Item2);
                info.Add(wInfo);
            }

            return info;
        }

        private void GetWeight(AssemblyInfo asmInfo, List<Tuple<string, double>> tuple)
        {
            //if(string.IsNullOrEmpty( asmInfo.material))
            //{
            //  return;
            //}
            List<AssemblyInfo> aggAsmInfo = new List<AssemblyInfo>();
            var baseInfo1 = new AssemblyInfo();
            baseInfo1 = asmInfo;

            AssemblyInfo.AssemblyAggregate(baseInfo1, 1, aggAsmInfo);
            double res;

            foreach (var asmInfoChild in aggAsmInfo)
            {
                if (asmInfoChild.isAsm)
                {
                    continue;
                }

                if (!(string.IsNullOrWhiteSpace(asmInfoChild.properties["MATERIAL"].StrValue) || (asmInfoChild.properties["MATERIAL"].StrValue).Equals("Material <not specified>"))
                    && double.TryParse(asmInfoChild.properties["WEIGHT"].StrValue, out res))
                {
                    tuple.Add(new Tuple<string, double>(asmInfoChild.Material, res * asmInfoChild.qty));
                }
            }
            //asmInfo.childs.ForEach(c => GetWeight(c, tuple));

        }

    }
}
