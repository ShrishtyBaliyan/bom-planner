﻿using Microsoft.Win32;
using System.Xml;
using System.Collections.Generic;
using System;
using System.IO;
using System.Windows;
using System.Reflection;
using System.Windows.Forms;

namespace CadData
{
    public class PropertyReader
    {
        public static bool DoExportImages;

        //public static ProgressBar bar;
        // new xdoc instance 
        static readonly XmlDocument xDoc = new XmlDocument();
        static string propertFile = "Properties.xml";
        public static List<AttributeData> attributes = new List<AttributeData>();
        public static List<Tuple<string, List<AttributeData>>> stockTypes = new List<Tuple<string, List<AttributeData>>>();
        public static KeyValuePair<string, List<string>> key;
        public static List<string> boughtOuts = new List<string>();
        public static List<string> PartTypes = new List<string>();
        public static List<string> Priority = new List<string>();

        public static ProgressBar bar;

        public static void ReadProperties()
        {
            DoExportImages = Convert.ToBoolean(SQLConnection.export_images);

            string assemblyFolder = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            var xmlFileName = Path.Combine(assemblyFolder, propertFile);

            if (!File.Exists(xmlFileName))
            {
                return;
            }
            //load up the xml from the location 
            xDoc.Load(xmlFileName);
            attributes.Clear();
            stockTypes.Clear();
            key = new KeyValuePair<string, List<string>>();
            // cycle through each child noed 
            foreach (XmlNode node in xDoc.DocumentElement.ChildNodes)
            {
                switch (node.Name)
                {
                    case "AsmInfo":
                        {
                            // first node is the url ... have to go to nexted loc node 
                            foreach (XmlNode locNode in node)
                            {
                                // thereare a couple child nodes here so only take data from node named loc 
                                if (locNode.Name == "property")
                                {
                                    var att = new AttributeData()
                                    {
                                        // get the content of the loc node
                                        Name = locNode.InnerText
                                    };
                                    if (att.Name != "IMAGE" || (att.Name == "IMAGE" && DoExportImages))
                                    {
                                        attributes.Add(att);
                                    }
                                    else if (att.Name == "IMAGE" && !DoExportImages)
                                    {

                                    }
                                    for (int i = 0; i < locNode.Attributes.Count; i++)
                                    {
                                        var atVal = locNode.Attributes[i];
                                        switch (atVal.Name)
                                        {
                                            case "Display_Name":
                                                {
                                                    att.DisplayName = atVal.InnerText;
                                                }
                                                break;

                                            case "order":
                                                {

                                                    if (int.TryParse(atVal.InnerText, out var dVal))
                                                    {
                                                        att.Order = dVal;
                                                    }

                                                }
                                                break;
                                            case "IsSave":
                                                {
                                                    if (bool.TryParse(atVal.InnerText, out var dVal))
                                                    {
                                                        att.IsSave = dVal;
                                                    }
                                                }
                                                break;
                                            case "IsDesign":
                                                {
                                                    if (bool.TryParse(atVal.InnerText, out var dVal))
                                                    {
                                                        att.IsDesign = dVal;
                                                    }
                                                }
                                                break;
                                            case "IsDate":
                                                {
                                                    if (bool.TryParse(atVal.InnerText, out var dVal))
                                                    {
                                                        att.IsDate = dVal;
                                                    }
                                                }
                                                break;
                                        }
                                    }
                                }
                            }
                        }
                        break;

                   
                } // switch
            }
        }


        public static void ReadBOMSettings()
        {

        }

    }
}
