﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadData
{
  public class ProgressBarData
  {
    public int Step { get; set; }

    public string Text { get; set; }
  }
}
