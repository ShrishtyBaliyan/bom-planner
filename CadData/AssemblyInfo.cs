﻿using CadData;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CadData
{
    /// <summary>
    /// Class stores the information needed for the assembly
    /// </summary>
    public class AssemblyInfo
    {
        /// <summary>
        /// Stores the cad file properties
        /// </summary>
        public CADProperties properties = new CADProperties();

        /// <summary>
        /// Stores the child for the cad file
        /// </summary>
        public List<AssemblyInfo> childs = new List<AssemblyInfo>();

        /// <summary>
        /// Stores true if the file is assembly type
        /// </summary>
        public bool isAsm;

        /// <summary>
        /// Stores the list of top level assemblies inside this file, if it is an assembly
        /// </summary>
        public List<AssemblyInfo> topLevelAsm = new List<AssemblyInfo>();


        public int id;

        public int qty = 1;

        public string path;


        /// <summary>
        /// 
        /// </summary>
        public AssemblyInfo()
        {
            foreach (var att in PropertyReader.attributes)
            {
                properties[att.Name] = new AttributeData(att);
            }
        }



        /// <summary>
        /// 
        /// </summary>
        /// <param name="info"></param>
        public AssemblyInfo(AssemblyInfo info)
        {
            foreach (var att in info.properties.GetAttributes())
            {
                properties[att.Name] = new AttributeData(att);
            }
            qty = info.qty;
        }


        public string PartNum
        {
            get
            {
                var attr = properties["PART NUMBER"];
                return attr.StrValue;
            }
            set { }
        }


        public String DesignDate
        {
            get
            {
                var attr = properties["Design Date"];
                return attr.StrValue;
            }
            set { }
        }

        public String DrawnDate
        {
            get
            {
                var attr = properties["Drawn Date"];
                return attr.StrValue;
            }
            set { }
        }

        public string ItemName
        {
            get
            {
                var attr = properties["ITEM NAME"];
                return attr.StrValue;
            }
            set { }
        }

        public string ClientName
        {
            get
            {
                var attr = properties["CLIENT"];
                return attr.StrValue;
            }
            set { }
        }

        public string Qty
        {
            get
            {
                var attr = properties["QTY"];
                return attr.StrValue;
            }
            set { }
        }

        public string Version
        {
            get
            {
                var attr = properties["VERSION"];
                return attr.StrValue;
            }
            set { }
        }

        public string origin
        {
            get
            {
                var attr = properties["ORIGIN"];
                return attr.StrValue;
            }
            set { }
        }


        public string MasterProject
        {
            get
            {
                var attr = properties["MASTER PROJECT"];
                return attr.StrValue;
            }
            set { }
        }


        public string ProjectNumber

        {
            get
            {
                var attr = properties["MODEL NUMBER"];
                return attr.StrValue;
            }
            set { }
        }



        public string Make
        {
            get
            {
                var attr = properties["MAKE"];
                return attr.StrValue;
            }
            set { }
        }



        public string designBy
        {
            get
            {
                var attr = properties["DESIGN BY"];
                return attr.StrValue;
            }
            set { }
        }

        public string DrawnBy
        {
            get
            {
                var attr = properties["DRAWN BY"];
                return attr.StrValue;
            }
            set { }
        }

        public string Des
        {
            get
            {
                var attr = properties["DESCRIPTION"];
                return attr.StrValue;
            }
            set { }

        }

        public string uWeight
        {
            get
            {
                var attr = properties["WEIGHT"];
                return attr.StrValue;
            }
            set { }
        }


        public string ItemGroup
        {
            get
            {
                var attr = properties["TYPE"];
                return attr.StrValue;
            }
            set { }
        }

        public string Material
        {
            get
            {
                var attr = properties["MATERIAL"];
                return attr.StrValue;
            }
            set { }
        }

        public string Remark
        {
            get
            {
                var attr = properties["REMARK"];
                return attr.StrValue;
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="assemblyInfo"></param>
        /// <param name="currAsmCount"></param>
        /// <param name="aggAsmInfos"></param>
        public static void AsmAggregate(AssemblyInfo assemblyInfo, int currAsmCount, List<AssemblyInfo> aggAsmInfos)
        {
            foreach (var asmInfoChild in assemblyInfo.childs)
            {
                //var clonnedChild = new AssemblyInfo(asmInfoChild);
                var existingInfo = aggAsmInfos.FirstOrDefault(info => info.PartNum.Equals(asmInfoChild.PartNum));
                if (existingInfo != null)
                {
                    existingInfo.qty += currAsmCount * asmInfoChild.qty;
                    continue;
                }
                else
                {
                    asmInfoChild.qty = currAsmCount * asmInfoChild.qty;
                    aggAsmInfos.Add(asmInfoChild);
                }
            }


            foreach (var child in assemblyInfo.childs)
            {
                AsmAggregate(child, child.qty * currAsmCount, aggAsmInfos);
            }

        }


        //public List<AssemblyInfo> AsmAggregate()
        //{
        //    var returnList = new List<AssemblyInfo>();
        //    var result = new List<AssemblyInfo>();
        //    result.AddRange(AsmAggregate(this, returnList));
        //    return result;
        //}

        //public List<AssemblyInfo> AsmAggregate(AssemblyInfo assemblyInfo, List<AssemblyInfo> aggAsmInfos)
        //{
        //    aggAsmInfos.Add(assemblyInfo);
        //    foreach (var info in assemblyInfo.childs)
        //    {
        //        if (info.childs.Any())
        //            AsmAggregate(info, aggAsmInfos);
        //        else
        //        {
        //            aggAsmInfos.Add(info);
        //        }
        //    }
        //    return aggAsmInfos.ToList();
        //}




        /// <summary>
        /// 
        /// </summary>
        /// <param name="assemblyInfo"></param>
        /// <param name="currAsmCount"></param>
        /// <param name="aggAsmInfos"></param>
        public static void AssemblyAggregate(AssemblyInfo assemblyInfo, int currAsmCount, List<AssemblyInfo> aggAsmInfos)
        {
            foreach (var asmInfoChild in assemblyInfo.childs)
            {
                var clonnedChild = new AssemblyInfo(asmInfoChild);
                if (asmInfoChild.childs.Count > 0)
                {
                    clonnedChild.isAsm = true;
                }
                var existingInfo = aggAsmInfos.FirstOrDefault(info => info.PartNum.Equals(clonnedChild.PartNum));
                if (existingInfo != null)
                {
                    existingInfo.qty += currAsmCount * clonnedChild.qty;
                }
                else
                {
                    clonnedChild.qty = currAsmCount * clonnedChild.qty;
                    aggAsmInfos.Add(clonnedChild);
                }
            }


            foreach (var child in assemblyInfo.childs)
            {
                AssemblyAggregate(child, child.qty * currAsmCount, aggAsmInfos);
            }

        }

    }

    /// <summary>
    /// 
    /// </summary>
    public class WeightInfo
    {
        public string materialName = string.Empty;

        public double tWeight = 0;
        public double cost = 0;
        public double totalCost = 0;
    }

}


