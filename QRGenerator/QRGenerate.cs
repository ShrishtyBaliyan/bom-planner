﻿using SolidWorks.Interop.sldworks;
using SolidWorks.Interop.swconst;
using System.IO;
using System.Linq;

namespace QRGenerator
{
    public class QRGenerate
    {

        private void btnApply_Click(string path)
        {
            var location = new System.Windows.Point(double.Parse("40"), double.Parse("40"));

            var dInfo = new DirectoryInfo(path);
            var fileInfos = dInfo.GetFiles();

            foreach (var info in fileInfos.Where(fileInfo => fileInfo.Extension.ToUpperInvariant().Equals(".SLDDRW")))
            {
                var fileName = Path.GetFileNameWithoutExtension(info.FullName);
                var partPath = Path.GetDirectoryName(info.FullName);
                var type = (int)swDocumentTypes_e.swDocPART;

                var partFile = Path.Combine(partPath, fileName + ".sldprt");
                if (!File.Exists(partFile))
                {
                    type = (int)swDocumentTypes_e.swDocASSEMBLY;
                    partFile = Path.Combine(partPath, fileName + ".sldasm");
                    if (!File.Exists(partFile))
                    {
                        continue;
                    }
                }

                SolidWorks.Interop.sldworks.SldWorks swApp = new SldWorks();
                swApp.Visible = true;
                int longstatus = 0;
                int longwarnings = 0;
                var partDwg = swApp.OpenDoc6(partFile, type, 0, "", ref longstatus, ref longwarnings);

                var swModel = (ModelDoc2)partDwg;
                //var assodel = (AssemblyDoc)openDoc;
                var swCustPropMgr = swModel.Extension.CustomPropertyManager[""];
                //  var swConf = (Configuration)swConfMgr.ActiveConfiguration;
                //   var swCustPropMgr = swConf.CustomPropertyManager;
                var partInfo = GetInfo(swCustPropMgr, partFile);

                var qrString = partInfo.PartNum + " - " + partInfo.Description + " - " + partInfo.Material;

                var imagePath = Path.Combine(Path.GetTempPath(), "img.png");
                CreateQRCode(qrString, imagePath);
                //partDwg.Close();

                partDwg = swApp.OpenDoc6(info.FullName, (int)swDocumentTypes_e.swDocDRAWING, 0, "", ref longstatus, ref longwarnings);

                var swDraw = partDwg as DrawingDoc;

                swModel.ClearSelection2(true);
                var vSheetNames = (string[])swDraw.GetSheetNames();
                foreach (var vSheetName in vSheetNames)
                {
                    swDraw.ActivateSheet(vSheetName);
                    var swSheet = (Sheet)swDraw.GetCurrentSheet();

                    var sketchManager = partDwg.SketchManager;

                    var sketchPicture = sketchManager.InsertSketchPicture(imagePath);
                    sketchPicture.SetOrigin(this.location.X, this.location.Y);
                    sketchPicture.SetSize(.020, .020, false);
                }
                partDwg.Save2(true);
                //partName.Save2(true);
                swApp.CloseDoc(info.FullName);
                swApp.CloseDoc(partFile);
            }
        }
    }
}
