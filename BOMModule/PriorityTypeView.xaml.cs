﻿using BOMModule.ViewModel;
using System.Windows;

namespace BOMModule
{
    /// <summary>
    /// Interaction logic for PriorityTypeView.xaml
    /// </summary>
    public partial class PriorityTypeView : Window
    {
        public PriorityTypeView()
        {
            InitializeComponent();
            Loaded += PriorityTypeView_Loaded;
        }
        private void PriorityTypeView_Loaded(object sender, RoutedEventArgs e)
        {
            if (DataContext is IClosePriorityWindow vm)
            {
                vm.ClosePriority += () =>
                {
                    this.Close();
                };
            }
        }
    }
}
