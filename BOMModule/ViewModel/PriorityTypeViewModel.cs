﻿using CADData;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace BOMModule.ViewModel
{
    public class PriorityTypeViewModel : INotifyPropertyChanged, IClosePriorityWindow
    {
        public List<string> cmbPriorities;
        public List<string> CmbPriorities
        {
            get => PropertyReader.Priority.ToList();
            set
            {
                this.cmbPriorities = value;

                OnPropertyChanged("CmbPriorities");
            }
        }


        public List<string> cmbTypes;
        public List<string> CmbTypes
        {
            get => SQLConnection.Part_Type.Split(',').ToList();
            set
            {
                this.cmbTypes = value;
                OnPropertyChanged("CmbTypes");
            }
        }


        /// <summary>
        /// 
        /// </summary>
        private string selectedItem;
        public string SelectedItem
        {
            get => this.selectedItem;
            set
            {
                this.selectedItem = value;
                OnPropertyChanged("SelectedItem");
            }
        }


        /// <summary>
        /// 
        /// </summary>
        private Priority selectedPriority;
        public Priority SelectedPriority
        {
            get => this.selectedPriority;
            set
            {
                this.selectedPriority = value;
                OnPropertyChanged("SelectedPriority");
            }
        }



       
        public void BOMPriorityByType(AssemblyInfo rootAssembly, string path)
        {
            BOM bOM = new BOM();
            bOM.BOMPriorityListByType(rootAssembly, this.selectedPriority, path, this.selectedItem);
        }



        /// <summary>
        /// 
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string PropertyName)
        {
            if (this.PropertyChanged == null)
                return;
            this.PropertyChanged((object)this, new PropertyChangedEventArgs(PropertyName));
        }



        /// <summary>
        ///  Ok Command
        /// </summary>
        private ICommand okCommand;
        public ICommand OKCommand
        {
            get
            {
                if (this.okCommand == null)
                    this.okCommand = (ICommand)new ICommandInterface(new Action<object>(this.executeOk), new Func<object, bool>(this.canExecuteOk));
                return this.okCommand;
            }
        }

        private bool canExecuteOk(object arg) => true;

        private void executeOk(object parameter)
        {
            CloseWindow();
        }



        /// <summary>
        /// Close Window
        /// </summary>
        private ICommand closeCommand;
        public ICommand CloseCommand
        {
            get
            {
                if (this.closeCommand == null)
                    this.closeCommand = (ICommand)new ICommandInterface(new Action<object>(this.executeClose), new Func<object, bool>(this.canExecuteClose));
                return this.closeCommand;
            }
        }

        private bool canExecuteClose(object arg) => true;

        private void executeClose(object parameter)
        {
            this.selectedItem = null;
            this.selectedPriority = Priority.None;
            CloseWindow();
        }


        /// <summary>
        /// 
        /// </summary>
        void CloseWindow()
        {
            ClosePriority?.Invoke();
        }

        /// <summary>
        /// 
        /// </summary>
        public Action ClosePriority { get; set; }
    }

    interface IClosePriorityWindow
    {
        Action ClosePriority { get; set; }
    }
}
