﻿using CADData;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace BOMModule.ViewModel
{
    public class PartListByTypeViewModel : INotifyPropertyChanged, ICloseWindow
    {
        public List<string> cmbTypes;
        public List<string> CmbTypes
        {
            get => SQLConnection.Part_Type.Split(',').ToList();
            set
            {
                this.cmbTypes = value;

                OnPropertyChanged("CmbTypes");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public string selectedType;
        public string SelectedType
        {
            get => this.selectedType;
            set
            {
                this.selectedType = value;
                OnPropertyChanged("SelectedItem");
            }
        }


        public PartListByTypeViewModel()
        {

        }

        /// <summary>
        /// 
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string PropertyName)
        {
            if (this.PropertyChanged == null)
                return;
            this.PropertyChanged((object)this, new PropertyChangedEventArgs(PropertyName));
        }
        




        public void BOMPartListByType(AssemblyInfo rootAssembly, string path)
        {
            BOM bOM = new BOM();
            bOM.BOMPartListByType(rootAssembly, path, this.selectedType);
        }

        /// <summary>
        ///  Ok Command
        /// </summary>
        private ICommand submitCommand;
        public ICommand SubmitCommand
        {
            get
            {
                if (this.submitCommand == null)
                    this.submitCommand = (ICommand)new ICommandInterface(new Action<object>(this.executeSubmit), new Func<object, bool>(this.canExecuteSubmit));
                return this.submitCommand;
            }
        }

        private bool canExecuteSubmit(object arg) => true;

        private void executeSubmit(object parameter)
        {
            CloseWindow();
        }

        /// <summary>
        /// Close Window
        /// </summary>
        private ICommand closeCommand;
        public ICommand CloseCommand
        {
            get
            {
                if (this.closeCommand == null)
                    this.closeCommand = (ICommand)new ICommandInterface(new Action<object>(this.executeClose), new Func<object, bool>(this.canExecuteClose));
                return this.closeCommand;
            }
        }

        private bool canExecuteClose(object arg) => true;

        private void executeClose(object parameter)
        {
            this.selectedType = null;
            CloseWindow();
        }


        /// <summary>
        /// 
        /// </summary>
        void CloseWindow()
        {
            ClosePartView?.Invoke();
        }

        /// <summary>
        /// 
        /// </summary>
        public Action ClosePartView { get; set; }
    }

    interface ICloseWindow
    {
        Action ClosePartView { get; set; }
    }
}

