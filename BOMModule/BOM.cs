﻿using BOMCreator;
using BOMModule.ViewModel;
using CADData;
using System;
using System.Windows.Forms;

namespace BOMModule
{
    public class BOM
    {
       
        public BOM()
        {
            
        }

     

        public void BOMDesign(AssemblyInfo rootAssembly, string path)
        {
           
            var bomProcessor = BOMProcessorFactory.Create(rootAssembly,
                                      null,
                                      FileType.asm,
                                      Priority.None,
                                      BOMType.General,
                                      path,
                                      IssuedTo.NA,
                                      Source.None,
                                      "");
               
          
            bomProcessor.Process(ReportType.Genral, BoM.Design, null);
            
        }



        public void BOMPartListByType(AssemblyInfo rootAssembly, string path, string selectedItem)
        {
            if (!string.IsNullOrEmpty(selectedItem))
            {
                var bomProcessor = BOMProcessorFactory.Create(rootAssembly,
                                           null,
                                           FileType.part,
                                           Priority.None,
                                           BOMType.General,
                                           path,
                                           IssuedTo.NA,
                                           Source.None,
                                           selectedItem);

                bomProcessor.Process(ReportType.Genral, BoM.PartByType, null);
            }
        }


        public void BOMPriorityListByType(AssemblyInfo rootAssembly,Priority priority, string path, string selectedItem)
        {
            if (!string.IsNullOrEmpty(selectedItem))
            {
                var bomProcessor = BOMProcessorFactory.Create(rootAssembly,
                                           null,
                                           FileType.part,
                                           priority,
                                           BOMType.General,
                                           path,
                                           IssuedTo.NA,
                                           Source.None,
                                           selectedItem);

                bomProcessor.Process(ReportType.Genral, BoM.PartByType, null);
            }
        }
    }
}
