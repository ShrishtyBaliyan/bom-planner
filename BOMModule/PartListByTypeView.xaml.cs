﻿using BOMModule.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace BOMModule
{
    /// <summary>
    /// Interaction logic for PartListByTypeView.xaml
    /// </summary>
    public partial class PartListByTypeView : Window
    {
        public PartListByTypeView()
        {
            InitializeComponent();
            Loaded += PartListByTypeView_Loaded;
        }
        private void PartListByTypeView_Loaded(object sender, RoutedEventArgs e)
        {
            if (DataContext is ICloseWindow vm)
            {
                vm.ClosePartView += () =>
                {
                    this.Close();
                };
            }
        }
    }
}
