﻿using CadData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace BOMCreator
{
    public class BOMProcessorFactory 
    {

        public static IBOMProcessor Create(AssemblyInfo rootAssembly,
                                           BOMType type,
                                           FileType fileType,
                                           string itemGroup = "")
        {
            switch (type)
            {
                case BOMType.General:
                    {
                        return new BOMProcessorGeneral(rootAssembly,
                                                        type,
                                                        fileType,
                                                        itemGroup);
                    }

                case BOMType.Release:
                    {
                        return new BOMProcessorRelease(rootAssembly,
                                                        type,
                                                        fileType,
                                                        itemGroup);
                    }
            }

            return null;

        }
    }
}
