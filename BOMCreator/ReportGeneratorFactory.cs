﻿using CadData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BOMCreator
{
    public class ReportGeneratorFactory
    {
        public static IReportGenerator Create(AssemblyInfo assemblyInfo,
                                              ReportType reportType,
                                              FileType fileType,
                                              string itemGroup)
        {
            switch (reportType)
            {
                case ReportType.Release:
                    {
                        return new ReportGeneratorBOMRelease(assemblyInfo, reportType, fileType, itemGroup);
                    }

                case ReportType.Genral:
                    {
                        return new ReportGeneratorGeneral(assemblyInfo, reportType, fileType, itemGroup);
                    }
            }
            return null;
        }
    }
}
