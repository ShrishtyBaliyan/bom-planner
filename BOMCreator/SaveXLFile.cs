﻿using CadData;
using ClosedXML.Excel;
using Microsoft.Win32;
using ReleaseBOM;
using ReleaseBOM.Sql;
using System.Collections.Generic;
using System.Data;
using System.Windows;

namespace BOMCreator
{
    public class SaveXLFile
    {

        /// <summary>
        /// 
        /// </summary>
        public static InternalEventModule QREvent = new InternalEventModule();


        /// <summary>
        /// 
        /// </summary>
        public static InternalEventModule PdfEvent = new InternalEventModule();



        /// <summary>
        /// 
        /// </summary>
        /// <param name="workbook"></param>
        /// <param name="savedt"></param>
        public void FileSave(XLWorkbook workbook, List<ReleaseModel> releaseList, AssemblyInfo baseInfo, bool QR, bool pdf)
        {

            SaveFileDialog saveDlg = new SaveFileDialog();
            saveDlg.InitialDirectory = @"D:\";
            bool save = (bool)saveDlg.ShowDialog();
            if (save)
            {
                ReleaseBOMSqlQueries.SaveReleaseBOM(releaseList, baseInfo);
                workbook.SaveAs(saveDlg.FileName + ("") + ".xlsx");
                if (QR)
                {
                    QREvent.QRGenerate(baseInfo, baseInfo.path);
                }
                if (pdf)
                {
                    PdfEvent.PdfGenerate(baseInfo, baseInfo.path);
               }

                MessageBox.Show("BOM Released successfully");
            }
            else
            {
                MessageBox.Show("BOM is not Released");
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="workbook"></param>
        public void FileSaveCreateBOM(XLWorkbook workbook)
        {
            SaveFileDialog saveDlg = new SaveFileDialog();
            saveDlg.InitialDirectory = @"D:\";
            bool save = (bool)saveDlg.ShowDialog();
            if (save)
            {
                workbook.SaveAs(saveDlg.FileName + ("") + ".xlsx");
                MessageBox.Show("BOM Createed successfully");
            }
            else
            {
                MessageBox.Show("BOM is not Createed");
            }
        }
    }
}
