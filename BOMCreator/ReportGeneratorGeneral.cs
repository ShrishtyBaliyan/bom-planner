﻿using BOMCreator;
using BOMCreator.ViewModel;
using BOMPlanner.View;
using CadData;
using CaDData;
using ClosedXML.Excel;
using ClosedXML.Excel.Drawings;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace BOMCreator
{
    public class ReportGeneratorGeneral : IReportGenerator
    {
        string ver;

        private const string asm_info = @"SELECT *
    FROM [dbo].[Asm_Version_Info]
    where AsmInfo_id =";


        private const string asm_info_table = @"SELECT *
    FROM [dbo].[Assembly_info]
    where id =";

        private List<AsmID> asmIdsWithChild = new List<AsmID>();
        private AssemblyInfo asmInfos = new AssemblyInfo();

        AssemblyInfo baseInfo;

        public bool DoUseTemplate { get; set; }

        public string TemplateName { get; set; }

        bool DoExportImages = Convert.ToBoolean(SQLConnection.export_images);
        private ReportType reportType;
        private FileType fileType;
        private string itemGroup;
        private SqlConnection con;
        private SqlDataAdapter Adapter;
        private DataTable dt;

        public ReportGeneratorGeneral(AssemblyInfo baseInfo, ReportType reportType, FileType fileType, string itemGroup)
        {
            this.baseInfo = baseInfo;
            this.reportType = reportType;
            this.fileType = fileType;
            this.itemGroup = itemGroup;
        }


        public void Generate()
        {
            var info = new DataTable();
            info.Columns.Clear();
            info.Columns.Add("S No");
            info.Columns.Add("Part Number");
            info.Columns.Add("Item Name");
            info.Columns.Add("Make");
            info.Columns.Add("Unit Qty");
            info.Columns.Add("Item Group");
            info.Columns.Add("Material");
            info.Columns.Add("Remark");
            info.Columns.Add("Unit Weight(Kg)");
            info.Columns.Add("Total Weight(Kg)");

            UpdateDataGrid(info, 1, baseInfo, this.itemGroup);


            //For Material Summary
            var materialInfo = new DataTable();
            materialInfo.Columns.Clear();
            materialInfo.Columns.Add("S No");
            materialInfo.Columns.Add("Material");
            materialInfo.Columns.Add("Total Weight(kg)");

            var weightInfo = new List<WeightInfo>();
            FillWeightInfo wght = new FillWeightInfo();
            weightInfo = wght.GetWeightInfo(baseInfo);
            int Sno = 1;
            foreach (var name in weightInfo)
            {
                materialInfo.Rows.Add(Sno, name.materialName, name.tWeight);
                Sno++;
            }

            //passing Assembly details and material summary datatable to the view!!
            PreviewViewModel previewViewModel = new PreviewViewModel(info, materialInfo, this.baseInfo, this.reportType, this.fileType, this.itemGroup);
            PreviewView previewView = new PreviewView();
            previewView.DataContext = previewViewModel;
            //show Preview
            previewView.ShowDialog();
        }



        /// <summary>
        /// for update the preview information
        /// </summary>
        /// <param name="info"></param>
        /// <param name="sno"></param>
        /// <param name="asmInfo"></param>
        /// <param name="prtType"></param>
        /// <param name="isModel"></param>
        private void UpdateDataGrid(DataTable info, int sno, AssemblyInfo asmInfo, string itemGroup)
        {
            if (!asmInfo.childs.Any())
            {
                return;
            }

            //asmInfo.childs = asmInfo.childs.OrderBy(o => o.pName).ToList();

            // var desc = (isModel ? "Model " : string.Empty) + asmInfo.PartNum + "-" + asmInfo.properties["DESCRIPTION"].StrValue;
            info.Rows.Add("", asmInfo.PartNum, asmInfo.properties["ITEM NAME"].StrValue, asmInfo.qty, "", "",
                       "", "", "", "");
            sno = 1;

            if (asmInfo.topLevelAsm.Any())
            {
                FillDataGrid(info, sno, asmInfo.topLevelAsm, itemGroup);
            }

            FillDataGrid(info, sno, asmInfo.childs, itemGroup);
        }



        /// <summary>
        /// for filling the child details of assembly in preview
        /// </summary>
        /// <param name="info"></param>
        /// <param name="sno"></param>
        /// <param name="childs"></param>
        /// <param name="prtType"></param>
        /// <param name="isPriority"></param>
        void FillDataGrid(DataTable info, int sno, List<AssemblyInfo> childs, string itemGroup)
        {

            foreach (var child in childs)
            {
                string tWeight = "";

                if (!String.IsNullOrWhiteSpace(child.uWeight))
                {
                    tWeight = (Convert.ToDouble(child.qty) * Convert.ToDouble(child.uWeight)).ToString();
                }

                if (child.properties["TYPE"].StrValue.Equals("Assembly", StringComparison.InvariantCultureIgnoreCase))
                {
                    info.Rows.Add(sno, child.PartNum, child.ItemName, child.Make,
                        child.qty, child.ItemGroup, child.Material, child.Remark, child.uWeight, tWeight);
                    sno++;
                }

            }

            foreach (var child in childs)
            {
                if (child.properties["TYPE"].StrValue.Equals("Assembly", StringComparison.InvariantCultureIgnoreCase))
                {
                    continue;
                }
                string tWeight = "";

                if (!String.IsNullOrWhiteSpace(child.uWeight))
                {
                    tWeight = (Convert.ToDouble(child.qty) * Convert.ToDouble(child.uWeight)).ToString();
                }

                info.Rows.Add(sno, child.PartNum, child.ItemName, child.Make,
                     child.qty, child.ItemGroup, child.Material, child.Remark, child.uWeight, tWeight);

                sno++;
            }

            info.Rows.Add("");
            foreach (var child in childs)
            {
                UpdateDataGrid(info, sno, child, itemGroup);
            }
        }




        /// <summary>
        /// function For create BOM
        /// </summary>
        /// <param name="progress"></param>
        public void GenerateExcelGen()
        {
           
            var weightInfo = new List<WeightInfo>();

            var id = GetAsm_id(baseInfo.PartNum);
            if(id < 1)
            {
                MessageBox.Show("First Save Assembly to Database");
                return;
            }
            var ver = GetLatestVersion(id);

            if (DoExportImages)
            {
                TemplateName = "templateCreateImg.xlsx";
            }
            else
            {
                TemplateName = "templateCreate.xlsx";
            }
           
            var workbook = ExcelHelper.GetXLWorkbook(true, out IXLWorksheet worKsheeT, TemplateName);
            int rowNo = 4;

            var colcount = ExcelHelper.GetColumncount(BOMType.General);

            var range = worKsheeT.Range(worKsheeT.Cell(1, 3).Address, worKsheeT.Cell(4, 12).Address);
            //worKsheeT.Cell(4, 3).Value = msg;
            worKsheeT.Cell(3, 1).Value = baseInfo.PartNum;
            worKsheeT.Cell(1, 1).Value = baseInfo.ItemName;

            if(TemplateName == "templateCreate.xlsx")
            {
                worKsheeT.Cell(3, 11).Value = ver;
                worKsheeT.Cell(1, 11).Value = DateTime.Now.ToString("dd-MM-yyyy");
            }

            else if(TemplateName == "templateCreateImg.xlsx")
            {
                worKsheeT.Cell(3, 12).Value = ver;
                worKsheeT.Cell(1, 12).Value = DateTime.Now.ToString("dd-MM-yyyy");
            }

            worKsheeT.Cell(1, 11).Value = DateTime.Now.ToString("dd-MM-yyyy");
            range.Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
            range.Style.Font.FontSize = 16;
            UpdateExcelCreateBOM(this.baseInfo, ref rowNo, worKsheeT, colcount);

            FillWeightInfo wght = new FillWeightInfo();
            weightInfo = wght.GetWeightInfo(baseInfo);
            FillWeightExcel(ref rowNo, worKsheeT, weightInfo);

            worKsheeT.Column(1).AdjustToContents();
            worKsheeT.Column(2).Width = 20;

            //func for save excel report
            new SaveXLFile().FileSaveCreateBOM(workbook);
        }



        /// <summary>
        /// 
        /// </summary>
        /// <param name="rowNo"></param>
        /// <param name="worKsheeT"></param>
        /// <param name="dt"></param>
        private void UpdateExcelCreateBOM(AssemblyInfo asmInfo, ref int rowNo, IXLWorksheet worKsheeT, int colCount)
        { 
            if (!asmInfo.childs.Any())
            {
                return;
            }

           
            if (asmInfo.PartNum == baseInfo.PartNum)
            {
                var id = GetAsm_id(asmInfo.PartNum);
                ver = GetLatestVersion(id);
            }
            

            //AssemblyInfo list = new AssemblyInfo();
            //if (!ver.Equals("V1", StringComparison.InvariantCultureIgnoreCase))
            //{
            //    list = ExtractExistingAsm(baseInfo.PartNum);
            //}
           

            //asmInfo.childs = asmInfo.childs.OrderBy(o => o.pName).ToList();
            var range = worKsheeT.Range(worKsheeT.Cell(rowNo, 1), worKsheeT.Cell(rowNo, colCount + 1));
            range.Merge();

            range.Style.Font.Bold = true;

            var border = range.Style.Border;
            border.SetOutsideBorder(XLBorderStyleValues.Thick);
            border.SetInsideBorder(XLBorderStyleValues.Thick);

            worKsheeT.Row(rowNo).Height = 20;
            range.Style.Font.FontSize = 16;
            range.Style.Font.Bold = true;
            var desc =  asmInfo.PartNum + "-" + asmInfo.properties["ITEM NAME"].StrValue;
            worKsheeT.Cell(rowNo, 1).Value = desc;
            range.Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
            if (rowNo > 4)
            {
                worKsheeT.Cell(rowNo, colCount + 2).Value = asmInfo.qty + "Nos";

            }
            range = worKsheeT.Range(worKsheeT.Cell(rowNo, colCount + 1), worKsheeT.Cell(rowNo, colCount + 2));
            border = range.Style.Border;
            border.SetOutsideBorder(XLBorderStyleValues.Thick);
            border.SetInsideBorder(XLBorderStyleValues.Thin);
            range.Style.Font.FontSize = 16;
            range.Style.Font.Bold = true;
            if (desc.Count() > 55)
            {
                //worKsheeT.Cell(rowNo, 1).RowHeight = worKsheeT.Cell(rowNo, 1).RowHeight * 2; //to do
            }


            if (asmInfo.topLevelAsm.Any())
            {
                FillExcelGen(ref rowNo, worKsheeT, asmInfo.topLevelAsm, ver);
                rowNo--;
            }
            FillExcelGen(ref rowNo, worKsheeT, asmInfo.childs, ver);

        }



        /// <summary>
        /// for filling the details of childs of assembly
        /// </summary>
        void FillExcelGen(ref int rowNo, IXLWorksheet worKsheeT, List<AssemblyInfo> childs, string asmVer)
        {
            rowNo++;

            var attNames = ExcelHelper.SetHeading(worKsheeT, rowNo, true, BOMType.General);
            var colCount = ExcelHelper.GetColumncount(BOMType.General);

            var range = worKsheeT.Range(worKsheeT.Cell(rowNo, 1), worKsheeT.Cell(rowNo, colCount + 2));
            var border = range.Style.Border;
            border.SetOutsideBorder(XLBorderStyleValues.Thick);
            border.SetInsideBorder(XLBorderStyleValues.Thick);
            range.Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
            range.Style.Font.FontSize = 14;
            range.Style.Font.Bold = true;
            rowNo++;

            var sno = 1;
            foreach (var child in childs)
            {
                var id = GetAsm_id(child.PartNum);
                var version = GetLatestVersion(id);

                AssemblyInfo list = new AssemblyInfo();

                if (!version.Equals("V1", StringComparison.InvariantCultureIgnoreCase))
                {
                    asmInfos = ExtractExistingAsm(child.PartNum);
                }

                //if (!String.IsNullOrEmpty(prtType) && !AssemblyTraverser.ValidateType(child, prtType))
                //{
                //    continue;
                //}
                worKsheeT.Cell(rowNo, 1).Value = sno;
                for (var index = 0; index < colCount; index++)
                {
                    var aatrib = child.properties[attNames[index]];
                    if (aatrib.Name.Equals("QTY"))
                    {
                        worKsheeT.Cell(rowNo, index + 2).Value = child.qty;
                        if (!version.Equals("V1", StringComparison.InvariantCultureIgnoreCase))
                        {
                            if (child.qty != asmInfos.qty)
                            {
                                worKsheeT.Cell(rowNo, index + 2).Style.Fill.BackgroundColor = XLColor.Yellow;
                            }
                        }
                    }
                    
                    else if (aatrib.Name.Equals("VERSION"))
                    {
                        worKsheeT.Cell(rowNo, index + 2).Value = version;
                    }

                    else if (aatrib.Name.Equals("IMAGE"))
                    {
                        range = worKsheeT.Range(worKsheeT.Cell(rowNo, index + 2).Address, worKsheeT.Cell(rowNo, index + 2).Address);
                        worKsheeT.Column(2).Width = 20;
                        worKsheeT.Row(rowNo).Height = 90;



                        var imagePath = Path.Combine(Path.GetTempPath(), "pic.JPG");
                        if (aatrib.Img != null)
                        {

                            var img = resizeImage(aatrib.Img, 110, 110);
                            img.Save(imagePath);

                            var pic = worKsheeT.AddPicture(imagePath);
                            pic.Placement = XLPicturePlacement.MoveAndSize;
                            pic.MoveTo(worKsheeT.Cell(rowNo, 2), worKsheeT.Cell(rowNo + 1, 3));

                        }
                    }
                    else
                    {
                        var val = child.properties[attNames[index]].StrValue;
                        worKsheeT.Cell(rowNo, index + 2).Value = val;
                        if (val == "-")
                        {
                            worKsheeT.Cell(rowNo, index + 2).Style.Fill.BackgroundColor = XLColor.Red;
                        }
                        if (!version.Equals("V1", StringComparison.InvariantCultureIgnoreCase))
                        { 
                            if (aatrib.Name.Equals("PART NUMBER"))
                            {
                                if (!val.Equals(asmInfos.PartNum, StringComparison.InvariantCultureIgnoreCase))
                                {
                                    var range2 = worKsheeT.Range(worKsheeT.Cell(rowNo, 1), worKsheeT.Cell(rowNo, colCount + 1));
                                    range2.Style.Fill.BackgroundColor = XLColor.Yellow;
                                }
                            }
                            else if (aatrib.Name.Equals("ITEM NAME"))
                            {
                                if (!val.Equals(asmInfos.ItemName, StringComparison.InvariantCultureIgnoreCase))
                                {
                                    worKsheeT.Cell(rowNo, index + 2).Style.Fill.BackgroundColor = XLColor.Yellow;
                                }
                            }
                            else if (aatrib.Name.Equals("MAKE"))
                            {
                                if (!val.Equals(asmInfos.Make, StringComparison.InvariantCultureIgnoreCase))
                                {
                                    worKsheeT.Cell(rowNo, index + 2).Style.Fill.BackgroundColor = XLColor.Yellow;
                                }
                            }
                            else if (aatrib.Name.Equals("MATERIAL"))
                            {
                                if (!val.Equals(asmInfos.Material, StringComparison.InvariantCultureIgnoreCase))
                                {
                                    worKsheeT.Cell(rowNo, index + 2).Style.Fill.BackgroundColor = XLColor.Yellow;
                                }
                            }
                          
                            else if (aatrib.Name.Equals("WEIGHT"))
                            {
                                if (!val.Equals(asmInfos.uWeight, StringComparison.InvariantCultureIgnoreCase))
                                {
                                    worKsheeT.Cell(rowNo, index + 2).Style.Fill.BackgroundColor = XLColor.Yellow;
                                }
                            }
                        }
                        if (string.IsNullOrEmpty(val) && !aatrib.Name.Equals("MATERIAL") && !aatrib.Name.Equals("REMARK"))
                        {
                            range = worKsheeT.Range(worKsheeT.Cell(rowNo, index + 2), worKsheeT.Cell(rowNo, index + 2));
                            var doColor = true;

                            if (child.ItemGroup.Equals("hardware", StringComparison.InvariantCultureIgnoreCase)
                                   ||
                                   child.ItemGroup.Equals("bop", StringComparison.InvariantCultureIgnoreCase))
                            {
                                if (aatrib.Name.Equals("revision", StringComparison.InvariantCultureIgnoreCase)
                                  ||
                                  aatrib.Name.Equals("weight", StringComparison.InvariantCultureIgnoreCase))
                                {
                                    doColor = false;
                                }
                            }

                            if (doColor)
                                range.Style.Fill.BackgroundColor = XLColor.Red;
                        }
                    }
                }
              

                if (child.PartNum == "-")
                {
                    range.Style.Fill.BackgroundColor = XLColor.Red;
                }


                if (double.TryParse(child.uWeight, out var calc))
                {
                    worKsheeT.Cell(rowNo, colCount + 2).Value = calc * child.qty;
                }
                else
                {
                    range = worKsheeT.Range(worKsheeT.Cell(rowNo, colCount + 2), worKsheeT.Cell(rowNo, colCount + 2));
                    if (!child.ItemGroup.Equals("hardware", StringComparison.InvariantCultureIgnoreCase)
                      &&
                      !child.ItemGroup.Equals("bop", StringComparison.InvariantCultureIgnoreCase))
                    {
                        range.Style.Fill.BackgroundColor = XLColor.Red;
                    }
                }


                range = worKsheeT.Range(worKsheeT.Cell(rowNo, 1), worKsheeT.Cell(rowNo, colCount + 2));
                worKsheeT.Column(1).AdjustToContents();
                worKsheeT.Columns(3, colCount + 2).AdjustToContents();
                border = range.Style.Border;
                border.SetOutsideBorder(XLBorderStyleValues.Thick);
                border.SetInsideBorder(XLBorderStyleValues.Thick);
                range.Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Left);
                worKsheeT.Cell(rowNo, 3).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Left);
                range.Style.Font.FontSize = 12;
                rowNo++;
                sno++;


            }

            rowNo++;
            foreach (var child in childs)
            {
                UpdateExcelCreateBOM(child, ref rowNo, worKsheeT, colCount);
            }
        }



        /// <summary>
        /// for weight & material summary
        /// </summary>
        static void FillWeightExcel(ref int rowNo, IXLWorksheet worKsheeT, List<WeightInfo> weightInfo)
        {
            rowNo++;
            var range = worKsheeT.Range(worKsheeT.Cell(rowNo, 2).Address, worKsheeT.Cell(rowNo, 3).Address);
            //range.Merge();
            // range.Style.Alignment.WrapText = true;
            range.Style.Font.Bold = true;

            var border = range.Style.Border;
            border.SetOutsideBorder(XLBorderStyleValues.Medium);
            // border.Weight = 2d;

            range.Style.Font.FontSize = 14;
            range.Style.Font.Bold = true;
            worKsheeT.Cell(rowNo, 2).Value = "Material";
            range.Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);

            range.Style.Font.FontSize = 14;
            range.Style.Font.Bold = true;
            worKsheeT.Cell(rowNo, 3).Value = "Weight (Kg)";
            range.Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);

            foreach (var info in weightInfo)
            {
                rowNo++;
                range = worKsheeT.Range(worKsheeT.Cell(rowNo, 2).Address, worKsheeT.Cell(rowNo, 2).Address);
                border = range.Style.Border;
                border.SetOutsideBorder(XLBorderStyleValues.Medium);



                //border.Weight = 2d;
                range.Style.Font.FontSize = 12;
                range.Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Left);
                worKsheeT.Cell(rowNo, 2).Value = info.materialName;

                range = worKsheeT.Range(worKsheeT.Cell(rowNo, 3).Address, worKsheeT.Cell(rowNo, 3).Address);
                border = range.Style.Border;
                border.SetOutsideBorder(XLBorderStyleValues.Medium);


                //border.Weight = 2d;
                range.Style.Font.FontSize = 12;
                range.Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Left);
                worKsheeT.Cell(rowNo, 3).Value = info.tWeight;


            }

            rowNo++;

        }



        /// <summary>
        /// 
        /// </summary>
        public static Image resizeImage(Image image, int new_height, int new_width)
        {
            Bitmap new_image = new Bitmap(new_width, new_height);
            Graphics g = Graphics.FromImage((Image)new_image);
            g.InterpolationMode = InterpolationMode.High;
            g.DrawImage(image, 0, 0, new_width, new_height);
            return new_image;
        }



        /// <summary>
        /// get assembly info from database
        /// </summary>
        /// <param name="asmPNum"></param>
        private AssemblyInfo ExtractExistingAsm(string asmPNum)
        {
            string sql = @"SELECT [child_id],[AsmVersion] FROM [dbo].[Asm_ChildInfo_History] where child_id = (select [id] from [dbo].[Assembly_info] where part_number = '" + asmPNum + "')";
            DataTable tableAsmInfo;
            using (con = new SqlConnection(SQLConnection.connection_string))
            {
                using (SqlCommand cmd = new SqlCommand(sql, con))
                {
                    cmd.CommandType = CommandType.Text;
                    using (Adapter = new SqlDataAdapter(cmd))
                    {
                        using (dt = new DataTable())
                        {
                            Adapter.Fill(dt);
                        }
                    }

                    if (dt == null ||
                        dt.Rows.Count == 0)
                    {
                        return null;
                    }
                } // using
            } // using


            var asmId = new AsmID { id = Convert.ToInt32(dt.Rows[0][0]) };
            var asmVersion = new AsmID { asmVersion = Convert.ToInt32(dt.Rows[dt.Rows.Count - 1][1]) };


            // do not read chils, as diolg is loaded, read child while comparing specific asm
            var childs = AddChilds(asmId, asmVersion, out var asmInfo, true);
            asmId.childIds = childs;
            asmIdsWithChild.Add(asmId);
            return asmInfo;
        }



        /// <summary>
        /// get info of existing assembly from database
        /// </summary>
        /// <param name="asmId"></param>
        /// <param name="asmInfo"></param>
        /// <returns></returns>
        public static List<AsmID> AddChilds(AsmID asmId, AsmID asmVersion, out AssemblyInfo asmInfo, bool readChild)
        {
            DataTable tableAsmInfo;
            DataTable tableAssemblyInfo;
            var childs = new List<AsmID>();
            var child_select = "select [child_id] from Asm_ChildInfo_History where assembly_id = '" + asmId.id + "' and AsmVersion = '" + asmVersion.asmVersion + "' ";
            DataTable childTable;
            using (var con = new SqlConnection(SQLConnection.connection_string))
            {
                using (SqlCommand cmd = new SqlCommand(child_select, con))
                {
                    cmd.CommandType = CommandType.Text;
                    using (var Adapter = new SqlDataAdapter(cmd))
                    {
                        using (childTable = new DataTable())
                        {
                            Adapter.Fill(childTable);
                        }
                    }
                } // using

                using (SqlCommand cmd = new SqlCommand(asm_info_table + asmId.id, con))
                {
                    cmd.CommandType = CommandType.Text;
                    using (var Adapter = new SqlDataAdapter(cmd))
                    {
                        using (tableAssemblyInfo = new DataTable())
                        {
                            Adapter.Fill(tableAssemblyInfo);
                        }
                    }
                } // using

                using (SqlCommand cmd = new SqlCommand(asm_info + asmId.id, con))
                {
                    cmd.CommandType = CommandType.Text;
                    using (var Adapter = new SqlDataAdapter(cmd))
                    {
                        using (tableAsmInfo = new DataTable())
                        {
                            Adapter.Fill(tableAsmInfo);
                        }
                    }
                } // using
            } // using

            if (tableAssemblyInfo.Rows.Count == 0)
            {
                asmInfo = null;
                return null;
            }

            asmInfo = GetAsmInfo(tableAsmInfo.Rows[tableAsmInfo.Rows.Count-2], tableAssemblyInfo.Rows[0]);

            if (!readChild)
            {
                return null;
            }

            foreach (DataRow childData in childTable.Rows)
            {
                var asmChild = new AsmID
                {
                    id = Convert.ToInt32(childData["child_id"])
                };

                if (asmChild.id == asmId.id)
                {
                    continue;
                }
                childs.Add(asmChild);

                var childIds = AddChilds(asmChild, asmVersion, out var child, true);
                asmChild.childIds = childIds;
                //child.qty = asmChild.count;
                asmInfo.childs.Add(child);
            } // foreach

            return childs;
        }



        /// <summary>
        /// fill the properties of assembly in a list
        /// </summary>
        /// <param name="row"></param>
        /// <returns></returns>
        private static AssemblyInfo GetAsmInfo(DataRow row, DataRow table)
        {
            var asmInfo = new AssemblyInfo();
            asmInfo.properties["PART NUMBER"].StrValue = table["part_number"].ToString();
            asmInfo.properties["ITEM NAME"].StrValue = table["item_name"].ToString();
            asmInfo.qty = Convert.ToInt16(row["Unit_qty"]);
            asmInfo.properties["CLIENT"].StrValue = Convert.ToString(row["client_name"]);
            asmInfo.properties["ORIGIN"].StrValue = row["origin"].ToString();
            asmInfo.properties["MASTER PROJECT"].StrValue = row["master_project"].ToString();
            asmInfo.properties["MODEL NUMBER"].StrValue = row["project_number"].ToString();
            asmInfo.properties["MAKE"].StrValue = row["make"].ToString();
            asmInfo.properties["MATERIAL"].StrValue = row["Material"].ToString();
            asmInfo.properties["DESIGN BY"].StrValue = row["design_by"].ToString();
            asmInfo.properties["Design Date"].StrValue = row["design_date"].ToString();
            asmInfo.properties["DRAWN BY"].StrValue = row["drawn_by"].ToString();
            asmInfo.properties["Drawn Date"].StrValue = row["drawn_date"].ToString();
            asmInfo.properties["WEIGHT"].StrValue = row["UnitWeight"].ToString();
            asmInfo.path = table["path_str"].ToString();

            return asmInfo;
        }



        /// <summary>
        /// 
        /// </summary>
        /// <param name="asm_id"></param>
        /// <returns></returns>
        public static string GetLatestVersion(int asm_id)
        {
            string getVer = @"select [id],[Version],[IsStandard] from [dbo].[Versions] where Asm_id = '" + asm_id + "' ";

            using (SqlConnection con = new SqlConnection(SQLConnection.connection_string))
            {
                DataTable dt = new DataTable();
                using (SqlCommand cmd = new SqlCommand(getVer, con))
                {
                    cmd.CommandType = CommandType.Text;
                    using (SqlDataAdapter Adapter = new SqlDataAdapter(cmd))
                    {
                        using (dt = new DataTable())
                        {
                            Adapter.Fill(dt);
                        }
                    }

                    if (dt.Rows.Count == 0)
                    {
                        return null;
                    }
                    else
                    {
                        return dt.Rows[dt.Rows.Count-1][1].ToString();
                    }
                } // using
            } // using
        }




        /// <summary>
        /// 
        /// </summary>
        /// <param name="pName"></param>
        /// <returns></returns>
        public static int GetAsm_id(string pName)
        {
            string sqlSelect = @"SELECT [id] FROM [dbo].[Assembly_info] where  part_number = '" + pName + "' ";
            using (SqlConnection con = new SqlConnection(SQLConnection.connection_string))
            {
                DataTable dt = new DataTable();
                using (SqlCommand cmd = new SqlCommand(sqlSelect, con))
                {
                    cmd.CommandType = CommandType.Text;
                    using (SqlDataAdapter Adapter = new SqlDataAdapter(cmd))
                    {
                        using (dt = new DataTable())
                        {
                            Adapter.Fill(dt);
                        }
                    }

                    if (dt.Rows.Count == 0)
                    {
                        return 0;
                    }
                    else
                    {
                        return Convert.ToInt32(dt.Rows[0][0]);
                    }
                } // using
            } // using
        }
    }

    public class AsmID
    {
        public int id;
        public List<AsmID> childIds;
        public int count;
        public int asmVersion;
    }
}
