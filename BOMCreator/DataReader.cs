﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace BOMCreator.DataReader
{

  public static class DataReader
  {
    private static DataSet data = new DataSet();
    //OleDbConnection conn;
    //string connectionString;

    public static bool LoadData()
    {

      try
      {
        string assemblyFolder = System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
        string xmlFileName = System.IO.Path.Combine(assemblyFolder, "Stock Details.xlsx");
        // xmlFileName = System.IO.Path.Combine("d:\\", "ItemMaster.xlsx");

        // if the File extension is .XLSX using below connection string
        var connectionString = "Provider=Microsoft.ACE.OLEDB.12.0;" +
                        "Data Source='" + xmlFileName +
                        "';Extended Properties=\"Excel 12.0;HDR=YES;\"";

        foreach (var sheetName in GetExcelSheetNames(connectionString))
        {
          using (OleDbConnection con = new OleDbConnection(connectionString))
          {
            var dataTable = new DataTable();
            string query = string.Format("SELECT * FROM [{0}]", sheetName);
            con.Open();
            OleDbDataAdapter adapter = new OleDbDataAdapter(query, con);
            adapter.Fill(dataTable);
            dataTable.TableName = sheetName.ToUpper();
            data.Tables.Add(dataTable);
          }
        }
      }
      catch (Exception ex)
      {
        return false;
      }
      return true;
    }


    public static double GetQuantity(string stockType,
                                     double thickness,
                                      double width,
                                     double height,
                                     double length,
                                     double diameter,

                                     out string desc)
    {
      desc = string.Empty;
      var tName = stockType.Contains(" ") ? "'" + stockType + "$'" : stockType + "$";
      var index = data.Tables.IndexOf(tName);
      if (index < 0)
      {
        return 0;
      }
      var qty = 0.0;
      var table = data.Tables[index];

      switch (stockType.ToUpper())
      {
        case "FLAT":
          {
            var rows = table.AsEnumerable();
            rows = GetMatchingRows(rows, "Thickness", thickness);
            rows = GetMatchingRows(rows, "Width", width);
            if (rows.Any())
            {
              var row = rows.FirstOrDefault();
              if (row != null)
              {
                desc = row.Field<string>("Description");
                var len = row.Field<double>("Length");
                if (len > 0)
                {
                  qty = len / (length + 5);
                }
              }
            }
          }
          break;

        case "ROUND":
        case "HEX":
        case "NYLON ROUND":
          {
            var rows = table.AsEnumerable();
            rows = GetMatchingRows(rows, "Diameter", diameter);
            if (rows.Any())
            {
              var row = rows.FirstOrDefault();
              if (row != null)
              {
                desc = row.Field<string>("Description");
                var len = row.Field<double>("Length");
                if (len > 0)
                {
                  qty = len / (length + 5);
                }
              }
            }
          }
          break;

        case "SS SQUARE TUBE":
        case "MS SQUARE TUBE":
        case "MS RECTANGULAR TUBE":
        case "ALUMINIUM PROFILE":
          {
            var rows = table.AsEnumerable();

            rows = GetMatchingRows(rows, "Width", width);
            rows = GetMatchingRows(rows, "Height", height);
            if (rows.Any())
            {
              var row = rows.FirstOrDefault();
              if (row != null)
              {
                desc = row.Field<string>("Description");
                var len = row.Field<double>("Length");
                if (len > 0)
                {
                  qty = len / (length + 5);
                }
              }
            }
          }
          break;

        case "SS SHEET":
        case "MS SHEET":
        case "CHEQUER PLATE":
        case "ACRYLIC SHEET":
        case "COPPER SHEET":
        case "BACKALITE SHEET":
        case "ASBESTOS SHEET":
        case "MS PLATE":
          {
            var rows = table.AsEnumerable();
            rows = GetMatchingRows(rows, "Thickness", thickness);
            if (rows.Any())
            {
              var row = rows.FirstOrDefault();
              if (row != null)
              {
                desc = row.Field<string>("Description");
                var len = row.Field<double>("Length");
                var w = row.Field<double>("Width");
                var area = length * width;
                if (len * w > 0)
                {
                  qty = len * w / area;
                }
              }
            }
          }
          break;

      }

      return Math.Ceiling(qty);
    }

    public static EnumerableRowCollection<DataRow> GetMatchingRows(EnumerableRowCollection<DataRow> rows, string colName, double val)
    {
      //var rows = table.AsEnumerable();
      var tRows = rows.Where(r => r.Field<double?>(colName).isEqualTo(val));
      if (!tRows.Any())
      {
        tRows = rows.Where(r => r.Field<double?>(colName) > val);
        var nextVal = tRows.Min(r => r.Field<double?>(colName));
        tRows = rows.Where(r => r.Field<double?>(colName).isEqualTo(nextVal.Value));
      }

      return tRows;
    }

    public static bool isEqualTo(this double? val, double valToCompare, double tol = 0.01)
    {
      return val.HasValue ? Math.Abs(val.Value - valToCompare) < 0.01 : false;
    }

    public static bool isGreaterThan(this double val, double valToCompare, double tol = 0.01)
    {
      return Math.Abs(val - valToCompare) < 0.01;
    }

    static string[] GetExcelSheetNames(string connectionString)
    {
      OleDbConnection con = null;
      DataTable dt = null;
      con = new OleDbConnection(connectionString);
      con.Open();
      dt = con.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, new object[] { null, null, null, "TABLE" });

      if (dt == null)
      {
        return null;
      }

      string[] excelSheetNames = new string[dt.Rows.Count];
      int i = 0;

      foreach (DataRow row in dt.Rows)
      {
        excelSheetNames[i] = row["TABLE_NAME"].ToString();
        i++;
      }

      return excelSheetNames;
    }
  }
}
