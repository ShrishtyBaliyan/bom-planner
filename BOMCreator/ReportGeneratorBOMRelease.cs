﻿
using BOMCreator.ViewModel;
using BOMPlanner.SQL;
using CadData;
using ClosedXML.Excel;
using ReleaseBOM;
using ReleaseBOM.Sql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Windows;

namespace BOMCreator
{
    public class ReportGeneratorBOMRelease : IReportGenerator
    {
        //stores the value of Required Quantity
        string reqqty;

        //stores the value of Release Quantity
        string releaseqty;

        //stores the value of Total Release Quantity
        string TotalReleaseqty;

        //stores the value of Previous Release Quantity
        string PreviousReleaseqty;

        //stores the value of Set Due Date 
        string duedate;

        //stores the value of Last Receive By 
        string LastReceiveBy = string.Empty;

        //stores the value of Last Receive Date 
        string LastReceiveDate = string.Empty;

        //stores the value of Last Release By 
        string LastReleaseBy = string.Empty;

        //stores the value of Last Release Date
        string LastReleaseDate = string.Empty;

        //stores the value of Custom Release
        string customRel;

        //stores the value of ReleaseBy
        string releaseBy;

        //stores the value of Total Quantity
        string totalQty;

        //stores the value of Balance Quantity
        string balanceQty;

        //stores the details of Main Assembly
        AssemblyInfo baseInfo;

        /// <summary>
        /// stores the value of ReportType
        /// </summary>
        ReportType reportType;

        /// <summary>
        /// stores the value of FileType
        /// </summary>
        FileType fileType;

        /// <summary>
        /// stores the value of ItemGroup
        /// </summary>
        string itemGroup;



        /// <summary>
        /// true if template needs to be used,else false
        /// </summary>
        public bool DoUseTemplate { get; set; }



        /// <summary>
        /// stores template name
        /// </summary>
        public string TemplateName { get; set; }


        /// <summary>
        /// stores Part Code
        /// </summary>
        private string partCode;
        public string PartCode
        {
            get => this.partCode;
            set
            {
                this.partCode = value;
            }
        }


        /// <summary>
        /// 
        /// </summary>
        public List<ReleaseModel> releaseBOMList = new List<ReleaseModel>();

        /// <summary>
        /// 
        /// </summary>
        public List<ReleaseModel> releaseList = new List<ReleaseModel>();


        /// <summary>
        /// 
        /// </summary>
        /// <param name="releaseBOMList"></param>
        /// <param name="baseInfo"></param>
        public ReportGeneratorBOMRelease(List<ReleaseModel> releaseBOMList, AssemblyInfo baseInfo)
        {
            this.releaseBOMList = releaseBOMList;
            this.baseInfo = baseInfo;
        }



        /// <summary>
        /// 
        /// </summary>
        /// <param name="baseInfo"></param>
        /// <param name="reportType"></param>
        /// <param name="fileType"></param>
        /// <param name="itemGroup"></param>
        public ReportGeneratorBOMRelease( AssemblyInfo baseInfo,
                                              ReportType reportType,
                                              FileType fileType,
                                              string itemGroup)
        {
            this.baseInfo = baseInfo;
            this.reportType = reportType;
            this.fileType = fileType;
            this.itemGroup = itemGroup;
        }


        /// <summary>
        /// func for showing assembly info list in Preview
        /// </summary>
        /// <param name="progress"></param>
        public void Generate()
        {
            try
            {
                List<ReleaseModel> info = new List<ReleaseModel>();
                List<ReleaseModel> DataList = new List<ReleaseModel>();
                UpdateDataGrid(info, "1", baseInfo, this.releaseBy);
                if (!UpdateDGList(info, DataList))
                {
                    return;
                }

                //passing Assembly details as a list to the view!!
                ReleaseBOMViewModel designViewModel = new ReleaseBOMViewModel(DataList, this.baseInfo, this.releaseBy, this.TotalReleaseqty);
                var releaseView = new ReleaseView();
                releaseView.DataContext = designViewModel;
                //show Preview
                releaseView.ShowDialog();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }



        /// <summary>
        /// update the values in datagrid list according to the values that are stored in database
        /// </summary>
        /// <param name="info"></param>
        /// <param name="DataList"></param>
        private bool UpdateDGList(List<ReleaseModel> info, List<ReleaseModel> DataList)
        {
            var parentName = string.Empty;
            foreach (var item in info)
            {
                if (String.IsNullOrWhiteSpace(item.SNO) && (!item.PartNumber.Equals(baseInfo.PartNum, StringComparison.InvariantCultureIgnoreCase)))
                {
                    parentName = item.PartNumber;
                    DataList.Add(new ReleaseModel()
                    {
                        SNO = item.SNO,
                        PartNumber = item.PartNumber,
                        ItemName = item.ItemName,
                        UnitQty = item.UnitQty,
                        RequiredQty = "",
                        TotalQty = "",
                        ReleaseQty = "",
                        ReleaseDate = "",
                        ReleaseBy = "",
                        TotalReleasedQty = "",
                        PreviousReleasedQty = "",
                        BalanceQty = "",
                        CustomRelease = "",
                        DueDate = "",
                        LastReceivedBy = "",
                        LastReceivedDate = "",
                        PreviousReleasedBy = "",
                        PreviousReleasedDate = ""
                    });
                    continue;
                }
                else if (String.IsNullOrWhiteSpace(item.SNO) && (item.PartNumber.Equals(baseInfo.PartNum, StringComparison.InvariantCultureIgnoreCase)))
                {
                    parentName = item.PartNumber;
                }
                else if (String.IsNullOrWhiteSpace(item.SNO) && String.IsNullOrWhiteSpace(item.PartNumber))
                {
                    DataList.Add(new ReleaseModel()
                    {
                        SNO = item.SNO,
                        PartNumber = item.PartNumber,
                        ItemName = item.ItemName,
                        UnitQty = item.UnitQty,
                        RequiredQty = item.RequiredQty,
                        TotalQty = item.TotalQty,
                        ReleaseQty = item.ReleaseQty,
                        ReleaseDate = item.ReleaseDate,
                        ReleaseBy = item.ReleaseBy,
                        TotalReleasedQty = item.TotalReleasedQty,
                        PreviousReleasedQty = item.PreviousReleasedQty,
                        BalanceQty = item.BalanceQty,
                        CustomRelease = item.CustomRelease,
                        DueDate = item.DueDate,
                        ReceiveBy = item.ReceiveBy,
                        ReceiveDate = item.ReceiveDate,
                        LastReceivedBy = item.LastReceivedBy,
                        LastReceivedDate = item.LastReceivedDate,
                        PreviousReleasedBy = item.PreviousReleasedBy,
                        PreviousReleasedDate = item.PreviousReleasedDate
                    });
                }
                var ReleaseBOMid = ReleaseBOMSqlQueries.GetAsmChildId(parentName, item.PartNumber);
                if(ReleaseBOMid < 0)
                {
                    MessageBox.Show("Please first save Assembly!!!");
                    return false;
                }

                DataTable ReleaseBOM = ReleaseBOMSqlQueries.GetReleaseBOMinfo(ReleaseBOMid);
                DataTable dt = new DataTable();
                var rDt = ReleaseBOMSqlQueries.GetAsmId(ReleaseBOMid);
                var parent = Convert.ToInt32(rDt.Rows[0][0]);
                var child = Convert.ToInt32(rDt.Rows[0][1]);
                var childHisId = ReleaseBOMSqlQueries.GetAsmchildHistory(parent, child);
                if (ReleaseBOM == null)
                {
                    if (childHisId > -1)
                    {
                        ReleaseBOM = ReleaseBOMSqlQueries.GetReleaseBOMinfo(childHisId);
                    }
                }
                
                dt = ReleaseBOMSqlQueries.GetDueDate(ReleaseBOMid);
                if (dt == null)
                {
                    dt = ReleaseBOMSqlQueries.GetDueDate(childHisId);
                    if (dt == null)
                    {
                        MessageBox.Show("Please Set Due Date First!");
                        return false;
                    }
                }
                this.duedate = dt.Rows[dt.Rows.Count-1][0].ToString();
                FillFinalList(ReleaseBOM, DataList, item);
            }
            return true;
        }



        /// <summary>
        /// fill the values in datagrid list according to the values that are stored in database
        /// </summary>
        /// <param name="ReleaseBOMid"></param>
        /// <param name="DataList"></param>
        /// <param name="item"></param>
        public void FillFinalList(DataTable ReleaseBOM, List<ReleaseModel> DataList, ReleaseModel item)
        {

            if (ReleaseBOM != null)
            {
                reqqty = ReleaseBOM.Rows[0][0].ToString();
                TotalReleaseqty = ReleaseBOM.Rows[0][1].ToString();
                customRel = ReleaseBOM.Rows[0][2].ToString();
                LastReceiveBy = ReleaseBOM.Rows[0][3].ToString();
                LastReceiveDate = ReleaseBOM.Rows[0][4].ToString();
                LastReleaseDate = ReleaseBOM.Rows[0][4].ToString();
                LastReleaseBy = ReleaseBOM.Rows[0][5].ToString();
                PreviousReleaseqty = ReleaseBOM.Rows[0][6].ToString();
                

                totalQty = ((Convert.ToInt32(reqqty)+ Convert.ToInt32(customRel)) * Convert.ToInt32(item.UnitQty)).ToString();
                balanceQty = (Convert.ToInt32(totalQty) - (Convert.ToInt32(TotalReleaseqty) + Convert.ToInt32(releaseqty))).ToString();
            }
            else
            {
                totalQty = item.UnitQty;
                balanceQty = totalQty;
            }

            releaseBy = SQLQueries.UserInfo();

            DataList.Add(new ReleaseModel()
            {
                SNO = item.SNO,
                PartNumber = item.PartNumber,
                ItemName = item.ItemName,
                UnitQty = item.UnitQty,
                RequiredQty = reqqty,
                TotalQty = totalQty,
                ReleaseQty = "0",
                ReleaseDate = DateTime.Now.ToString(),
                ReleaseBy = releaseBy,
                TotalReleasedQty = TotalReleaseqty,
                PreviousReleasedQty = PreviousReleaseqty,
                BalanceQty = balanceQty,
                CustomRelease = customRel,
                DueDate = this.duedate,
                ReceiveBy = item.ReceiveBy,
                ReceiveDate = DateTime.Now.ToString(),
                LastReceivedBy = LastReceiveBy,
                LastReceivedDate = LastReceiveDate,
                PreviousReleasedBy = LastReleaseBy,
                PreviousReleasedDate = LastReleaseDate

            });

        }


        /// <summary>
        /// for update the datagrid values 
        /// </summary>
        /// <param name="info"></param>
        /// <param name="sno"></param>
        /// <param name="asmInfo"></param>
        /// <param name="prtType"></param>
        /// <param name="isModel"></param>
        private void UpdateDataGrid(List<ReleaseModel> info, string sno, AssemblyInfo asmInfo, string releaseBy)
        {
            if (!asmInfo.childs.Any())
            {
                return;
            }

            releaseBy = SQLQueries.UserInfo();

            var desc = asmInfo.PartNum;
            info.Add(new ReleaseModel()
            {
                SNO = "",
                PartNumber = desc,
                ItemName = asmInfo.ItemName,
                UnitQty = asmInfo.qty.ToString(),
                RequiredQty = reqqty,
                TotalQty = totalQty,
                ReleaseQty = "0",
                ReleaseDate = DateTime.Now.ToString(),
                ReleaseBy = releaseBy,
                TotalReleasedQty = TotalReleaseqty,
                PreviousReleasedQty = PreviousReleaseqty,
                BalanceQty = balanceQty,
                CustomRelease = customRel,
                DueDate = this.duedate,
                ReceiveBy = "",
                ReceiveDate = DateTime.Now.ToString(),
                LastReceivedBy = LastReceiveBy,
                LastReceivedDate = LastReceiveDate,
                PreviousReleasedBy = LastReleaseBy,
                PreviousReleasedDate = LastReleaseDate
            });

            sno = "1";
            if (asmInfo.topLevelAsm.Any())
            {
                FillDataGrid(info, sno, asmInfo.topLevelAsm, releaseBy, asmInfo.PartNum);

            }
            FillDataGrid(info, sno, asmInfo.childs, releaseBy, asmInfo.PartNum);
        }



        /// <summary>
        /// for fill the datagrid values
        /// </summary>
        /// <param name="info"></param>
        /// <param name="sno"></param>
        /// <param name="childs"></param>
        /// <param name="prtType"></param>
        /// <param name="isPriority"></param>
        void FillDataGrid(List<ReleaseModel> info, string sno, List<AssemblyInfo> childs, string releaseBy, string parent)
        {
            foreach (var child in childs)
            {
                if(child.ItemGroup.Equals("BOP", StringComparison.InvariantCultureIgnoreCase) || child.ItemGroup.Equals("Hardware", StringComparison.InvariantCultureIgnoreCase))
                {
                    continue;
                }

                info.Add(new ReleaseModel()
                {
                    SNO = sno,
                    PartNumber = child.PartNum,
                    ItemName = child.ItemName,
                    UnitQty = child.qty.ToString(),
                    RequiredQty = reqqty,
                    TotalQty = totalQty,
                    ReleaseQty = "0",
                    ReleaseDate = DateTime.Now.ToString(),
                    ReleaseBy = releaseBy,
                    TotalReleasedQty = TotalReleaseqty,
                    PreviousReleasedQty = PreviousReleaseqty,
                    BalanceQty = balanceQty,
                    CustomRelease = customRel,
                    DueDate = this.duedate,
                    ReceiveBy = "",
                    ReceiveDate = DateTime.Now.ToString(),
                    LastReceivedBy = LastReceiveBy,
                    LastReceivedDate = LastReceiveDate,
                    PreviousReleasedBy = LastReleaseBy,
                    PreviousReleasedDate = LastReleaseDate
                });

                var Sno = Convert.ToInt32(sno);
                Sno++;
                sno = Sno.ToString();
            }
            info.Add(new ReleaseModel());
            foreach (var child in childs)
            {
                UpdateDataGrid(info, sno, child, releaseBy);
            }
        }





        /// <summary>
        /// function For generate excel of Release BOM
        /// </summary>
        /// <param name="progress"></param>
        public void GenerateExcelGen(string releaseBy, bool diffRelease, string receiveby, bool QR, bool pdf)
        {
            var templatename = "templateRelease.xlsx";
            var workbook = ExcelHelper.GetXLWorkbook(true, out IXLWorksheet worKsheeT, templatename);
            int rowNo = 4;

            var id = ReportGeneratorGeneral.GetAsm_id(baseInfo.PartNum);
            var ver = ReportGeneratorGeneral.GetLatestVersion(id);

            var range = worKsheeT.Range(worKsheeT.Cell(1, 3).Address, worKsheeT.Cell(4, 17).Address);
            //worKsheeT.Cell(4, 3).Value = msg;
            worKsheeT.Cell(3, 1).Value = baseInfo.PartNum;
            worKsheeT.Cell(2, 1).Value = baseInfo.ItemName;
            worKsheeT.Cell(3, 18).Value = ver;
            worKsheeT.Cell(1, 18).Value = DateTime.Now.ToString("dd-MM-yyyy");
            range.Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);

            foreach (var item in releaseBOMList)
            {
                if (string.IsNullOrWhiteSpace(item.PartNumber) || string.IsNullOrWhiteSpace(item.UnitQty))
                {
                    continue;
                }

                releaseqty = item.ReleaseQty;

                if (string.IsNullOrWhiteSpace(releaseqty))
                {
                    releaseqty = "0";
                }

                if (string.IsNullOrWhiteSpace(item.TotalReleasedQty))
                {
                    TotalReleaseqty = (Convert.ToInt32("0") + Convert.ToInt32(releaseqty)).ToString(); 
                }
                else
                {
                    TotalReleaseqty = (Convert.ToInt32(item.TotalReleasedQty) + Convert.ToInt32(item.ReleaseQty)).ToString();
                }

                if (string.IsNullOrWhiteSpace(item.CustomRelease))
                {
                    customRel = "0";
                }
                else
                {
                    customRel = item.CustomRelease;
                }

                reqqty = item.RequiredQty;
                if (string.IsNullOrWhiteSpace(reqqty))
                {
                    reqqty = "0";
                }
                totalQty = ((Convert.ToInt32(reqqty) + Convert.ToInt32(customRel)) * Convert.ToInt32(item.UnitQty)).ToString();
                balanceQty = (Convert.ToInt32(totalQty) - (Convert.ToInt32(TotalReleaseqty))).ToString();

                releaseList.Add(new ReleaseModel()
                {
                    SNO = item.SNO,
                    PartNumber = item.PartNumber,
                    ItemName = item.ItemName,
                    UnitQty = item.UnitQty,
                    RequiredQty = reqqty,
                    TotalQty = totalQty,
                    ReleaseQty = releaseqty,
                    ReleaseDate = DateTime.Now.ToString(),
                    ReleaseBy = releaseBy,
                    TotalReleasedQty = TotalReleaseqty,
                    PreviousReleasedQty = PreviousReleaseqty,
                    BalanceQty = balanceQty,
                    CustomRelease = customRel,
                    DueDate = item.DueDate,
                    ReceiveBy = receiveby,
                    ReceiveDate = DateTime.Now.ToString(),
                    LastReceivedBy = item.LastReceivedBy,
                    LastReceivedDate = item.LastReceivedDate,
                    PreviousReleasedBy = item.PreviousReleasedBy,
                    PreviousReleasedDate = item.PreviousReleasedDate
                });
            }


            UpdateExcelGen(ref rowNo, worKsheeT, this.releaseBOMList);

            worKsheeT.Column(1).AdjustToContents();
            worKsheeT.Column(2).Width = 20;

            //func for save excel report
            new SaveXLFile().FileSave(workbook, this.releaseList, this.baseInfo, QR, pdf);

        }



        /// <summary>
        /// func for update excel worksheet of ReleaseBOM
        /// </summary>
        /// <param name="asmInfo"></param>
        /// <param name="rowNo"></param>
        /// <param name="worKsheeT"></param>
        /// <param name="colCount"></param>
        /// <param name="prtType"></param>
        /// <param name="isModel"></param>
        private void UpdateExcelGen(ref int rowNo, IXLWorksheet worKsheeT, List<ReleaseModel> releaseBOMList)
        {
            var range = worKsheeT.Range(worKsheeT.Cell(rowNo, 1), worKsheeT.Cell(rowNo, 18));
            var border = range.Style.Border;
            border.SetOutsideBorder(XLBorderStyleValues.Thick);
            border.SetInsideBorder(XLBorderStyleValues.Thick);
            range.Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
            range.Style.Font.FontSize = 10;
            range.Style.Font.Bold = true;

            worKsheeT.Cell(rowNo, 1).Value = "SNO";
            worKsheeT.Cell(rowNo, 2).Value = "Part Number";
            worKsheeT.Cell(rowNo, 3).Value = "Item Name";
            worKsheeT.Cell(rowNo, 4).Value = "Unit Qty";
            worKsheeT.Cell(rowNo, 5).Value = "Required Qty";
            worKsheeT.Cell(rowNo, 6).Value = "Total Qty";
            worKsheeT.Cell(rowNo, 7).Value = "Release Qty";
            worKsheeT.Cell(rowNo, 8).Value = "Release Date";
            worKsheeT.Cell(rowNo, 9).Value = "Release By";
            worKsheeT.Cell(rowNo, 10).Value = "Total Released Qty";
            worKsheeT.Cell(rowNo, 11).Value = "Previous Released Qty";
            worKsheeT.Cell(rowNo, 12).Value = "Balance Qty";
            worKsheeT.Cell(rowNo, 13).Value = "Custom Release";
            worKsheeT.Cell(rowNo, 14).Value = "Due Date";
            worKsheeT.Cell(rowNo, 15).Value = "Receive By";
            worKsheeT.Cell(rowNo, 16).Value = "Receive Date";
            worKsheeT.Cell(rowNo, 17).Value = "Previous Released By";
            worKsheeT.Cell(rowNo, 18).Value = "Previous Released Date";
          
            rowNo++;

            foreach (var item in releaseBOMList)
            {
                var totalRelqty = "";

                if(string.IsNullOrWhiteSpace(item.SNO) && !item.PartNumber.Equals(baseInfo.PartNum, StringComparison.InvariantCultureIgnoreCase))
                {
                    totalRelqty = item.TotalReleasedQty;
                }

                else
                {
                    if (string.IsNullOrWhiteSpace(item.TotalReleasedQty))
                    {
                        totalRelqty = (Convert.ToInt32("0") + Convert.ToInt32(releaseqty)).ToString();
                    }
                    else
                    {
                        totalRelqty = (Convert.ToInt32(item.TotalReleasedQty) + Convert.ToInt32(item.ReleaseQty)).ToString();
                    }
                }


                worKsheeT.Cell(rowNo, 1).Value = item.SNO;
                worKsheeT.Cell(rowNo, 2).Value = item.PartNumber;
                worKsheeT.Cell(rowNo, 3).Value = item.ItemName;
                worKsheeT.Cell(rowNo, 4).Value = item.UnitQty;
                worKsheeT.Cell(rowNo, 5).Value = item.RequiredQty;
                worKsheeT.Cell(rowNo, 6).Value = item.TotalQty;
                worKsheeT.Cell(rowNo, 7).Value = item.ReleaseQty;
                worKsheeT.Cell(rowNo, 8).Value = item.ReleaseDate;
                worKsheeT.Cell(rowNo, 9).Value = item.ReleaseBy;
                worKsheeT.Cell(rowNo, 10).Value = totalRelqty;
                worKsheeT.Cell(rowNo, 11).Value = item.PreviousReleasedQty;
                worKsheeT.Cell(rowNo, 12).Value = item.BalanceQty;
                worKsheeT.Cell(rowNo, 13).Value = item.CustomRelease;
                worKsheeT.Cell(rowNo, 14).Value = item.DueDate;
                worKsheeT.Cell(rowNo, 15).Value = item.ReceiveBy;
                worKsheeT.Cell(rowNo, 16).Value = item.ReceiveDate;
                worKsheeT.Cell(rowNo, 17).Value = item.PreviousReleasedBy;
                worKsheeT.Cell(rowNo, 18).Value = item.PreviousReleasedDate;

                if (string.IsNullOrWhiteSpace(item.SNO) && !string.IsNullOrWhiteSpace(item.PartNumber))
                {
                    var Range = worKsheeT.Range(worKsheeT.Cell(rowNo, 1), worKsheeT.Cell(rowNo, 18));
                    Range.Style.Font.Bold = true;
                }
                rowNo++;
            }

            var range2 = worKsheeT.Range(worKsheeT.Cell(5, 1), worKsheeT.Cell(rowNo, 18));
            var border2 = range2.Style.Border;
            worKsheeT.Columns(1, 18).AdjustToContents();
            border2.SetOutsideBorder(XLBorderStyleValues.Thick);
            border2.SetInsideBorder(XLBorderStyleValues.Thin);
            range2.Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
            range2.Style.Font.FontSize = 10;
            rowNo++;


            rowNo++;
            var range3 = worKsheeT.Range(worKsheeT.Cell(rowNo+1, 16), worKsheeT.Cell(rowNo+2, 17));
            var border3 = range3.Style.Border;
            border3.SetOutsideBorder(XLBorderStyleValues.Thin);
            border3.SetInsideBorder(XLBorderStyleValues.Thin);
            range3.Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
            range3.Style.Font.FontSize = 10;
            range3.Merge();

            worKsheeT.Cell(rowNo+2, 15).Value = "Receive By :";
            worKsheeT.Cell(rowNo + 4, 15).Value = "Receive Date :";
            worKsheeT.Cell(rowNo + 4, 16).Value = DateTime.Today;
            worKsheeT.Column(15).AdjustToContents();

        }




        ///// <summary>
        ///// function for 
        ///// </summary>
        ///// <typeparam name="T"></typeparam>
        ///// <param name="models"></param>
        ///// <returns></returns>
        //public DataTable ConvertToDataTable<T>(List<T> models)
        //{
        //    // creating a data table instance and typed it as our incoming model   
        //    // as I make it generic, if you want, you can make it the model typed you want.  
        //    DataTable dataTable = new DataTable(typeof(T).Name);

        //    //Get all the properties of that model  
        //    PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);

        //    // Loop through all the properties              
        //    // Adding Column name to our datatable  
        //    foreach (PropertyInfo prop in Props)
        //    {
        //        //Setting column names as Property names    
        //        dataTable.Columns.Add(prop.Name);
        //    }
        //    // Adding Row and its value to our dataTable  
        //    foreach (T item in models)
        //    {
        //        var values = new object[Props.Length];
        //        for (int i = 0; i < Props.Length; i++)
        //        {
        //            //inserting property values to datatable rows    
        //            values[i] = Props[i].GetValue(item, null);
        //        }
        //        // Finally add value to datatable    
        //        dataTable.Rows.Add(values);
        //    }
        //    return dataTable;
        //}
    }
}
   
