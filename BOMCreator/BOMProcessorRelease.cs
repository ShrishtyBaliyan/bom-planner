﻿using CadData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BOMCreator
{
    public class BOMProcessorRelease : IBOMProcessor
    { /// <summary>
      /// Stores the RootAssembly
      /// </summary>
        AssemblyInfo rootAssembly;

        /// <summary>
        /// 
        /// </summary>
        AssemblyInfo processedAssembly;

        /// <summary>
        /// Stores the Item Group of the file
        /// </summary>
        string itemGroup = "";

        /// <summary>
        /// Stores the type of the file
        /// </summary>
        FileType fileType;


        /// <summary>
        /// Stores the value indicating the type of bom
        /// </summary>
        private BOMType bOMType;


        public BOMProcessorRelease(AssemblyInfo rootAssembly,
                                   BOMType bOMType,
                                   FileType fileType,
                                   string itemGroup = "")
        {
            this.rootAssembly = rootAssembly;
            this.bOMType = bOMType;
            this.fileType = fileType;
            this.itemGroup = itemGroup;
        }



        /// <summary>
        /// 
        /// </summary>
        /// <param name="reportType"></param>
        public void Process(ReportType reportType)
        {
            if (fileType != FileType.asm)
            {
                processedAssembly = new AssemblyTraverser().Traverse(this.rootAssembly, this.bOMType, this.fileType, this.itemGroup);
            }
            else
            {
                processedAssembly = rootAssembly;

            }

            var reportGen = ReportGeneratorFactory.Create(processedAssembly, reportType, this.fileType, this.itemGroup);
            reportGen.Generate();
        }
    }
}
