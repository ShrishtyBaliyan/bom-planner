﻿using CadData;
using ClosedXML.Excel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;

namespace BOMCreator
{
    public static class ExcelHelper
    {
        public static object worKbooK;

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static int GetColumncount(BOMType bOMType)
        {
            switch (bOMType)
            {
                case BOMType.General:
                    {
                        var att = PropertyReader.attributes.Where(atb => atb.Order < 10000 && atb.IsDesign).ToList();
                        return att.Count;
                    }

                case BOMType.Release:
                    {
                        var att = PropertyReader.attributes.Where(atb => atb.Order < 10000 && atb.IsSave).ToList();
                        return att.Count;
                    }
            }
            return 0;
        }



        /// <summary>
        /// 
        /// </summary>
        /// <param name="prtType"></param>
        /// <param name="fileType"></param>
        /// <param name="priority"></param>
        /// <param name="issuedTo"></param>
        /// <param name="source"></param>
        /// <returns></returns>
        //public static string GetHeading(string prtType,
        //                                FileType fileType,
        //                                Priority priority,
        //                                IssuedTo issuedTo,
        //                                Source source)
        //{
        //    var msg = string.Empty;
        //    if (!string.IsNullOrEmpty(prtType))
        //    {
        //        msg = prtType.ToUpper() + " PART LIST ";
        //    }
        //    else if (fileType == FileType.asm && priority == Priority.None)
        //    {
        //        msg = "ASSEMBLY WISE BILL OF MATERIAL";
        //    }
        //    else if (fileType == FileType.part && priority == Priority.None && issuedTo == IssuedTo.NA && source == Source.None)
        //    {
        //        msg = " PART WISE BILL OF MATERIAL";
        //    }
        //    else if (fileType == FileType.part && priority == Priority.high && issuedTo == IssuedTo.NA && source == Source.None)
        //    {
        //        msg = " PART WISE BILL OF MATERIAL(HIGH PRIORITY)";
        //    }
        //    else if (fileType == FileType.part && priority == Priority.medium && issuedTo == IssuedTo.NA && source == Source.None)
        //    {
        //        msg = " PART WISE BILL OF MATERIAL (MEDIUM PRIORITY)";
        //    }
        //    else if (fileType == FileType.part && priority == Priority.low && issuedTo == IssuedTo.NA && source == Source.None)
        //    {
        //        msg = " PART WISE BILL OF MATERIAL(LOW PRIORITY)";
        //    }
        //    else if (fileType == FileType.bOut && priority == Priority.None && issuedTo == IssuedTo.NA && source == Source.None)
        //    {
        //        msg = "BOUGHT OUT & HARDWARE LIST";
        //    }
        //    else if (fileType == FileType.bOut && priority == Priority.high && issuedTo == IssuedTo.NA && source == Source.None)
        //    {
        //        msg = "BOUGHT OUT BILL OF MATERIAL(HIGH PRIORITY)";
        //    }
        //    else if (fileType == FileType.bOut && priority == Priority.medium && issuedTo == IssuedTo.NA && source == Source.None)
        //    {
        //        msg = "BOUGHT OUT BILL OF MATERIAL (MEDIUM PRIORITY)";
        //    }
        //    else if (fileType == FileType.bOut && priority == Priority.low && issuedTo == IssuedTo.NA && source == Source.None)
        //    {
        //        msg = "BOUGHT OUT BILL OF MATERIAL (LOW PRIORITY)";
        //    }

        //    else if (fileType == FileType.part && issuedTo == IssuedTo.PD && source == Source.None && priority == Priority.None)
        //    {
        //        msg = " ISSUED TO PRODUCTION";
        //    }
        //    else if (fileType == FileType.part && issuedTo == IssuedTo.PU && source == Source.None && priority == Priority.None)
        //    {
        //        msg = " ISSUED TO PURCHASE";
        //    }
        //    else if (fileType == FileType.part && issuedTo == IssuedTo.QC && source == Source.None && priority == Priority.None)
        //    {
        //        msg = " ISSUED TO QUALITY";
        //    }
        //    else if (fileType == FileType.part && issuedTo == IssuedTo.None && source == Source.None && priority == Priority.None)
        //    {
        //        msg = " ISSUED TO NONE";
        //    }

        //    else if (fileType == FileType.part && source == Source.InHouse)
        //    {
        //        msg = " IN HOUSE PART LIST";
        //    }
        //    else if (fileType == FileType.part && source == Source.OutSourced)
        //    {
        //        msg = " OUT SORCED PART LIST";
        //    }

        //    return msg;
        //}



        /// <summary>
        /// 
        /// </summary>
        /// <param name="useTemplate"></param>
        /// <param name="templatename"></param>
        /// <returns></returns>
        public static XLWorkbook GetXLWorkbook(bool useTemplate,
                                               out IXLWorksheet worksheet,
                                               string templatename = "")
        {
            XLWorkbook worKbooK;
            var copiedFile = Path.Combine(Path.GetTempPath(), DateTime.Now.ToString("yyyy-dd-M-HH-mm-ss") + ".xlsx");
            if (useTemplate)
            {
                string assemblyFolder = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
                string excelFileName = Path.Combine(assemblyFolder, templatename);
                File.Copy(excelFileName, copiedFile);
                worKbooK = new XLWorkbook(copiedFile);
                worksheet = worKbooK.Worksheets.ElementAt(0);
                worksheet.Name = "Assembly";
            }
            else
            {
                worKbooK = new XLWorkbook();
                worksheet = worKbooK.Worksheets.Add("Assembly");
            }

            worksheet.PageSetup.FitToPages(1, 1);
            worksheet.PageSetup.PaperSize = XLPaperSize.A4Paper;

            return worKbooK;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="wrkSheet"></param>
        /// <param name="row"></param>
        /// <param name="doShowSno"></param>
        /// <returns></returns>
        public static List<string> SetHeading(IXLWorksheet wrkSheet, int row, bool doShowSno, BOMType bOMType)
        {
            var attNames = new List<string>();
            var att = PropertyReader.attributes.Where(atb => atb.Order < 10000);

            switch (bOMType)
            {
                case BOMType.General:
                    {
                        att = PropertyReader.attributes.Where(atb => atb.Order < 10000 && atb.IsDesign).ToList();
                        break;
                    }

                case BOMType.Release:
                    {
                        att = PropertyReader.attributes.Where(atb => atb.Order < 10000 && atb.IsSave).ToList();
                        break;
                    }
            }


            var orederedAtt = att.OrderBy(attib => attib.Order);
            var colNum = 1;
            if (doShowSno)
            {
                wrkSheet.Cell(row, colNum).Value = "SNO";
                colNum++;
            }

            foreach (var atrib in orederedAtt)
            {
                wrkSheet.Cell(row, colNum).Value = atrib.DisplayName;
                colNum++;

                attNames.Add(atrib.Name);
            }

            wrkSheet.Cell(row, colNum).Value = "Total Weight (Kg)";

            return attNames;
        }
    }
}
