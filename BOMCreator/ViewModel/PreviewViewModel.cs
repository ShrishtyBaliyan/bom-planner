﻿using CadData;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace BOMCreator.ViewModel
{
    public class PreviewViewModel : ICloseWindow
    {

        AssemblyInfo baseInfo;

        FileType fileType;

        string itemGroup;

        ReportType reportType;


        /// <summary>
        /// 
        /// </summary>
        private string label;
        public string Label
        {
            get => this.label;

        }


        /// <summary>
        /// stores details of material & weight summary for PreView
        /// </summary>
        private DataTable materialDt;
        public DataTable MaterialDt
        {
            get => this.materialDt;

        }


        /// <summary>
        /// stores details of Assembly details for PreView
        /// </summary>
        private DataTable viewDt;
        public DataTable ViewDt
        {
            get => this.viewDt;

        }


        public PreviewViewModel(DataTable info, DataTable materialInfo, AssemblyInfo baseInfo, ReportType reportType, FileType fileType, string itemGroup)
        {
            this.viewDt = info;
            this.materialDt = materialInfo;
            this.baseInfo = baseInfo;
            this.reportType = reportType;
            this.fileType = fileType;
            this.itemGroup = itemGroup;
            this.label = baseInfo.PartNum + "-" + baseInfo.ItemName;
        }



        /// <summary>
        /// cmd for export Report General View
        /// </summary>
        private ICommand exportCommand;
        public ICommand ExportCommand
        {
            get
            {
                if (this.exportCommand == null)
                    this.exportCommand = (ICommand)new ICommandInterface(new Action<object>(this.executeExport), new Func<object, bool>(this.canExecuteExport));
                return this.exportCommand;
            }
        }
        private bool canExecuteExport(object arg) => true;
        private void executeExport(object parameter)
        {
                ReportGeneratorGeneral reportGeneratorGeneral = new ReportGeneratorGeneral(this.baseInfo,  this.reportType, this.fileType, this.itemGroup);
                reportGeneratorGeneral.GenerateExcelGen();
        }





        /// <summary>
        /// cmd for closing window
        /// </summary>
        private ICommand closeCommand;
        public ICommand CloseCommand
        {
            get
            {
                if (this.closeCommand == null)
                    this.closeCommand = (ICommand)new ICommandInterface(new Action<object>(this.executeClose), new Func<object, bool>(this.canExecuteClose));
                return this.closeCommand;
            }
        }
        private bool canExecuteClose(object arg) => true;
        private void executeClose(object parameter)
        {
            CloseWindow();
        }



        /// <summary>
        /// func for closing window
        /// </summary>
        void CloseWindow()
        {
            Close?.Invoke();
        }

        public Action Close { get; set; }

    }
}
