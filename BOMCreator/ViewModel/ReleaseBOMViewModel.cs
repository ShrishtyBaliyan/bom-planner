﻿using CadData;
using ReleaseBOM;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Input;

namespace BOMCreator.ViewModel
{
    public class ReleaseBOMViewModel : INotifyPropertyChanged , ICloseWindow
    {
      
        /// <summary>
        /// stores details of Assembly
        /// </summary>
        AssemblyInfo baseInfo;

        /// <summary>
        /// stores the value of user who is releasing the BOM
        /// </summary>
        string releaseBy;


        /// <summary>
        /// stores Previously released value
        /// </summary>
        string preReleaseqty;


        /// <summary>
        /// 
        /// </summary>
        string receiveby;

        /// <summary>
        /// stores list values after refresing the Release BOM Window
        /// </summary>
        public List<ReleaseModel> dgList = new List<ReleaseModel>();


        /// <summary>
        /// List Binded to DataGrid
        /// </summary>
        private List<ReleaseModel> designList;
        public List<ReleaseModel> DesignList
        {
            get => this.designList;
            set
            {
                this.designList = value;
                OnPropertyChanged("DesignList");
            }
        }


        /// <summary>
        /// stores boolean value of same release qty checkbox
        /// </summary>
        private bool sameRelease;
        public bool SameRelease
        {
            get => this.sameRelease;
            set
            {
                this.sameRelease = value;
                OnPropertyChanged("SameRelease");
            }
        }



        /// <summary>
        /// stores boolean value of diff release qty checkbox
        /// </summary>
        private bool diffRelease;
        public bool DiffRelease
        {
            get => this.diffRelease;
            set
            {
                this.diffRelease = value;
                OnPropertyChanged("DiffRelease");
            }
        }


        /// <summary>
        /// stores boolean value of QRCode Generate
        /// </summary>
        private bool qRCode;
        public bool QRCode
        {
            get => this.qRCode;
            set
            {
                this.qRCode = value;
                OnPropertyChanged("QRCode");
            }
        }


        /// <summary>
        /// stores boolean value of PDF Generate
        /// </summary>
        private bool pdf;
        public bool PDF
        {
            get => this.pdf;
            set
            {
                this.pdf = value;
                OnPropertyChanged("PDF");
            }
        }



        /// <summary>
        /// stores boolean value of Receiver(m/c Shop)
        /// </summary>
        private bool mcShop;
        public bool MCShop
        {
            get => this.mcShop;
            set
            {
                this.mcShop = value;
                OnPropertyChanged("MCShop");
            }
        }


        /// <summary>
        /// stores boolean value of Receiver(Assy Shop)
        /// </summary>
        private bool assyShop;
        public bool AssyShop
        {
            get => this.assyShop;
            set
            {
                this.assyShop = value;
                OnPropertyChanged("AssyShop");
            }
        }



        /// <summary>
        /// stores boolean value of Receiver(Weld Shop)
        /// </summary>
        private bool weldShop;
        public bool WeldShop
        {
            get => this.weldShop;
            set
            {
                this.weldShop = value;
                OnPropertyChanged("WeldShop");
            }
        }



        /// <summary>
        /// stores the Name of Main Assembly, used for heading of Release BOM window
        /// </summary>
        private string label;
        public string Label
        {
            get => this.label;
            set
            {
                this.label = value;
                OnPropertyChanged("Label");
            }
        }



        /// <summary>
        /// Property Changed Event Handler
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string PropertyName)
        {
            if (this.PropertyChanged == null)
                return;
            this.PropertyChanged((object)this, new PropertyChangedEventArgs(PropertyName));
        }




        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="info"></param>
        /// <param name="baseInfo"></param>
        public ReleaseBOMViewModel(List<ReleaseModel> info, AssemblyInfo baseInfo, string releaseby, string preReleaseqty)
        {
            this.designList = info;
            this.baseInfo = baseInfo;
            this.releaseBy = releaseby;
            this.preReleaseqty = preReleaseqty;

            this.label = baseInfo.PartNum + "-" + baseInfo.ItemName;
        }



        /// <summary>
        /// Command for Release BOM
        /// </summary>
        private ICommand releaseCommand;
        public ICommand ReleaseCommand
        {
            get
            {
                if (this.releaseCommand == null)
                    this.releaseCommand = (ICommand)new ICommandInterface(new Action<object>(this.executeRelease), new Func<object, bool>(this.canExecuteRelease));
                return this.releaseCommand;
            }
        }
        private bool canExecuteRelease(object arg) => true;
        private void executeRelease(object parameter)
        {
            if (this.diffRelease == false && this.sameRelease == false)
            {
                MessageBox.Show("Please Select Either 'Same Release' or 'Different Release'");
                return;
            }

            else if (this.diffRelease == true && this.sameRelease == true)
            {
                MessageBox.Show("Please Select Either 'Same Release' or 'Different Release'");
                return;
            }
            else
            {
                if (!RefreshDataGrid())
                {
                    return;
                }
                ReportGeneratorBOMRelease reportGeneratorGeneral = new ReportGeneratorBOMRelease(this.dgList, this.baseInfo);
                reportGeneratorGeneral.GenerateExcelGen(this.releaseBy, this.diffRelease, receiveby, this.qRCode, this.pdf);
            }
        }



        /// <summary>
        /// Command for refresh DataGrid Values
        /// </summary>
        private ICommand refreshCommand;
        public ICommand RefreshCommand
        {
            get
            {
                if (this.refreshCommand == null)
                    this.refreshCommand = (ICommand)new ICommandInterface(new Action<object>(this.executeRefresh), new Func<object, bool>(this.canExecuteRefresh));
                return this.refreshCommand;
            }
        }
        private bool canExecuteRefresh(object arg) => true;
        private void executeRefresh(object parameter)
        {
            if (!this.diffRelease  && !this.sameRelease)
            {
                MessageBox.Show("Please Select Either 'Same Release' or 'Different Release'");
                return;
            }

            else if (this.diffRelease == true && this.sameRelease == true)
            {
                MessageBox.Show("Please Select Either 'Same Release' or 'Different Release'");
                return;
            }
            else
            {
                if (!RefreshDataGrid())
                {
                    return;
                }
                this.designList.Clear();
                this.DesignList = dgList;
            }
        }



        /// <summary>
        /// Command for close window
        /// </summary>
        private ICommand closeCommand;
        public ICommand CloseCommand
        {
            get
            {
                if (this.closeCommand == null)
                    this.closeCommand = (ICommand)new ICommandInterface(new Action<object>(this.executeClose), new Func<object, bool>(this.canExecuteClose));
                return this.closeCommand;
            }
        }
        private bool canExecuteClose(object arg) => true;
        private void executeClose(object parameter)
        {
            CloseWindow();
        }


        /// <summary>
        /// func for close window
        /// </summary>
        void CloseWindow()
        {
            Close?.Invoke();
        }

        public Action Close { get; set; }



        /// <summary>
        /// function for Refresh the values of DataGrid
        /// </summary>
        public bool RefreshDataGrid()
        {
            try
            {
                if (this.mcShop && !this.assyShop && !this.weldShop)
                {
                    this.receiveby = "M/C Shop";
                }
                else if (!this.mcShop && this.assyShop && !this.weldShop)
                {
                    this.receiveby = "Assy Shop";
                }
                else if (!this.mcShop && !this.assyShop && this.weldShop)
                {
                    this.receiveby = "Weld Shop";
                }
                else if (this.mcShop && this.assyShop && !this.weldShop)
                {
                    this.receiveby = "M/C Shop;Assy Shop";
                }
                else if (this.mcShop && !this.assyShop && this.weldShop)
                {
                    this.receiveby = "M/C Shop;Weld Shop";
                }
                else if (!this.mcShop && this.assyShop && this.weldShop)
                {
                    this.receiveby = "Assy Shop;Weld Shop";
                }
                else if (this.mcShop && this.assyShop && this.weldShop)
                {
                    this.receiveby = "M/C Shop;Assy Shop;Weld Shop";
                }
                else
                {
                    MessageBox.Show("Please Select Any One Feild From Release to");
                    return false;
                }
                if (!RefreshDataGrid(baseInfo, this.receiveby))
                {
                    return false;
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }

            return true;
        }




        /// <summary>
        ///  function for Refresh Values of DataGrid
        /// </summary>
        /// <param name="info"></param>
        /// <param name="sno"></param>
        /// <param name="asmInfo"></param>
        /// <param name="prtType"></param>
        /// <param name="isModel"></param>
        private bool RefreshDataGrid(AssemblyInfo asmInfo, string receiveBy, bool isModel = false)
        {
            if (!asmInfo.childs.Any())
            {
                return false;
            }

            List<ReleaseModel> clonnedDgList = new List<ReleaseModel>();

            var reqqty = string.Empty;
            var releaseqty = string.Empty;
            var duedate = string.Empty;
            var totalQty = string.Empty;
            var balanceQty = string.Empty;

            foreach (var item in this.DesignList)
            {
                if (String.IsNullOrWhiteSpace(item.PartNumber) || (String.IsNullOrWhiteSpace(item.SNO) && (!item.PartNumber.Equals(baseInfo.PartNum, StringComparison.InvariantCultureIgnoreCase))))
                {
                    clonnedDgList.Add(new ReleaseModel()
                    {
                        SNO = item.SNO,
                        PartNumber = item.PartNumber,
                        ItemName = item.ItemName,
                        UnitQty = item.UnitQty,
                        RequiredQty = item.RequiredQty,
                        TotalQty = item.TotalQty,
                        ReleaseQty = item.RequiredQty,
                        ReleaseDate = item.ReleaseDate,
                        ReleaseBy = item.ReleaseBy,
                        TotalReleasedQty = item.TotalReleasedQty,
                        PreviousReleasedQty = item.PreviousReleasedQty,
                        BalanceQty = item.BalanceQty,
                        CustomRelease = item.CustomRelease,
                        DueDate = item.DueDate,
                        ReceiveBy = item.ReceiveBy,
                        ReceiveDate = item.ReceiveDate,
                        LastReceivedBy = item.LastReceivedBy,
                        LastReceivedDate = item.LastReceivedDate,
                        PreviousReleasedBy = item.PreviousReleasedBy,
                        PreviousReleasedDate = item.PreviousReleasedDate
                    });
                }

                else
                {
                    reqqty = this.designList[0].RequiredQty;
                    releaseBy = this.designList[0].ReleaseBy;

                    if (this.sameRelease)
                    {
                        releaseqty = this.designList[0].ReleaseQty;
                    }
                    else if (this.diffRelease)
                    {
                        releaseqty = item.ReleaseQty;
                    }

                    totalQty = ((Convert.ToInt32(reqqty) + Convert.ToInt32(item.CustomRelease)) * Convert.ToInt32(item.UnitQty)).ToString();
                    balanceQty = (Convert.ToInt32(totalQty) - (Convert.ToInt32(item.TotalReleasedQty) + Convert.ToInt32(releaseqty))).ToString();

                    if (Convert.ToInt32(balanceQty) < 0)
                    {
                        MessageBox.Show("ReleaseQty can not be greater than BalanceQty");
                        return false;
                    }

                    if (string.IsNullOrWhiteSpace(reqqty))
                    {
                        MessageBox.Show("'Required Qty' field can not be Empty!");
                        return false;
                    }

                    clonnedDgList.Add(new ReleaseModel()
                    {
                        SNO = item.SNO,
                        PartNumber = item.PartNumber,
                        ItemName = item.ItemName,
                        UnitQty = item.UnitQty,
                        RequiredQty = reqqty,
                        TotalQty = totalQty,
                        ReleaseQty = releaseqty,
                        ReleaseDate = DateTime.Now.ToString(),
                        ReleaseBy = releaseBy,
                        TotalReleasedQty = item.TotalReleasedQty,
                        PreviousReleasedQty = item.PreviousReleasedQty,
                        BalanceQty = balanceQty,
                        CustomRelease = item.CustomRelease,
                        DueDate = item.DueDate,
                        ReceiveBy = receiveby,
                        ReceiveDate = DateTime.Now.ToString(),
                        LastReceivedBy = item.LastReceivedBy,
                        LastReceivedDate = item.LastReceivedDate,
                        PreviousReleasedBy = item.PreviousReleasedBy,
                        PreviousReleasedDate = item.PreviousReleasedDate
                    });

                }

            }
            this.dgList.Clear();
            this.dgList = clonnedDgList;
            return true;
        }

    }

}
