﻿using CadData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BOMCreator
{
  class BOMModel
  {
    public CADProperties cadProps = new CADProperties();
    public List<Tuple<string, List<AttributeData>>> stockTypes = new List<Tuple<string, List<AttributeData>>>();
    public KeyValuePair<string, List<string>> key;
    public List<string> boughtOuts = new List<string>();
    public List<string> PartTypes = new List<string>();
  }
}
