﻿using CadData;
using CadData.Helpers;
using System;
using System.Linq;

namespace BOMCreator
{
    public class AssemblyTraverser
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name = "nLevel" ></ param >
        /// < param name="baseInfo"></param>
        /// <param name = "bomType" ></ param >
        /// < param name="fileType"></param>
        /// <param name = "issuedTo" ></ param >
        /// < param name="source"></param>
        /// <param name = "priority" ></ param >
        /// < param name="prtType"></param>
        /// <returns></returns>
        public AssemblyInfo Traverse(AssemblyInfo baseInfo,
                                     BOMType bomType,
                                     FileType fileType,
                                     string itemGroup)
        {
            var updatedInfo = new AssemblyInfo(baseInfo);
            switch (bomType)
            {
                case BOMType.General:
                    {
                        TraverseComponentPartTopAsm(1, baseInfo, updatedInfo, fileType, itemGroup);

                        return updatedInfo;
                    }

            }

            return null;
        }






        /// <summary>
        /// 
        /// </summary>
        /// <param name="nLevel"></param>
        /// <param name="asmInfo"></param>
        /// <param name="updatedAsmInfo"></param>
        /// <param name="type"></param>
        /// <param name="priority"></param>
        /// <param name="prtType"></param>
        public void TraverseComponentPartTopAsm(long nLevel,
                                            AssemblyInfo asmInfo,
                                            AssemblyInfo updatedAsmInfo,
                                            FileType filetype,
                                            string itemGroup)
        {
            //var updatedAsmInfo = new AsmInfo(asmInfo);

            var childs = asmInfo.childs.ToList();
            for (int i = 0; i < childs.Count; i++)
            {
                var currChildInfo = childs[i];

                if (string.IsNullOrEmpty(currChildInfo.PartNum))
                {
                    continue;
                }

                if (!ValidateType(currChildInfo, itemGroup))
                {
                    continue;
                }

                if (!currChildInfo.isAsm)
                {
                    if (filetype == FileType.bOut)
                    {
                        if (!IsTypeBoutOut(currChildInfo))
                        {
                            continue;
                        } // if        

                        //if (!ValidatePriority(currChildInfo, priority, prtType))
                        //{
                        //    continue;
                        //}
                    }

                    if (filetype == FileType.part)
                    {
                        if (IsTypeBoutOut(currChildInfo) && !currChildInfo.ItemGroup.Equals(itemGroup, StringComparison.InvariantCultureIgnoreCase))
                        {
                            continue;
                        } // i

                        //if (!ValidatePriority(currChildInfo, itemGroup))
                        //{
                        //    continue;
                        //}
                    }

                }

                var existingInfo = updatedAsmInfo.childs.FirstOrDefault(info => info.PartNum.Equals(currChildInfo.PartNum));
                if (existingInfo == null || currChildInfo.PartNum == "-")
                {
                    if (!currChildInfo.isAsm)
                    {
                        updatedAsmInfo.childs.Add(new AssemblyInfo(currChildInfo));
                        TraverseComponentPartTopAsm(nLevel + 1, currChildInfo, updatedAsmInfo, filetype, itemGroup);
                    } // if 
                    else if (currChildInfo.isAsm && IsTypeBoutOut(currChildInfo))
                    {
                        currChildInfo.childs.Clear();
                        updatedAsmInfo.childs.Add(currChildInfo);
                    }
                    else if (currChildInfo.isAsm)
                    {

                        if (nLevel == 1)
                        {
                            var topAsm = updatedAsmInfo.topLevelAsm.FirstOrDefault(info => info.PartNum.Equals(currChildInfo.PartNum));
                            if (topAsm == null)
                            {
                                updatedAsmInfo.topLevelAsm.Add(new AssemblyInfo(currChildInfo));
                                updatedAsmInfo.topLevelAsm[updatedAsmInfo.topLevelAsm.Count - 1].isAsm = true;
                            }
                            else
                            {
                                topAsm.qty += currChildInfo.qty;
                            }
                        }
                        else if (currChildInfo.ItemGroup.Equals(itemGroup, StringComparison.InvariantCultureIgnoreCase))
                        {
                            updatedAsmInfo.childs.Add(new AssemblyInfo(currChildInfo));
                        }
                        TraverseComponentPartTopAsm(nLevel + 1, currChildInfo, updatedAsmInfo, filetype, itemGroup);
                    } // else if
                }
                else
                {
                    existingInfo.qty += currChildInfo.qty;
                    TraverseComponentPartTopAsm(nLevel + 1, currChildInfo, updatedAsmInfo, filetype, itemGroup);
                }

            }
            //var arrangedChildLst = new List<AsmInfo>();
            updatedAsmInfo.childs.Sort(new AlphanumComparatorFast());
            updatedAsmInfo.topLevelAsm.Sort(new AlphanumComparatorFast());
        }



        ///// <summary>
        ///// 
        ///// </summary>
        ///// <param name="baseInfo"></param>
        ///// <param name="priority"></param>
        ///// <param name="prtType"></param>
        ///// <returns></returns>
        //private bool ValidatePriority(AssemblyInfo baseInfo,
        //                          Priority priority,
        //                          string prtType)
        //{
        //    if (priority == Priority.high)
        //    {
        //        if (baseInfo.Priority == string.Empty || baseInfo.Priority.ToUpper()[0] != 'H')
        //        {
        //            return false;
        //        } // if 
        //    }


        //    if (priority == Priority.medium)
        //    {
        //        if (baseInfo.Priority == string.Empty || baseInfo.Priority.ToUpper()[0] != 'M')
        //        {
        //            return false;
        //        } // if 
        //    }

        //    if (priority == Priority.low)
        //    {
        //        if (baseInfo.Priority == string.Empty || baseInfo.Priority.ToUpper()[0] != 'L')
        //        {
        //            return false;
        //        } // if 
        //    }
        //    return true;
        //}



        /// <summary>
        /// 
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        private static bool IsTypeBoutOut(AssemblyInfo info)
        {
            if (PropertyReader.boughtOuts.Any(b => b.Equals(info.ItemGroup, StringComparison.InvariantCultureIgnoreCase)))
            {
                return true;
            }
            return false;
        }



        //public static bool ValidatePriorityType(AssemblyInfo baseInfo,
        //                     string priorityType)
        //{
        //    if (!string.IsNullOrEmpty(priorityType))
        //    {
        //        var types = PropertyReader.Priority.ToList();
        //        types.Remove(priorityType);

        //        if (types.Any(type => baseInfo.Type.Equals(type, StringComparison.InvariantCultureIgnoreCase)))
        //        {
        //            return false;
        //        }
        //    }

        //    return true;
        //}



        /// <summary>
        /// 
        /// </summary>
        /// <param name="baseInfo"></param>
        /// <param name="prtType"></param>
        /// <returns></returns>
        public static bool ValidateType(AssemblyInfo baseInfo,
                              string itemGroup)
        {
            if (!string.IsNullOrEmpty(itemGroup))
            {
                var types = SQLConnection.Item_Group.Split(',').ToList();
                types.Remove(itemGroup);

                if (!baseInfo.ItemGroup.Equals(itemGroup, StringComparison.InvariantCultureIgnoreCase))
                {
                    return false;
                }
            }

            return true;
        }
    }
}
