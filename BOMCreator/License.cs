﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.NetworkInformation;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Win32;
using OpenFileDialog = System.Windows.Forms.OpenFileDialog;

namespace BOMCreator
{
  public static class License
  {
    private static string CompName = string.Empty;
    private static string MacAdd = string.Empty;
    private static int days = 7;
    private static DateTime startDate = DateTime.Now;

    public static void ReadLicense()
    {
      try
      {
        OpenFileDialog diag = new OpenFileDialog();
        diag.Title = "Read License File";
        diag.DefaultExt = "txt";
        diag.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
        diag.FilterIndex = 2;
        var ff = diag.ShowDialog();
        if (ff == DialogResult.Cancel)
        {
          return;
        }
        var path = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);
        path = Path.Combine(path, "License.txt");
        File.Delete(path);

        File.Copy(diag.FileName, path);
      }
      catch
      {
        MessageBox.Show("Can not copy license file");
      }

    }

    //public static string GetDirectoryPath(this Assembly assembly)
    //{
    //  string filePath = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);
    //  return Path.GetDirectoryName(filePath);
    //}

    public static void SetLicensePath()
    {
      var fileDialog = new OpenFileDialog { Filter = "License (*.txt)|*.txt" };
      fileDialog.ShowDialog();
      var path = fileDialog.FileName;
      if (!File.Exists(path))
      {
        return;
      }
      var key = Registry.CurrentUser.CreateSubKey(@"SOFTWARE\WinRegistry");
      if (key == null) return;
      key.SetValue("LicensePath", path);
      //key.SetValue("Height", this.Height);
      key.Close();
    }

        public static bool ValidateLicense()
        {
            try
            {
                // var licenseFile = new List<string>();
                //var path = string.Empty;
                //var key = Registry.CurrentUser.OpenSubKey(@"SOFTWARE\WinRegistry");
                //if (key != null)
                //{
                //  path = key.GetValue("LicensePath") as string;
                var path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\License.txt";
                //licenseFile = Directory.GetFiles(path).Where(p => p.Contains(".txt")).ToList();
                if (!File.Exists(path))
                {
                    if (MessageBox.Show("No valid license file found.", "",
                          MessageBoxButtons.OK) == DialogResult.OK)
                    {
                        return false;
                    }
                    //else
                    //{
                    //    SetLicensePath();
                    //    key = Registry.CurrentUser.OpenSubKey(@"SOFTWARE\WinRegistry");
                    //    if (key != null) path = key.GetValue("LicensePath") as string;
                    //    //licenseFile = Directory.GetFiles(path).Where(p => p.Contains(".txt")).ToList();
                    //    if (!File.Exists(path))
                    //    {
                    //        return false;
                    //    }
                    //}
                }
                //else
                //{
                //  SetLicensePath();
                //  key = Registry.CurrentUser.OpenSubKey(@"SOFTWARE\WinRegistry");
                //  if (key != null) path = key.GetValue("LicensePath") as string;
                // // licenseFile = Directory.GetFiles(path).Where(p => p.Contains(".txt")).ToList();
                //  if (!File.Exists(path))
                //  {
                //    return false;
                //  }
                //}
                //}
                //else
                //{
                //  SetLicensePath();
                //  key = Registry.CurrentUser.OpenSubKey(@"SOFTWARE\WinRegistry");
                //  if (key != null) path = key.GetValue("LicensePath") as string;
                //  //licenseFile = Directory.GetFiles(path).Where(p => p.Contains(".txt")).ToList();
                //  if (!File.Exists(path))
                //  {
                //    return false;
                //  }
                //}

                var txt = File.ReadAllText(path);
                var str = EncryptionHelper.Decrypt(txt);

                var details = str.Split(' ');
                CompName = details[0];
                MacAdd = details[1];
                days = int.Parse(details[2]);
                startDate = DateTime.FromBinary(long.Parse(details[3]));

                var currentDate = DateTime.Now;
                var diff = currentDate.Subtract(startDate);
                if (diff.Days < days && MacAdd.Equals(GetMacAddress()))
                {
                    return true;
                }
            }
            catch

            {
                MessageBox.Show("Invalid license file");
                return false;
            }
            MessageBox.Show("Invalid license file");
            return false;
        }


    public static string GetMacAddress()
    {
      return NetworkInterface
    .GetAllNetworkInterfaces()
    .Where(nic => nic.OperationalStatus == OperationalStatus.Up && nic.NetworkInterfaceType != NetworkInterfaceType.Loopback)
    .Select(nic => nic.GetPhysicalAddress().ToString())
    .FirstOrDefault();
    }
  }


}
