﻿using CadData;
using System.Windows;

namespace SaveCustomProperties
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class CustomPropertiesView : Window
    {
        public CustomPropertiesView()
        {
            InitializeComponent(); 
            Loaded += View_Loaded;
        }

        private void View_Loaded(object sender, RoutedEventArgs e)
        {
            if (DataContext is ICloseWindow vm)
            {
                vm.Close += () =>
                {
                    this.Close();
                };
            }
        }
    }
}
