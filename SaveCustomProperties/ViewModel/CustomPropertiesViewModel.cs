﻿using BOMPlanner.SQL;
using CadData;
using SolidWorks.Interop.sldworks;
using SolidWorks.Interop.swconst;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Windows.Forms;
using System.Windows.Input;
using MessageBox = System.Windows.Forms.MessageBox;

namespace SaveCustomProperties.ViewModel
{
    public class CustomPropertiesViewModel : INotifyPropertyChanged, ICloseWindow
    {
        int updated = 0;
        private const string asm_info = @"SELECT *
    FROM [dbo].[Asm_Version_Info]
    where AsmInfo_id =";
        

        private const string asm_info_table = @"SELECT *
    FROM [dbo].[Assembly_info]
    where id =";

        private List<AsmID> asmIdsWithChild = new List<AsmID>();
        private List<AssemblyInfo> asmInfos = new List<AssemblyInfo>();

        private SqlConnection con;
        private SqlDataAdapter Adapter;

        private DataTable dt;

        private List<AssemblyInfo> infos;

        AssemblyInfo baseInfo;


        /// <summary>
        /// stores the value of Standard Data Checkbox
        /// </summary>
        private bool standardCheck;
        public bool StandardCheck
        {
            get => this.standardCheck;
            set
            {
                this.standardCheck = value;
                OnPropertyChanged("StandardCheck");
            }
        }

        /// <summary>
        /// stores the value of Custom Build Checkbox
        /// </summary>
        private bool customCheck;
        public bool CustomCheck
        {
            get => this.customCheck;
            set
            {
                this.customCheck = value;
                OnPropertyChanged("CustomCheck");
            }
        }

        /// <summary>
        ///  stores the details of assemby's custom properties
        /// </summary>
        private DataTable table;
        public DataTable Table
        {
            get => this.table;
            set
            {
                this.table = value;
                OnPropertyChanged("Table");
            }
        }


        /// <summary>
        ///  
        /// </summary>
        private List<SavePropertiesModel> list;
        public List<SavePropertiesModel> List
        {
            get => this.list;
            set
            {
                this.list = value;
                OnPropertyChanged("List");
            }
        }




        /// <summary>
        /// stores the value of user who has logged In
        /// </summary>
        public string UserInfo;


        /// <summary>
        /// 
        /// </summary>
        private string label;
        public string Label
        {
            get => this.label;
            set
            {
                this.label = value;
                OnPropertyChanged("Label");
            }
        }


        /// <summary>
        /// Property Changed Event Handler
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string PropertyName)
        {
            if (this.PropertyChanged == null)
                return;
            this.PropertyChanged((object)this, new PropertyChangedEventArgs(PropertyName));
        }


        /// <summary>
        /// Constructor
        /// </summary>
        public CustomPropertiesViewModel(AssemblyInfo baseInfo, string userInfo)
        {
            this.baseInfo = baseInfo;
            this.UserInfo = userInfo;
            this.label = baseInfo.PartNum + "-" + baseInfo.ItemName;
            frmSave_Load();
            this.Table = this.table;
            this.List = this.list;
        }



        /// <summary>
        /// Save Assembly Info in Database
        /// </summary>
        private ICommand saveCommand;
        public ICommand SaveCommand
        {
            get
            {
                if (this.saveCommand == null)
                    this.saveCommand = (ICommand)new ICommandInterface(new Action<object>(this.executeSave), new Func<object, bool>(this.canExecuteSave));
                return this.saveCommand;
            }
        }

        private bool canExecuteSave(object arg) => true;

        private void executeSave(object parameter)
        {
            if (this.standardCheck == true && this.customCheck == true)
            {
                MessageBox.Show("You can only save either Standard Data or Custom Build!");
                return;
            }
            else if (this.standardCheck == false && this.customCheck == false)
            {
                MessageBox.Show("Please select either Standard Data or Custom Build to save the information!");
                return;
            }
            else if (this.standardCheck == true || this.customCheck == true)
            {
                InsertAssembly();
                //ExtractExistingAsm(baseInfo.PartNum);
            }
           
        }



        /// <summary>
        /// Close Window
        /// </summary>
        private ICommand closeCommand;

        public ICommand CloseCommand
        {
            get
            {
                if (this.closeCommand == null)
                    this.closeCommand = (ICommand)new ICommandInterface(new Action<object>(this.executeClose), new Func<object, bool>(this.canExecuteClose));
                return this.closeCommand;
            }
        }

        private bool canExecuteClose(object arg) => true;

        private void executeClose(object parameter)
        {
            CloseWindow();
        }




        /// <summary>
        /// Cmd for saving missing part numbers
        /// </summary>
        private ICommand savePNumCommand;
        private SldWorks SwApp;

        public ICommand SavePNumCommand
        {
            get
            {
                if (this.savePNumCommand == null)
                    this.savePNumCommand = (ICommand)new ICommandInterface(new Action<object>(this.executeSavePNum), new Func<object, bool>(this.canExecuteSavePNum));
                return this.savePNumCommand;
            }
        }

        private bool canExecuteSavePNum(object arg) => true;

        private void executeSavePNum(object parameter)
        {
            SaveInSldwrks(this.list);
        }

        private void SaveInSldwrks(List<SavePropertiesModel> slwrksProperties)
        {
            if (this.SwApp == null)
            {
                SwApp = new SldWorks();
                SwApp.Visible = true;
            }
            try
            {
                foreach (var info in slwrksProperties)
                {
                    //if (string.IsNullOrWhiteSpace(info.PartNumber))
                    //{
                    //    MessageBox.Show("Please Fill Part Number of empty field!");
                    //    return;
                    //}
                    var ext = Path.GetExtension(info.Path);
                    var type = ext.Equals(".sldprt", StringComparison.InvariantCultureIgnoreCase)
                      ? (int)swDocumentTypes_e.swDocPART
                      : ext.Equals(".sldasm", StringComparison.InvariantCultureIgnoreCase)
                        ? (int)swDocumentTypes_e.swDocASSEMBLY
                        : -1;

                    if (type < 0)
                    {
                        continue;
                    }

                    int err = 0;
                    int war = 0;
                    var modelDoc = SwApp.OpenDoc6(info.Path, type, (int)swOpenDocOptions_e.swOpenDocOptions_Silent, "",
                      ref err, ref war);
                    if (modelDoc == null)
                    {
                        MessageBox.Show("Current Part File is not in System!!!! Please Update from solidworks for Respective File--- " + info.Path + "---- Please give path for this part");
                        OpenFileDialog fdlg = new OpenFileDialog();
                        DialogResult result = fdlg.ShowDialog();
                        if (result == DialogResult.OK)
                        {
                            var path = fdlg.FileName;
                            modelDoc = SwApp.OpenDoc6(path, type, (int)swOpenDocOptions_e.swOpenDocOptions_Silent, "",
                         ref err, ref war);

                        }
                    }
                    var modelExt = modelDoc.Extension;
                    var propManager = modelExt.CustomPropertyManager[""];

                    var checkExistance = (propManager.Add2("Part Number",
                 (int)swCustomInfoType_e.swCustomInfoText,
                 (modelDoc.CustomInfo["Part Number"])));
                    if (checkExistance == 0 || checkExistance == -1)
                    {
                        propManager.Set2("Part Number", info.PartNumber);
                    }
                    else if (checkExistance == 1)
                    {
                        propManager.Add2("Part Number", (int)swCustomInfoType_e.swCustomInfoText,
                         info.PartNumber);
                    }
                }

                MessageBox.Show("Custom Property 'Part Number' updated successfully!!");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }

        }




        /// <summary>
        /// function for save assembly info in database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmSave_Load()
        {
            var asm_id = SQLQueries.GetAsm_id(this.baseInfo.PartNum);
            DataTable verdt = SQLQueries.GetLatestVersion(asm_id);
            if(verdt == null)
            {
                this.CustomCheck = false;
                this.StandardCheck = false;
            }
            else
            {
                if (Convert.ToBoolean(verdt.Rows[verdt.Rows.Count - 1][2]))
                {
                    this.CustomCheck = false;
                    this.StandardCheck = true;
                }
                else
                {
                    this.CustomCheck = true;
                    this.StandardCheck = false;
                }
            }
          

            this.list = new List<SavePropertiesModel>();
            this.list.Clear();

            table = new DataTable();

            table.Columns.Add("SNO");
            table.Columns.Add("Part Number");
            table.Columns.Add("Item Name");
            table.Columns.Add("Unit Qty");
            table.Columns.Add("Client Name");
            table.Columns.Add("Origin");
            table.Columns.Add("Master Project");
            table.Columns.Add("Project Number");
            table.Columns.Add("Make");
            table.Columns.Add("Material");
            table.Columns.Add("Design By");
            table.Columns.Add("Design Date");
            table.Columns.Add("Drawn By");
            table.Columns.Add("Drawn Date");
            table.Columns.Add("Upload By");
            table.Columns.Add("Upload Date");
            table.Columns.Add("Modified By");
            table.Columns.Add("Modified Date");
            table.Columns.Add("Unit Weight(kg)");
            table.Columns.Add("Path");


            var aggAsmInfos = new List<AssemblyInfo>();
            aggAsmInfos.Add(baseInfo);
            AssemblyInfo.AsmAggregate(baseInfo, 1, aggAsmInfos);

            var sno = 1;
           
            foreach(var info in aggAsmInfos)
            
            {
                table.Rows.Add(sno, info.PartNum, info.properties["ITEM NAME"].StrValue, info.qty, info.ClientName, info.origin, info.MasterProject, info.ProjectNumber, info.Make, info.Material, info.designBy,
                    info.DesignDate, info.DrawnBy, info.DrawnDate, "", "", "", "", info.uWeight, info.path);
                sno++;
            }


            foreach (var info in aggAsmInfos)
            {
                var Sno = 1;
                if (string.IsNullOrWhiteSpace(info.PartNum))
                {
                    this.list.Add(new SavePropertiesModel()
                    {
                        SNO = Sno.ToString(),
                        PartNumber = info.PartNum,
                        Path = info.path
                    });
                    Sno++;
                }
            }


            string[] partNumbers = null;

            var sqlSelect = "select item_name from Assembly_info";
            using (SqlConnection conn = new SqlConnection(SQLConnection.connection_string))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand(sqlSelect, conn))
                {
                    using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                    {
                        using (DataTable dt = new DataTable())
                        {
                            sda.Fill(dt);
                            if (dt.Rows.Count == 0)
                            {
                                conn.Close();
                                return;
                            }
                            partNumbers = new string[dt.Rows.Count];
                            for (int index = 0; index < dt.Rows.Count; index++)
                            {
                                DataRow row = dt.Rows[index];
                                partNumbers[index] = row[0].ToString();
                            }
                        }
                    }
                }

                conn.Close();
            } // using

        }




        /// <summary>
        /// function for save the assembly info in database
        /// </summary>
        private void BtnSave(AssemblyInfo asminfo)
        {
            List<AssemblyInfo> asmInfos = new List<AssemblyInfo>();
            asmInfos.Clear();
            var agAsmInfo = new List<AssemblyInfo>();
            agAsmInfo.Add(asminfo);
            AssemblyInfo.AsmAggregate(asminfo, 1, agAsmInfo);


            asmInfos = ExtractExistingAsm(asminfo.PartNum);
            //if(asmInfos == null)
            //{
            //    //Insert Assembly into database
            //    InsertAssembly();
            //}

            var aggExistingInfo = new List<AssemblyInfo>();
            aggExistingInfo.Add(asmInfos[asmInfos.Count-1]);
            AssemblyInfo.AsmAggregate(asmInfos[asmInfos.Count - 1], 1, aggExistingInfo);

            if (aggExistingInfo.Count != agAsmInfo.Count)
            {
                //update version of existing assembly
                UpdateAsm(asminfo);
                updated++;
            }
            else
            {
                if (!asminfo.qty.Equals(asmInfos[asmInfos.Count - 1].qty) ||
                    !asminfo.ClientName.Equals(asmInfos[asmInfos.Count - 1].ClientName, StringComparison.InvariantCultureIgnoreCase)|| 
                    !asminfo.origin.Equals(asmInfos[asmInfos.Count - 1].origin, StringComparison.InvariantCultureIgnoreCase) ||
                    !asminfo.MasterProject.Equals(asmInfos[asmInfos.Count - 1].MasterProject, StringComparison.InvariantCultureIgnoreCase) ||
                    !asminfo.ProjectNumber.Equals(asmInfos[asmInfos.Count - 1].ProjectNumber, StringComparison.InvariantCultureIgnoreCase) ||
                    !asminfo.Make.Equals(asmInfos[asmInfos.Count - 1].Make, StringComparison.InvariantCultureIgnoreCase) ||
                    !asminfo.Material.Equals(asmInfos[asmInfos.Count - 1].Material, StringComparison.InvariantCultureIgnoreCase) ||
                    !asminfo.designBy.Equals(asmInfos[asmInfos.Count - 1].designBy, StringComparison.InvariantCultureIgnoreCase) ||
                    !asminfo.DesignDate.Equals(asmInfos[asmInfos.Count - 1].DesignDate, StringComparison.InvariantCultureIgnoreCase) ||
                    !asminfo.DrawnBy.Equals(asmInfos[asmInfos.Count - 1].DrawnBy, StringComparison.InvariantCultureIgnoreCase) ||
                    !asminfo.uWeight.Equals(asmInfos[asmInfos.Count - 1].uWeight, StringComparison.InvariantCultureIgnoreCase) ||
                    !asminfo.DrawnDate.Equals(asmInfos[asmInfos.Count - 1].DrawnDate, StringComparison.InvariantCultureIgnoreCase))
                {
                    //update version
                    UpdateAsm(asminfo);
                    updated++;
                }

                else 
                {
                    foreach(var item in agAsmInfo)
                    {
                        var exists = 0;
                        foreach (var exstngItem in aggExistingInfo)
                        {
                            if (item.PartNum == exstngItem.PartNum && item.qty == exstngItem.qty)
                            {
                                exists++;
                                break;
                            }
                        }
                        if(exists == 0)
                        {
                            //update
                            UpdateAsm(asminfo);
                            break;
                        }
                    }
                }
            } 
        }




        /// <summary>
        /// save main assembly in database
        /// </summary>
        public void InsertAssembly()
        {
            var aggAsmInfos = new List<AssemblyInfo>();
            aggAsmInfos.Add(baseInfo);
            AssemblyInfo.AsmAggregate(baseInfo, 1, aggAsmInfos);

            int inserted = 0;
           
            const string sql = @"INSERT INTO Assembly_info
           (part_number
           ,item_name
           ,path_str)
     VALUES
           (@part_number
           ,@item_name
           ,@path_str)";

            const string sqlSelect = @"SELECT [id] FROM [dbo].[Assembly_info] where  part_number = @part_number";

            using (SqlConnection conn = new SqlConnection(SQLConnection.connection_string))
            {
                conn.Open();
                foreach (var Item in aggAsmInfos)
                {
                    if (string.IsNullOrWhiteSpace(Item.PartNum))
                    {
                        MessageBox.Show("Part Number is Missing!");
                        return;
                    }
                    if (string.IsNullOrWhiteSpace(baseInfo.DesignDate))
                    {
                        MessageBox.Show("Design Date of Main Assembly is Missing!");
                        return;
                    }
                    var asmID = SQLQueries.GetAsm_id(Item.PartNum);
                    DataTable verdt = null;
                    if (asmID > 0)
                    {
                        verdt = SQLQueries.GetLatestVersion(asmID);

                        string isStandard = verdt.Rows[verdt.Rows.Count - 1][2].ToString();

                        if (this.customCheck && isStandard.Equals("true", StringComparison.InvariantCultureIgnoreCase))
                        {
                            MessageBox.Show("Can not convert 'Standard Data' To 'Custom Build'!");
                            return;
                        }

                    }

                    using (
                        
                        
                        SqlCommand cmdSelect = new SqlCommand(sqlSelect, conn))
                    {
                        cmdSelect.CommandType = CommandType.Text;
                        // cmdSelect.Parameters.AddWithValue("@path_str", Item.path);
                        cmdSelect.Parameters.AddWithValue("@part_number", Item.PartNum);
                        using (SqlDataAdapter sda = new SqlDataAdapter(cmdSelect))
                        {
                            using (DataTable dt = new DataTable())
                            {
                                sda.Fill(dt);
                                if (dt.Rows.Count > 0)
                                {
                                    BtnSave(Item);
                                    var Item_id = SQLQueries.GetAsm_id(Item.PartNum);
                                    DataTable Vdt = SQLQueries.GetLatestVersion(Item_id);
                                    var latestVer = (verdt.Rows[verdt.Rows.Count - 1][1]).ToString();
                                    UpdateVersion(latestVer, Item_id);
                                    continue;
                                }
                            }
                        }
                    }
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@part_number", Item.PartNum);
                        cmd.Parameters.AddWithValue("@item_name", Item.ItemName);
                        cmd.Parameters.AddWithValue("@path_str", Item.path);
                        cmd.ExecuteNonQuery();
                        inserted++;
                        var asmid = SQLQueries.GetAsm_id(Item.PartNum);
                        SaveVersion("V1", asmid);
                        verdt = SQLQueries.GetLatestVersion(asmid);
                        var verId = Convert.ToInt32(verdt.Rows[verdt.Rows.Count - 1][0]);
                        SaveAsmProperties(Item, verId, asmid
                            );
                    }
                }

                conn.Close();
            }
            if(inserted > 0 || updated > 0)
            {
                SaveChilds(aggAsmInfos);
            }

            MessageBox.Show("Assembly saved Successfully");
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="version"></param>
        /// <param name="asmID"></param>
        public void SaveVersion(string version, int asmID)
        {
            string saveV = @"insert into [dbo].[Versions] values ('" + version + "', '" + asmID + "', '" + this.UserInfo + "', '" + DateTime.Now.ToString() + "', '" + this.standardCheck + "' )";
            using (SqlConnection con = new SqlConnection(SQLConnection.connection_string))
            {
                using (SqlCommand cmd = new SqlCommand(saveV, con))
                {
                    cmd.CommandType = CommandType.Text;
                    con.Open();
                    cmd.ExecuteNonQuery();
                }

            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="version"></param>
        /// <param name="asmID"></param>
        public void UpdateVersion(string version, int asmID)
        {
            string saveV = @"update [dbo].[Versions] set IsStandard ='" + this.standardCheck + "' where [Version]= '" + version + "' and  [Asm_id]=  '" + asmID + "'";
            using (SqlConnection con = new SqlConnection(SQLConnection.connection_string))
            {
                using (SqlCommand cmd = new SqlCommand(saveV, con))
                {
                    cmd.CommandType = CommandType.Text;
                    con.Open();
                    cmd.ExecuteNonQuery();
                }

            }
        }




        /// <summary>
        /// function for save the assembly info in database
        /// </summary>
        private void SaveAsmProperties(AssemblyInfo Item, int version_id, int asmID)
        {
            const string sql = @"INSERT INTO Asm_Version_Info
           (AsmInfo_id
           ,Version_id
           ,Unit_qty
           ,client_name
           ,origin
           ,master_project
           ,project_number
           ,make
           ,Material
           ,design_by
           ,design_date
           ,drawn_by
           ,drawn_date
           ,upload_by
           ,upload_date
           ,modified_by
           ,modified_date
           ,UnitWeight)
     VALUES
           (@AsmInfo_id
           ,@Version_id
           ,@quantity
           ,@client_name
           ,@origin
           ,@master_project
           ,@project_number
           ,@make
           ,@Material
           ,@design_by
           ,@design_date
           ,@drawn_by
           ,@drawn_date
           ,@upload_by
           ,@upload_date
           ,@modified_by
           ,@modified_date
           ,@UnitWeight)";

            using (SqlConnection conn = new SqlConnection(SQLConnection.connection_string))
            {
                conn.Open();

                using (SqlCommand cmd = new SqlCommand(sql, conn))
                {
                    cmd.Parameters.AddWithValue("@AsmInfo_id", asmID);
                    cmd.Parameters.AddWithValue("@Version_id", version_id);
                    cmd.Parameters.AddWithValue("@quantity", Item.qty);
                    cmd.Parameters.AddWithValue("@client_name", Item.ClientName);
                    cmd.Parameters.AddWithValue("@origin", Item.origin);
                    cmd.Parameters.AddWithValue("@master_project", Item.MasterProject);
                    cmd.Parameters.AddWithValue("@project_number", Item.ProjectNumber);
                    cmd.Parameters.AddWithValue("@make", Item.Make);
                    cmd.Parameters.AddWithValue("@Material", Item.Material);
                    cmd.Parameters.AddWithValue("@design_by", Item.designBy);
                    cmd.Parameters.AddWithValue("@design_date", Item.DesignDate);
                    cmd.Parameters.AddWithValue("@drawn_by", Item.DrawnBy);
                    cmd.Parameters.AddWithValue("@drawn_date", Item.DrawnDate);
                    cmd.Parameters.AddWithValue("@upload_by", "");
                    cmd.Parameters.AddWithValue("@upload_date", "");
                    cmd.Parameters.AddWithValue("@modified_by", "");
                    cmd.Parameters.AddWithValue("@modified_date", "");
                    cmd.Parameters.AddWithValue("@UnitWeight", Item.uWeight);

                    cmd.ExecuteNonQuery();
                }
                conn.Close();
            }
        }




        /// <summary>
        /// save childs with their respect parent in database
        /// </summary>
        public void SaveChilds(List<AssemblyInfo> aggAsmInfos)
        {
            var asmChildInfo = new List<Tuple<string, string, int, int, int>>();

            var asmid = SQLQueries.GetAsm_id(baseInfo.PartNum);

            var verdt = SQLQueries.GetLatestVersion(asmid);
            var asmVer = Convert.ToInt32(verdt.Rows[verdt.Rows.Count - 1][0]);

            var pId = PurgeId(baseInfo.PartNum);
            DeleteAsmChildInfo(baseInfo.PartNum, pId);
            AddTopAsm(baseInfo,
                baseInfo.PartNum, ref asmChildInfo, asmVer, asmVer);

            foreach (var asmInfo in aggAsmInfos)
            {
                DeleteAsmChildInfo(asmInfo.PartNum, pId);
                AddChildInfo(asmInfo,
                    asmInfo.PartNum, ref asmChildInfo, asmVer);
            }

            foreach (var tuple in asmChildInfo)
            {
                using (SqlConnection con = new SqlConnection(SQLConnection.connection_string))
                {
                    using (SqlCommand cmd = new SqlCommand("INSERT_UPDATE_ASSEMBLY_CHILD_INFO", con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.Parameters.Add("@PARENT_ASSEMBLY", SqlDbType.NVarChar).Value = tuple.Item1;
                        cmd.Parameters.Add("@CHILD_ASSEMBLY", SqlDbType.NVarChar).Value = tuple.Item2;
                        cmd.Parameters.Add("@COUNT_CHILD", SqlDbType.NVarChar).Value = tuple.Item3;
                        cmd.Parameters.Add("@Version", SqlDbType.NVarChar).Value = tuple.Item4;
                        cmd.Parameters.Add("@AsmVersion", SqlDbType.NVarChar).Value = tuple.Item5;
                        con.Open();
                        cmd.ExecuteNonQuery();
                    } // using
                } // using
            } // foreach

            SQLQueries.FileUserRecord(this.UserInfo, this.baseInfo.PartNum);
            SQLQueries.SaveProjectRecord(this.baseInfo.PartNum, this.baseInfo.path);

        }



        /// <summary>
        /// 
        /// </summary>
        /// <param name="asmInfo"></param>
        /// <param name="parent"></param>
        /// <param name="asmChildInfo"></param>
        private void AddTopAsm(AssemblyInfo asmInfo,
            string parent,
            ref List<Tuple<string, string, int, int, int>> asmChildInfo, int childV_id, int parentV_id)
        {
            asmChildInfo.Add(new Tuple<string, string, int, int, int>(parent, asmInfo.PartNum, asmInfo.qty, childV_id, parentV_id));
        }



        /// <summary>
        /// 
        /// </summary>
        /// <param name="asmInfo"></param>
        /// <param name="parent"></param>
        /// <param name="asmChildInfo"></param>
        private void AddChildInfo(AssemblyInfo asmInfo,
            string parent,
            ref List<Tuple<string, string, int, int, int>> asmChildInfo, int parentV_id)
        {
            foreach (var info in asmInfo.childs)
            {
               // DeleteAsmChildInfo(info.PartNum);
                var childId = SQLQueries.GetAsm_id(info.PartNum);
                var verdt = SQLQueries.GetLatestVersion(childId);
                var ver = Convert.ToInt32(verdt.Rows[verdt.Rows.Count - 1][0]);
                asmChildInfo.Add(new Tuple<string, string, int, int, int>(parent, info.PartNum, info.qty, ver, parentV_id));
            }
        }




        /// <summary>
        /// save assembly with updated version
        /// </summary>
        public void UpdateAsm(AssemblyInfo assemblyInfo)
        {
            var asm_id = SQLQueries.GetAsm_id(assemblyInfo.PartNum);
            DataTable verdt = SQLQueries.GetLatestVersion(asm_id);
            var latestVer = (verdt.Rows[verdt.Rows.Count - 1][1]).ToString();
            var pieces = latestVer.Split(new[] { 'V' }, 2);
            var newVer = (Convert.ToInt32(pieces[1]) + 1).ToString();
            SaveVersion(("V" + newVer), asm_id);
            DataTable latestV = SQLQueries.GetLatestVersion(asm_id);
            var verId = Convert.ToInt32(latestV.Rows[latestV.Rows.Count - 1][0]);
            SaveAsmProperties(assemblyInfo, verId, asm_id);
        }



        /// <summary>
        /// get assembly info from database
        /// </summary>
        /// <param name="asmPNum"></param>
        private List<AssemblyInfo> ExtractExistingAsm(string asmPNum)
        {
            string sql = @"SELECT [child_id],[AsmVersion_id] FROM [dbo].[Assembly_Child_Info] where child_id = (select [id] from [dbo].[Assembly_info] where part_number = '" + asmPNum + "')";
            DataTable tableAsmInfo;
            using (con = new SqlConnection(SQLConnection.connection_string))
            {
                using (SqlCommand cmd = new SqlCommand(sql, con))
                {
                    cmd.CommandType = CommandType.Text;
                    using (Adapter = new SqlDataAdapter(cmd))
                    {
                        using (dt = new DataTable())
                        {
                            Adapter.Fill(dt);
                        }
                    }

                    if (dt == null ||
                        dt.Rows.Count == 0)
                    {
                        return null;
                    }
                } // using
            } // using


            var asmId = new AsmID { id = Convert.ToInt32(dt.Rows[0][0]) };
            var asmVersion = new AsmID { asmVersion = Convert.ToInt32(dt.Rows[dt.Rows.Count - 1][1]) };


            // do not read chils, as diolg is loaded, read child while comparing specific asm
            var childs = AddChilds(asmId, asmVersion, out var asmInfo, true);
            asmId.childIds = childs;
            asmIdsWithChild.Add(asmId);
            this.asmInfos.Add(asmInfo);
            return asmInfos;
        }



        /// <summary>
        /// get info of existing assembly from database
        /// </summary>
        /// <param name="asmId"></param>
        /// <param name="asmInfo"></param>
        /// <returns></returns>
        public static List<AsmID> AddChilds(AsmID asmId, AsmID asmVersion, out AssemblyInfo asmInfo, bool readChild)
        {
            DataTable tableAsmInfo;
            DataTable tableAssemblyInfo;
            var childs = new List<AsmID>();
            var child_select = "select [child_id] from Assembly_Child_Info where assembly_id = '" + asmId.id + "' and AsmVersion_id = '" + asmVersion.asmVersion + "' ";
            DataTable childTable;
            using (var con = new SqlConnection(SQLConnection.connection_string))
            {
                using (SqlCommand cmd = new SqlCommand(child_select, con))
                {
                    cmd.CommandType = CommandType.Text;
                    using (var Adapter = new SqlDataAdapter(cmd))
                    {
                        using (childTable = new DataTable())
                        {
                            Adapter.Fill(childTable);
                        }
                    }
                } // using

                using (SqlCommand cmd = new SqlCommand(asm_info_table + asmId.id, con))
                {
                    cmd.CommandType = CommandType.Text;
                    using (var Adapter = new SqlDataAdapter(cmd))
                    {
                        using (tableAssemblyInfo = new DataTable())
                        {
                            Adapter.Fill(tableAssemblyInfo);
                        }
                    }
                } // using

                using (SqlCommand cmd = new SqlCommand(asm_info + asmId.id, con))
                {
                    cmd.CommandType = CommandType.Text;
                    using (var Adapter = new SqlDataAdapter(cmd))
                    {
                        using (tableAsmInfo = new DataTable())
                        {
                            Adapter.Fill(tableAsmInfo);
                        }
                    }
                } // using
            } // using

            if (tableAssemblyInfo.Rows.Count == 0)
            {
                asmInfo = null;
                return null;
            }
            asmInfo = GetAsmInfo(tableAsmInfo.Rows[tableAsmInfo.Rows.Count-1], tableAssemblyInfo.Rows[0]);

            if (!readChild)
            {
                return null;
            }

            foreach (DataRow childData in childTable.Rows)
            {
                var asmChild = new AsmID
                {
                    id = Convert.ToInt32(childData["child_id"])
                };

                if(asmChild.id == asmId.id)
                {
                    continue;
                }
                childs.Add(asmChild);

                var childIds = AddChilds(asmChild, asmVersion, out var child, true);
                asmChild.childIds = childIds;
                //child.qty = asmChild.count;
                asmInfo.childs.Add(child);
            } // foreach

            return childs;
        }



        /// <summary>
        /// fill the properties of assembly in a list
        /// </summary>
        /// <param name="row"></param>
        /// <returns></returns>
        private static AssemblyInfo GetAsmInfo(DataRow row, DataRow table)
        {
            var asmInfo = new AssemblyInfo();
            asmInfo.properties["PART NUMBER"].StrValue = table["part_number"].ToString();
            asmInfo.properties["ITEM NAME"].StrValue = table["item_name"].ToString();
            asmInfo.qty = Convert.ToInt16(row["Unit_qty"]);
            asmInfo.properties["CLIENT"].StrValue = Convert.ToString(row["client_name"]);
            asmInfo.properties["ORIGIN"].StrValue = row["origin"].ToString();
            asmInfo.properties["MASTER PROJECT"].StrValue = row["master_project"].ToString();
            asmInfo.properties["MODEL NUMBER"].StrValue = row["project_number"].ToString();
            asmInfo.properties["MAKE"].StrValue = row["make"].ToString();
            asmInfo.properties["MATERIAL"].StrValue = row["Material"].ToString();
            asmInfo.properties["DESIGN BY"].StrValue = row["design_by"].ToString();
            asmInfo.properties["Design Date"].StrValue = row["design_date"].ToString();
            asmInfo.properties["DRAWN BY"].StrValue = row["drawn_by"].ToString();
            asmInfo.properties["Drawn Date"].StrValue = row["drawn_date"].ToString();
            asmInfo.properties["WEIGHT"].StrValue = row["UnitWeight"].ToString();
            asmInfo.path = table["path_str"].ToString();

            return asmInfo;
        }




        /// <summary>
        /// delete last child info from Assembly_Child_Info, insert those into History table with new purge id And insert new child info into Assembly_Child_Info
        /// </summary>
        /// <param name="pname"></param>
        private void DeleteAsmChildInfo(string pname, int pId)
        {
            DataTable datatable = null;
            string select = @"select * from [dbo].[Assembly_Child_Info] where assembly_id = (select id from [dbo].[Assembly_info] where part_number='" + pname + "' )";

            using (SqlConnection con = new SqlConnection(SQLConnection.connection_string))
            {
                using (SqlCommand cmd = new SqlCommand(select, con))
                {
                    cmd.CommandType = CommandType.Text;
                    using (var Adapter = new SqlDataAdapter(cmd))
                    {
                        using (datatable = new DataTable())
                        {
                            Adapter.Fill(datatable);
                        }
                    }
                }
            }

            if(datatable.Rows.Count > 0)
            {
                SaveHistoryChilds(datatable, pId);
            }

            string deleteAsmChildInfo = @"delete FROM [dbo].[Assembly_Child_Info] where assembly_id = (select id from [dbo].[Assembly_info] where part_number='" + pname + "' )";
            using (SqlConnection con = new SqlConnection(SQLConnection.connection_string))
            {
                using (SqlCommand cmd = new SqlCommand(deleteAsmChildInfo, con))
                {
                    cmd.CommandType = CommandType.Text;
                    con.Open();
                    cmd.ExecuteNonQuery();
                }

            }
        }


        /// <summary>
        /// 
        /// </summary>
        public void SaveHistoryChilds(DataTable dataTable, int pId)
        {
            foreach(DataRow row in dataTable.Rows)
            {
               

                string insertAsmChildInfo = @"insert into [dbo].[Asm_ChildInfo_History] values ('" + row[0] + "', '" + row[1] + "', '" + row[2] + "',  '" + row[3] + "' ," +
                    " '" + row[4] + "' , '" + row[5] + "',  '" + pId + "' )";
                using (SqlConnection con = new SqlConnection(SQLConnection.connection_string))
                {
                    using (SqlCommand cmd = new SqlCommand(insertAsmChildInfo, con))
                    {
                        cmd.CommandType = CommandType.Text;
                        con.Open();
                        cmd.ExecuteNonQuery();
                    }

                }
            }
          
        }


        public int PurgeId(string pname)
        {
            DataTable Purgedt = null;
            var pId = 0;
            string select = @"select [Purge_id] FROM [dbo].[Asm_ChildInfo_History] where assembly_id =(select id from [dbo].[Assembly_info] where part_number='" + pname + "' ) ";

            using (SqlConnection con = new SqlConnection(SQLConnection.connection_string))
            {
                using (SqlCommand cmd = new SqlCommand(select, con))
                {
                    cmd.CommandType = CommandType.Text;
                    using (var Adapter = new SqlDataAdapter(cmd))
                    {
                        using (Purgedt = new DataTable())
                        {
                            Adapter.Fill(Purgedt);
                        }
                    }
                }
            }
            if (Purgedt.Rows.Count == 0)
            {
                pId = 1;
            }
            else
            {
                pId = Convert.ToInt32(Purgedt.Rows[Purgedt.Rows.Count - 1][0]) + 1;
            }
            return pId;
        }



        /// <summary>
        /// func for closing window
        /// </summary>
        void CloseWindow()
        {
            Close?.Invoke();
        }

        public Action Close { get; set; }

    }

    public class AsmID
    {
        public int id;
        public List<AsmID> childIds;
        public int count;
        public int asmVersion;
    }

}
