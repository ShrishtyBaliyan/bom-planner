﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SaveCustomProperties
{
    public class SavePropertiesModel
    {
        [DisplayName("S No")]
        public string SNO { get; set; }

        [DisplayName("Part Number")]
        public string PartNumber { get; set; } = string.Empty;

        [DisplayName("Path")]
        public string Path { get; set; } = string.Empty;
    }
}
