﻿using CadData;
using System.Windows;

namespace SaveCustomProperties
{
    /// <summary>
    /// Interaction logic for FillPartNum.xaml
    /// </summary>
    public partial class FillPartNum : Window
    {
        public FillPartNum()
        {
            InitializeComponent();
            Loaded += View_Loaded;
        }

        private void View_Loaded(object sender, RoutedEventArgs e)
        {
            if (DataContext is ICloseWindow vm)
            {
                vm.Close += () =>
                {
                    this.Close();
                };
            }
        }
    }
}
