﻿using CadData;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Input;

namespace PartListByType.ViewModel
{
    public class PartListByTypeViewModel : INotifyPropertyChanged, ICloseWindow
    {
        public List<string> cmbTypes;
        public List<string> CmbTypes
        {
            get => SQLConnection.Item_Group.Split(',').ToList();
            set
            {
                this.cmbTypes = value;

                OnPropertyChanged("CmbTypes");
            }
        }


        public PartListByTypeViewModel()
        {

        }

        /// <summary>
        /// 
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string PropertyName)
        {
            if (this.PropertyChanged == null)
                return;
            this.PropertyChanged((object)this, new PropertyChangedEventArgs(PropertyName));
        }
        public string selectedType { get; set; }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="rootAssembly"></param>
        /// <param name="path"></param>
        public void BOMPartListByType(AssemblyInfo rootAssembly, string path)
        {
            PartListbyType partListbyType = new PartListbyType();
            partListbyType.BOMPartListByType(rootAssembly, path, this.selectedType);
        }



        /// <summary>
        ///  Ok Command
        /// </summary>
        private ICommand submitCommand;
        public ICommand SubmitCommand
        {
            get
            {
                if (this.submitCommand == null)
                    this.submitCommand = (ICommand)new ICommandInterface(new Action<object>(this.executeSubmit), new Func<object, bool>(this.canExecuteSubmit));
                return this.submitCommand;
            }
        }

        private bool canExecuteSubmit(object arg) => true;

        private void executeSubmit(object parameter)
        {
            if (this.selectedType == null)
            {
                string message = "Please select any part!";
                string title = "Editing Window";
                MessageBoxButtons buttons = MessageBoxButtons.OK;
                DialogResult result = MessageBox.Show(message, title, buttons);
                return;
            }
            CloseWindow();
        }



        /// <summary>
        /// Close Window
        /// </summary>
        private ICommand closeCommand;
        public ICommand CloseCommand
        {
            get
            {
                if (this.closeCommand == null)
                    this.closeCommand = (ICommand)new ICommandInterface(new Action<object>(this.executeClose), new Func<object, bool>(this.canExecuteClose));
                return this.closeCommand;
            }
        }

        private bool canExecuteClose(object arg) => true;

        private void executeClose(object parameter)
        {
            this.selectedType = null;
            CloseWindow();
        }


        /// <summary>
        /// 
        /// </summary>
        void CloseWindow()
        {
            Close?.Invoke();
        }

        /// <summary>
        /// 
        /// </summary>
        public Action Close { get; set; }
    }
}

