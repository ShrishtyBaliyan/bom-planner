﻿using BOMCreator;
using CadData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PartListByType
{
    public class PartListbyType
    {

        /// <summary>
        /// 
        /// </summary>
        /// <param name="rootAssembly"></param>
        /// <param name="path"></param>
        /// <param name="selectedItem"></param>
        public void BOMPartListByType(AssemblyInfo rootAssembly, string path, string selectedItem)
        {
            if (!string.IsNullOrEmpty(selectedItem))
            {
                var bomProcessor = BOMProcessorFactory.Create(rootAssembly,
                                           BOMType.General,
                                           FileType.part,
                                           selectedItem);

                bomProcessor.Process(ReportType.Genral);
            }
        }

    }
}
