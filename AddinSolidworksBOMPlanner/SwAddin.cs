using BOMCreator;
using BOMPlanner.SQL;
using BOMPlanner.View;
using BOMPlanner.ViewModel;
using CadData;
using EditItemGroup;
using EditItemGroup.ViewModel;
using ExportImage;
using ExportImage.ViewModel;
using PartListByType;
using PartListByType.ViewModel;
using ReleaseBOM;
using SaveCustomProperties;
using SaveCustomProperties.ViewModel;
using SolidWorks.Interop.sldworks;
using SolidWorks.Interop.swconst;
using SolidWorks.Interop.swpublished;
using SolidWorksTools;
using SolidWorksTools.File;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Runtime.InteropServices;
using MessageBox = System.Windows.Forms.MessageBox;
using Type = System.Type;

namespace AddinSolidworksBOMPlanner
{
    /// <summary>
    /// Summary description for SwCSharpAddin1.
    /// </summary>
    [Guid("eeacd33c-76ef-4c41-a684-a5e232d5819e"), ComVisible(true)]
    [SwAddin(
        Description = "BOM Planner",
        Title = "BOM Planner",
        LoadAtStartup = true
        )]
    public class SwAddin : ISwAddin
    {
        #region Local Variables
        ISldWorks iSwApp = null;
        ICommandManager iCmdMgr = null;
        int addinID = 0;
        BitmapHandler iBmp;
        public const int mainCmdBOMCreator = 9;
        public const int mainCmdBOMSetting = 10;


        public const int ItemIDSave = 0;
        public const int ItemIDCreateProject = 1;
        public const int ItemIDReleaseBOM = 2;
        public const int ItemIDExportImg = 3;
        public const int ItemIDAddItems = 4;
        public const int ItemIDPartListByType = 5;
        public const int ItemIDDesign = 6;
        public const int ItemIDDueDate = 7;
        public const int ItemIDMissingPNum = 8;
        public const int ItemIDnewUser = 9;



        public const int flyoutGroupID = 91;

        #region Event Handler Variables
        Hashtable openDocs = new Hashtable();
        SldWorks SwEventPtr = null;
        #endregion


        UserLoginViewModel userLoginViewModel = new UserLoginViewModel();

        #region Property Manager Variables
        //UserPMPage ppage = null;
        #endregion


        // Public Properties
        public ISldWorks SwApp
        {
            get { return iSwApp; }
        }
        public ICommandManager CmdMgr
        {
            get { return iCmdMgr; }
        }

        public Hashtable OpenDocs
        {
            get { return openDocs; }
        }

        public static string userCheck;
        public static string userGrpName;

        #endregion

        #region SolidWorks Registration
        [ComRegisterFunction]
        public static void RegisterFunction(Type t)
        {
            #region Get Custom Attribute: SwAddinAttribute
            SwAddinAttribute SWattr = null;
            Type type = typeof(SwAddin);

            foreach (System.Attribute attr in type.GetCustomAttributes(false))
            {
                if (attr is SwAddinAttribute)
                {
                    SWattr = attr as SwAddinAttribute;
                    break;
                }
            }

            #endregion

            try
            {
                Microsoft.Win32.RegistryKey hklm = Microsoft.Win32.Registry.LocalMachine;
                Microsoft.Win32.RegistryKey hkcu = Microsoft.Win32.Registry.CurrentUser;

                string keyname = "SOFTWARE\\SolidWorks\\Addins\\{" + t.GUID.ToString() + "}";
                Microsoft.Win32.RegistryKey addinkey = hklm.CreateSubKey(keyname);
                addinkey.SetValue(null, 0);

                addinkey.SetValue("Description", SWattr.Description);
                addinkey.SetValue("Title", SWattr.Title);

                keyname = "Software\\SolidWorks\\AddInsStartup\\{" + t.GUID.ToString() + "}";
                addinkey = hkcu.CreateSubKey(keyname);
                addinkey.SetValue(null, Convert.ToInt32(SWattr.LoadAtStartup), Microsoft.Win32.RegistryValueKind.DWord);
            }
            catch (NullReferenceException nl)
            {
                Console.WriteLine("There was a problem registering this dll: SWattr is null. \n\"" + nl.Message + "\"");
                System.Windows.Forms.MessageBox.Show("There was a problem registering this dll: SWattr is null.\n\"" + nl.Message + "\"");
            }

            catch (Exception e)
            {
                Console.WriteLine(e.Message);

                System.Windows.Forms.MessageBox.Show("There was a problem registering the function: \n\"" + e.Message + "\"");
            }
        }

        [ComUnregisterFunction]
        public static void UnregisterFunction(Type t)
        {
            try
            {
                Microsoft.Win32.RegistryKey hklm = Microsoft.Win32.Registry.LocalMachine;
                Microsoft.Win32.RegistryKey hkcu = Microsoft.Win32.Registry.CurrentUser;

                string keyname = "SOFTWARE\\SolidWorks\\Addins\\{" + t.GUID.ToString() + "}";
                hklm.DeleteSubKey(keyname);

                keyname = "Software\\SolidWorks\\AddInsStartup\\{" + t.GUID.ToString() + "}";
                hkcu.DeleteSubKey(keyname);
            }
            catch (NullReferenceException nl)
            {
                Console.WriteLine("There was a problem unregistering this dll: " + nl.Message);
                System.Windows.Forms.MessageBox.Show("There was a problem unregistering this dll: \n\"" + nl.Message + "\"");
            }
            catch (Exception e)
            {
                Console.WriteLine("There was a problem unregistering this dll: " + e.Message);
                System.Windows.Forms.MessageBox.Show("There was a problem unregistering this dll: \n\"" + e.Message + "\"");
            }
        }

        #endregion

        #region ISwAddin Implementation
        public SwAddin()
        {
          
        }

        public bool ConnectToSW(object ThisSW, int cookie)
        {
            //For User Login
            UserLogin();
            
            iSwApp = (ISldWorks)ThisSW;
            addinID = cookie;

            //Setup callbacks
            iSwApp.SetAddinCallbackInfo(0, this, addinID);

            #region Setup the Command Manager
            iCmdMgr = iSwApp.GetCommandManager(cookie);
            AddCommandMgr();
            #endregion

            #region Setup the Event Handlers
            SwEventPtr = (SldWorks)iSwApp;
            openDocs = new Hashtable();
            AttachEventHandlers();
            #endregion

            #region Setup Sample Property Manager
            //AddPMP();
            #endregion

            return true;
        }


        public bool DisconnectFromSW()
        {
            RemoveCommandMgr();
            //RemovePMP();
            DetachEventHandlers();

            Marshal.ReleaseComObject(iCmdMgr);
            iCmdMgr = null;
            Marshal.ReleaseComObject(iSwApp);
            iSwApp = null;
            //The addin _must_ call GC.Collect() here in order to retrieve all managed code pointers 
            GC.Collect();
            GC.WaitForPendingFinalizers();

            GC.Collect();
            GC.WaitForPendingFinalizers();

            return true;
        }
        #endregion

        #region UI Methods
        public void AddCommandMgr()
        {

            ICommandGroup IcmdBOMCreator;
            ICommandGroup IcmdBOMSetting;

            if (iBmp == null)
                iBmp = new BitmapHandler();
            Assembly thisAssembly;
            int saveAsm, createProject, releaseBOM;
            string Title = "BOM Planner", ToolTip = "BOM Planner";


            int[] docTypes = new int[]{(int)swDocumentTypes_e.swDocASSEMBLY,
                                       (int)swDocumentTypes_e.swDocDRAWING,
                                       (int)swDocumentTypes_e.swDocPART};

            thisAssembly = Assembly.GetAssembly(GetType());


            int cmdGroupErr = 0;
            bool ignorePrevious = false;

            object registryIDs;
            //get the ID information stored in the registry
            bool getDataResult = iCmdMgr.GetGroupDataFromRegistry(mainCmdBOMCreator, out registryIDs);

            int[] knownIDs = new int[4] { ItemIDSave, ItemIDCreateProject, ItemIDReleaseBOM, ItemIDExportImg };

            if (getDataResult)
            {
                if (!CompareIDs((int[])registryIDs, knownIDs)) //if the IDs don't match, reset the commandGroup
                {
                    ignorePrevious = true;
                }
            }

            IcmdBOMCreator = iCmdMgr.CreateCommandGroup2(mainCmdBOMCreator, Title, ToolTip, "", -1, ignorePrevious, ref cmdGroupErr);
            IcmdBOMCreator.LargeIconList = iBmp.CreateFileFromResourceBitmap("AddinSolidworks.MainIconLarge.bmp", thisAssembly);
            IcmdBOMCreator.SmallIconList = iBmp.CreateFileFromResourceBitmap("AddinSolidworks.MainIconSmall.bmp", thisAssembly);
            IcmdBOMCreator.LargeMainIcon = iBmp.CreateFileFromResourceBitmap("AddinSolidworks.MainIconLarge.bmp", thisAssembly);
            IcmdBOMCreator.SmallMainIcon = iBmp.CreateFileFromResourceBitmap("AddinSolidworks.MainIconSmall.bmp", thisAssembly);

            IcmdBOMSetting = iCmdMgr.CreateCommandGroup2(mainCmdBOMSetting, "Setting", "Setting", "", -1, ignorePrevious, ref cmdGroupErr);
            IcmdBOMSetting.LargeIconList = iBmp.CreateFileFromResourceBitmap("AddinSolidworks.MainIconLarge.bmp", thisAssembly);
            IcmdBOMSetting.SmallIconList = iBmp.CreateFileFromResourceBitmap("AddinSolidworks.MainIconSmall.bmp", thisAssembly);
            IcmdBOMSetting.LargeMainIcon = iBmp.CreateFileFromResourceBitmap("AddinSolidworks.MainIconLarge.bmp", thisAssembly);
            IcmdBOMSetting.SmallMainIcon = iBmp.CreateFileFromResourceBitmap("AddinSolidworks.MainIconSmall.bmp", thisAssembly);


            int menuToolbarOption = (int)(swCommandItemType_e.swMenuItem | swCommandItemType_e.swToolbarItem);
            saveAsm = IcmdBOMCreator.AddCommandItem2("Save Assembly Info", -1, "Save Assembly To Database",
              "Save Assembly To Database", 1, "SaveAssemblyInfo", "", ItemIDSave, menuToolbarOption);

            createProject = IcmdBOMCreator.AddCommandItem2("Create Project", -1, "Create New Project",
             "Create Project", 1, "CreateProject", "", ItemIDCreateProject, menuToolbarOption);

            releaseBOM = IcmdBOMCreator.AddCommandItem2("Release BOM", -1, "Release a BOM",
            "Release BOM", 1, "ReleaseBOM", "", ItemIDReleaseBOM, menuToolbarOption);

            var design = IcmdBOMCreator.AddCommandItem2("CreateDesign", -1, "Create a Design BOM",
             "Design BOM", 1, "CreateDesign", "", ItemIDDesign, menuToolbarOption);

            var partListbyType = IcmdBOMCreator.AddCommandItem2("CreateByPrtLst", -1, "Create a Part List by Type",
              "ItemGroup List by Type", 1, "CreatePartListbyType", "", ItemIDPartListByType, menuToolbarOption);

            var itemGroup = IcmdBOMCreator.AddCommandItem2("CreateItemGroups", -1, "Create a Add Item Groups",
             "Edit Item Groups", 1, "CreateItemGroups", "", ItemIDAddItems, menuToolbarOption);

             var dueDate = IcmdBOMCreator.AddCommandItem2("CreateSetDueDate", -1, "Create a Set Due Date",
            "Set Due Date", 1, "SetDueDate", "", ItemIDDueDate, menuToolbarOption);

            var missingPNum = IcmdBOMCreator.AddCommandItem2("CreateMissingPartNumber", -1, "Create a Missing Part Number",
           "Save Missing Part Numbers", 1, "MissingPartNumber", "", ItemIDMissingPNum, menuToolbarOption);

            var newUser = IcmdBOMCreator.AddCommandItem2("AddNewUser", -1, "Add a New User",
           "Add New User", 1, "AddNewUser", "", ItemIDnewUser, menuToolbarOption);

            IcmdBOMCreator.HasToolbar = true;
            IcmdBOMCreator.HasMenu = true;
            IcmdBOMCreator.Activate();


            int menuToolbarOption2 = (int)(swCommandItemType_e.swMenuItem | swCommandItemType_e.swToolbarItem);
            var exprtImages = IcmdBOMSetting.AddCommandItem2("BOMSetting", -1, "Export Images", "Export Images", 1,
            "ExportImages", "", ItemIDExportImg, menuToolbarOption2);

            IcmdBOMSetting.HasToolbar = true;
            IcmdBOMSetting.HasMenu = true;
            IcmdBOMSetting.Activate();

            bool bResult;


            //FlyoutGroup flyGroup = iCmdMgr.CreateFlyoutGroup(flyoutGroupID, "Dynamic Flyout", "Flyout Tooltip", "Flyout Hint",
            //  cmdGroup.SmallMainIcon, cmdGroup.LargeMainIcon, cmdGroup.SmallIconList, cmdGroup.LargeIconList, "FlyoutCallback", "FlyoutEnable");


            //flyGroup.AddCommandItem("FlyoutCommand 1", "test", 0, "FlyoutCommandItem1", "FlyoutEnableCommandItem1");

            //flyGroup.FlyoutType = (int)swCommandFlyoutStyle_e.swCommandFlyoutStyle_Simple;



            foreach (int type in docTypes)
            {
                CommandTab cmdTab;

                cmdTab = iCmdMgr.GetCommandTab(type, Title);

                if (cmdTab != null & !getDataResult | ignorePrevious)//if tab exists, but we have ignored the registry info (or changed command group ID), re-create the tab.  Otherwise the ids won't matchup and the tab will be blank
                {
                    bool res = iCmdMgr.RemoveCommandTab(cmdTab);
                    cmdTab = null;
                }

                //if cmdTab is null, must be first load (possibly after reset), add the commands to the tabs
                if (cmdTab == null)
                {
                    cmdTab = iCmdMgr.AddCommandTab(type, Title);

                    CommandTabBox cmdBox = cmdTab.AddCommandTabBox();

                    int[] cmdIDs = new int[12];
                    int[] TextType = new int[12];

                    //Cmd for Save Assembly Info
                    cmdIDs[0] = IcmdBOMCreator.get_CommandID(saveAsm);
                    TextType[0] = (int)swCommandTabButtonTextDisplay_e.swCommandTabButton_TextHorizontal;


                    if (string.IsNullOrWhiteSpace(userGrpName) || !userGrpName.Equals("Admin", StringComparison.InvariantCultureIgnoreCase))
                    {
                        cmdIDs[2] = IcmdBOMCreator.get_CommandID(createProject);
                        TextType[2] = (int)swCommandTabButtonTextDisplay_e.swCommandTabButton_TextHorizontal;

                        cmdIDs[3] = IcmdBOMCreator.get_CommandID(releaseBOM);
                        TextType[3] = (int)swCommandTabButtonTextDisplay_e.swCommandTabButton_TextHorizontal;
                        //Cmd For BOM PartListByType

                        cmdIDs[4] = IcmdBOMCreator.get_CommandID(design);
                        TextType[4] = (int)swCommandTabButtonTextDisplay_e.swCommandTabButton_TextHorizontal;

                        //Cmd For BOM PartListByType
                        cmdIDs[5] = IcmdBOMCreator.get_CommandID(partListbyType);
                        TextType[5] = (int)swCommandTabButtonTextDisplay_e.swCommandTabButton_TextHorizontal;


                        cmdIDs[6] = IcmdBOMCreator.get_CommandID(itemGroup);
                        TextType[6] = (int)swCommandTabButtonTextDisplay_e.swCommandTabButton_TextHorizontal;

                        cmdIDs[7] = IcmdBOMCreator.get_CommandID(missingPNum);
                        TextType[7] = (int)swCommandTabButtonTextDisplay_e.swCommandTabButton_TextHorizontal;
                        
                        cmdIDs[8] = IcmdBOMCreator.get_CommandID(newUser);
                        TextType[8] = (int)swCommandTabButtonTextDisplay_e.swCommandTabButton_TextHorizontal; 

                        cmdIDs[9] = IcmdBOMSetting.get_CommandID(exprtImages);
                        TextType[9] = (int)swCommandTabButtonTextDisplay_e.swCommandTabButton_TextHorizontal;

                        cmdIDs[10] = IcmdBOMSetting.ToolbarId;
                        TextType[10] = (int)swCommandTabButtonTextDisplay_e.swCommandTabButton_TextHorizontal;
                    }


                    else
                    {
                        cmdIDs[1] = IcmdBOMCreator.get_CommandID(dueDate);
                        TextType[1] = (int)swCommandTabButtonTextDisplay_e.swCommandTabButton_TextHorizontal;

                        cmdIDs[2] = IcmdBOMCreator.get_CommandID(createProject);
                        TextType[2] = (int)swCommandTabButtonTextDisplay_e.swCommandTabButton_TextHorizontal;

                        cmdIDs[3] = IcmdBOMCreator.get_CommandID(releaseBOM);
                        TextType[3] = (int)swCommandTabButtonTextDisplay_e.swCommandTabButton_TextHorizontal;
                        //Cmd For BOM PartListByType

                        cmdIDs[4] = IcmdBOMCreator.get_CommandID(design);
                        TextType[4] = (int)swCommandTabButtonTextDisplay_e.swCommandTabButton_TextHorizontal;

                        //Cmd For BOM PartListByType
                        cmdIDs[5] = IcmdBOMCreator.get_CommandID(partListbyType);
                        TextType[5] = (int)swCommandTabButtonTextDisplay_e.swCommandTabButton_TextHorizontal;


                        cmdIDs[6] = IcmdBOMCreator.get_CommandID(itemGroup);
                        TextType[6] = (int)swCommandTabButtonTextDisplay_e.swCommandTabButton_TextHorizontal;

                        cmdIDs[7] = IcmdBOMCreator.get_CommandID(missingPNum);
                        TextType[7] = (int)swCommandTabButtonTextDisplay_e.swCommandTabButton_TextHorizontal;

                        cmdIDs[8] = IcmdBOMCreator.get_CommandID(newUser);
                        TextType[8] = (int)swCommandTabButtonTextDisplay_e.swCommandTabButton_TextHorizontal;

                        cmdIDs[9] = IcmdBOMSetting.get_CommandID(exprtImages);
                        TextType[9] = (int)swCommandTabButtonTextDisplay_e.swCommandTabButton_TextHorizontal;

                        cmdIDs[10] = IcmdBOMSetting.ToolbarId;
                        TextType[10] = (int)swCommandTabButtonTextDisplay_e.swCommandTabButton_TextHorizontal;
                    }
                


                    bResult = cmdBox.AddCommands(cmdIDs, TextType);

                    //CommandTabBox cmdBox1 = cmdTab.AddCommandTabBox();
                    //cmdIDs = new int[1];
                    //TextType = new int[1];

                    //cmdIDs[0] = flyGroup.CmdID;
                    //TextType[0] = (int)swCommandTabButtonTextDisplay_e.swCommandTabButton_TextBelow | (int)swCommandTabButtonFlyoutStyle_e.swCommandTabButton_ActionFlyout;

                    //bResult = cmdBox1.AddCommands(cmdIDs, TextType);

                    //cmdTab.AddSeparator(cmdBox1, cmdIDs[0]);

                }

            }
            thisAssembly = null;

        }


        public void RemoveCommandMgr()
        {
            iBmp.Dispose();

            iCmdMgr.RemoveCommandGroup(mainCmdBOMCreator);
            iCmdMgr.RemoveFlyoutGroup(flyoutGroupID);
        }

        public bool CompareIDs(int[] storedIDs, int[] addinIDs)
        {
            List<int> storedList = new List<int>(storedIDs);
            List<int> addinList = new List<int>(addinIDs);

            addinList.Sort();
            storedList.Sort();

            if (addinList.Count != storedList.Count)
            {
                return false;
            }
            else
            {

                for (int i = 0; i < addinList.Count; i++)
                {
                    if (addinList[i] != storedList[i])
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        //public Boolean AddPMP()
        //{
        //  ppage = new UserPMPage(this);
        //  return true;
        //}

        //public Boolean RemovePMP()
        //{
        //  ppage = null;
        //  return true;
        //}

        #endregion


        #region openDoc Working 


        public void DocumentDetails(out AssemblyInfo parentInfo, out string path)
        {
            PropertyReader.ReadProperties();
            parentInfo = new AssemblyInfo();
            var reader = new SldWrksReader();
            var partDocument = (ModelDoc2)iSwApp.ActiveDoc as PartDoc;
            var assDocument = (ModelDoc2)iSwApp.ActiveDoc as AssemblyDoc;
            var swModel = partDocument == null ? (ModelDoc2)assDocument : (ModelDoc2)partDocument;
            if (partDocument != null) { var assodel = (PartDoc)partDocument; }
            else { var assodel = (AssemblyDoc)assDocument; }
            var swConfMgr = (ConfigurationManager)swModel.ConfigurationManager;
            var swConf = (Configuration)swConfMgr.ActiveConfiguration;
            var swRootComp = (Component2)swConf.GetRootComponent3(true);
            var swModelDocExt = swModel.Extension;
            var swCustPropMgr = swModelDocExt.CustomPropertyManager[""];
            path = swRootComp.GetPathName();
            parentInfo = reader.GetCustomProperties(swCustPropMgr, path, swConf);
            var progressBar = new Progress();
            progressBar.Show();
            System.Progress<int> progress = null;
            if (PropertyReader.bar != null)
            {
                progress = new Progress<int>(barValue =>
                {
                    // This lambda is executed in context of UI thread,
                    // so it can safely update form controls
                    PropertyReader.bar.Value = barValue;
                });
            }

            if (partDocument != null)
            {
                progressBar.Close();
                return;
            }

            reader.ReadAssembly(swRootComp, parentInfo, 1);
            //string message = "Do You Want To Upload Complete Tree For Assembly";
            //string title = "Add Assembly";
            //MessageBoxButtons buttons = MessageBoxButtons.YesNo;
            //DialogResult result = MessageBox.Show(message, title, buttons);
            //if (result == DialogResult.Yes)
            //{
            //    reader.ReadAssembly(swRootComp, parentInfo, 1);
            //}

            progressBar.Close();
        }


        //public void DocumentDetails(out AssemblyInfo parentInfo, out string path)
        //{
        //    PropertyReader.ReadProperties();
        //    parentInfo = new AssemblyInfo();
        //    var reader = new SldWrksReader();
        //    var openDoc = (ModelDoc2)iSwApp.ActiveDoc;
        //    var swModel = (ModelDoc2)openDoc;
        //    var assodel = (AssemblyDoc)openDoc;
        //    var swConfMgr = (ConfigurationManager)swModel.ConfigurationManager;
        //    var swConf = (Configuration)swConfMgr.ActiveConfiguration;
        //    var swRootComp = (Component2)swConf.GetRootComponent3(true);
        //    var swModelDocExt = swModel.Extension;
        //    var swCustPropMgr = swModelDocExt.CustomPropertyManager[""];
        //    path = swRootComp.GetPathName();
        //    parentInfo = reader.GetCustomProperties(swCustPropMgr, path);
        //    var progressBar = new Progress();
        //    progressBar.Show();
        //    System.Progress<int> progress = null;
        //    if (PropertyReader.bar != null)
        //    {
        //        progress = new Progress<int>(barValue =>
        //        {
        //            // This lambda is executed in context of UI thread,
        //            // so it can safely update form controls
        //            PropertyReader.bar.Value = barValue;
        //        });
        //    }
        //    reader.ReadAssembly(swRootComp, parentInfo, 1);
        //    progressBar.Close();
        //}

        #endregion



        #region UI Callbacks


        /// <summary>
        /// function for open login page
        /// </summary>
        public void UserLogin()
        {
            UserLoginView userLogin = new UserLoginView();
            userLogin.DataContext = userLoginViewModel;
            userLogin.ShowDialog();
            userCheck = SQLQueries.UserInfo();
            if (userCheck != null)
            {
                userGrpName = SQLQueries.getUserGrpId(userCheck);
                MessageBox.Show("User Logged In Successfully");
            }
        }


        /// <summary>
        /// CallBack Function for Set Due Date
        /// </summary>
        public void SetDueDate()
        {
            if (userCheck == null)
            {
                UserLogin();
            }

            else
            {
                DocumentDetails(out AssemblyInfo parentInfo, out string path);
                DueDate dueDate = new DueDate();
                dueDate.SaveDueDate(parentInfo);
            }
        }


        /// <summary>
        /// CallBack Function for Design
        /// </summary>
        public void CreateDesign()
        {
            if (userCheck == null)
            {
                UserLogin();
            }

            else
            {
                DocumentDetails(out AssemblyInfo parentInfo, out string path);

                var bomProcessor = BOMProcessorFactory.Create(parentInfo, BOMType.General, FileType.asm, "");
                bomProcessor.Process(ReportType.Genral);
            }
        }


        /// <summary>
        /// CallBack Function for Design
        /// </summary>
        public void CreateProject()
        {
           
        }

        /// <summary>
        /// 
        /// </summary>
        public void AddNewUser()
        {
            AddNewUser addNewUser = new AddNewUser();
            NewUserViewModel newUserViewModel = new NewUserViewModel();
            addNewUser.DataContext = newUserViewModel;
            addNewUser.ShowDialog();
        }


        /// <summary>
        /// 
        /// </summary>
        public void ReleaseBOM()
        {
            if (userCheck == null)
            {
                UserLogin();
            }

            else
            {
                DocumentDetails(out AssemblyInfo parentInfo, out string path);


                var bomProcessor = BOMProcessorFactory.Create(parentInfo,
                                                              BOMType.Release,
                                                              FileType.asm,
                                                              "");


                SaveXLFile.QREvent.QRGenerate += QREventHandler;
                SaveXLFile.PdfEvent.PdfGenerate += PdfEventHandler;

                bomProcessor.Process(ReportType.Release);


            }
        }



    /// <summary>
    /// Event Handler for QR Generator
    /// </summary>
    /// <param name="assemblyInfo"></param>
    /// <param name="path"></param>
    public void QREventHandler(AssemblyInfo parentInfo, string path)
    {
      QRGenerate generate = new QRGenerate();
      
      generate.Traverser(path, parentInfo, SwApp);
    }


        /// <summary>
        /// Event Handler for Pdf Generator
        /// </summary>
        /// <param name="assemblyInfo"></param>
        /// <param name="path"></param>
        public void PdfEventHandler(AssemblyInfo parentInfo, string path)
        {
            PdfGenerate pdfGenerate = new PdfGenerate();
            pdfGenerate.GetDirectory(parentInfo, SwApp);
        }


        /// <summary>
        /// CallBack Function for Design
        /// </summary>
        public void SaveAssemblyInfo()
        {
            if (userCheck == null)
            {
                UserLogin();
            }

            else
            {
                DocumentDetails(out AssemblyInfo parentInfo, out string path);
                CustomPropertiesView customPropertiesView = new CustomPropertiesView();
                CustomPropertiesViewModel customPropertiesViewModel = new CustomPropertiesViewModel(parentInfo, userCheck);
                customPropertiesView.DataContext = customPropertiesViewModel;
                customPropertiesView.ShowDialog();
            }
        }


        /// <summary>
        /// CallBack Function for Design
        /// </summary>
        public void MissingPartNumber()
        {
            if (userCheck == null)
            {
                UserLogin();
            }

            else
            {
                DocumentDetails(out AssemblyInfo parentInfo, out string path);
                FillPartNum fillPartNum = new FillPartNum();
                CustomPropertiesViewModel customPropertiesViewModel = new CustomPropertiesViewModel(parentInfo, userCheck);
                fillPartNum.DataContext = customPropertiesViewModel;
                fillPartNum.ShowDialog();
            }
        }



        /// <summary>
        /// CallBack function for images Export 
        /// </summary>
        public void ExportImages()
        {
            if (userCheck == null)
            {
                UserLogin();
            }

            else
            {
                var frm = new ExportImagesView();
                ExportImagesViewModel exportImagesViewModel = new ExportImagesViewModel();
                frm.DataContext = exportImagesViewModel;
                frm.ShowDialog();
            }
        }



        /// <summary>
        /// CallBack Function for AddItemGroups
        /// </summary>
        public void CreateItemGroups()
        {
            if (userCheck == null)
            {
                UserLogin();
            }

            else
            {
                var dlg = new AddItemsView();
                AddItemsViewModel addItemsViewModel = new AddItemsViewModel();
                dlg.DataContext = addItemsViewModel;
                dlg.ShowDialog();
            }
        }


        /// <summary>
        /// CallBack Function for PartListbyType
        /// </summary>
        public void CreatePartListbyType()
        {
            if (userCheck == null)
            {
                UserLogin();
            }

            else
            {
                DocumentDetails(out AssemblyInfo parentInfo, out string path);
                var dlg = new PartListByTypeView();
                PartListByTypeViewModel partListByTypeViewModel = new PartListByTypeViewModel();
                dlg.DataContext = partListByTypeViewModel;
                dlg.ShowDialog();
                partListByTypeViewModel.BOMPartListByType(parentInfo, path);
            }
        }



        /// <summary>
        /// 
        /// </summary>
        public void ShowPMP()
        {
            //if (ppage != null)
            //  ppage.Show();
        }

        public int EnablePMP()
        {
            if (iSwApp.ActiveDoc != null)
                return 1;
            else
                return 0;
        }


        public void FlyoutCallback()
        {
            FlyoutGroup flyGroup = iCmdMgr.GetFlyoutGroup(flyoutGroupID);
            flyGroup.RemoveAllCommandItems();

            flyGroup.AddCommandItem(DateTime.Now.ToLongTimeString(), "test", 0, "FlyoutCommandItem1", "FlyoutEnableCommandItem1");

        }


        public int FlyoutEnable()
        {
            return 1;
        }


        public void FlyoutCommandItem1()
        {
            iSwApp.SendMsgToUser("Flyout command 1");
        }


        public int FlyoutEnableCommandItem1()
        {
            return 1;
        }
        #endregion



        #region Event Methods
        public bool AttachEventHandlers()
        {
            AttachSwEvents();
            //Listen for events on all currently open docs
            AttachEventsToAllDocuments();
            return true;
        }

        private bool AttachSwEvents()
        {
            try
            {
                SwEventPtr.ActiveDocChangeNotify += new DSldWorksEvents_ActiveDocChangeNotifyEventHandler(OnDocChange);
                SwEventPtr.DocumentLoadNotify2 += new DSldWorksEvents_DocumentLoadNotify2EventHandler(OnDocLoad);
                SwEventPtr.FileNewNotify2 += new DSldWorksEvents_FileNewNotify2EventHandler(OnFileNew);
                SwEventPtr.ActiveModelDocChangeNotify += new DSldWorksEvents_ActiveModelDocChangeNotifyEventHandler(OnModelChange);
                SwEventPtr.FileOpenPostNotify += new DSldWorksEvents_FileOpenPostNotifyEventHandler(FileOpenPostNotify);
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return false;
            }
        }



        private bool DetachSwEvents()
        {
            try
            {
                SwEventPtr.ActiveDocChangeNotify -= new DSldWorksEvents_ActiveDocChangeNotifyEventHandler(OnDocChange);
                SwEventPtr.DocumentLoadNotify2 -= new DSldWorksEvents_DocumentLoadNotify2EventHandler(OnDocLoad);
                SwEventPtr.FileNewNotify2 -= new DSldWorksEvents_FileNewNotify2EventHandler(OnFileNew);
                SwEventPtr.ActiveModelDocChangeNotify -= new DSldWorksEvents_ActiveModelDocChangeNotifyEventHandler(OnModelChange);
                SwEventPtr.FileOpenPostNotify -= new DSldWorksEvents_FileOpenPostNotifyEventHandler(FileOpenPostNotify);
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return false;
            }

        }

        public void AttachEventsToAllDocuments()
        {
            ModelDoc2 modDoc = (ModelDoc2)iSwApp.GetFirstDocument();
            while (modDoc != null)
            {
                if (!openDocs.Contains(modDoc))
                {
                    AttachModelDocEventHandler(modDoc);
                }
                else if (openDocs.Contains(modDoc))
                {
                    bool connected = false;
                    DocumentEventHandler docHandler = (DocumentEventHandler)openDocs[modDoc];
                    if (docHandler != null)
                    {
                        connected = docHandler.ConnectModelViews();
                    }
                }

                modDoc = (ModelDoc2)modDoc.GetNext();
            }
        }

        public bool AttachModelDocEventHandler(ModelDoc2 modDoc)
        {
            if (modDoc == null)
                return false;

            DocumentEventHandler docHandler = null;

            if (!openDocs.Contains(modDoc))
            {
                switch (modDoc.GetType())
                {
                    case (int)swDocumentTypes_e.swDocPART:
                        {
                            docHandler = new PartEventHandler(modDoc, this);
                            break;
                        }
                    case (int)swDocumentTypes_e.swDocASSEMBLY:
                        {
                            docHandler = new AssemblyEventHandler(modDoc, this);
                            break;
                        }
                    case (int)swDocumentTypes_e.swDocDRAWING:
                        {
                            docHandler = new DrawingEventHandler(modDoc, this);
                            break;
                        }
                    default:
                        {
                            return false; //Unsupported document type
                        }
                }
                docHandler.AttachEventHandlers();
                openDocs.Add(modDoc, docHandler);
            }
            return true;
        }

        public bool DetachModelEventHandler(ModelDoc2 modDoc)
        {
            DocumentEventHandler docHandler;
            docHandler = (DocumentEventHandler)openDocs[modDoc];
            openDocs.Remove(modDoc);
            modDoc = null;
            docHandler = null;
            return true;
        }

        public bool DetachEventHandlers()
        {
            DetachSwEvents();

            //Close events on all currently open docs
            DocumentEventHandler docHandler;
            int numKeys = openDocs.Count;
            object[] keys = new object[numKeys];

            //Remove all document event handlers
            openDocs.Keys.CopyTo(keys, 0);
            foreach (ModelDoc2 key in keys)
            {
                docHandler = (DocumentEventHandler)openDocs[key];
                docHandler.DetachEventHandlers(); //This also removes the pair from the hash
                docHandler = null;
            }
            return true;
        }
        #endregion

        #region Event Handlers
        //Events



        public int OnDocChange()
        {
            return 0;
        }

        public int OnDocLoad(string docTitle, string docPath)
        {
            return 0;
        }

        int FileOpenPostNotify(string FileName)
        {
            AttachEventsToAllDocuments();
            return 0;
        }

        public int OnFileNew(object newDoc, int docType, string templateName)
        {
            AttachEventsToAllDocuments();
            return 0;
        }

        public int OnModelChange()
        {
            return 0;
        }

        #endregion
    }

}
