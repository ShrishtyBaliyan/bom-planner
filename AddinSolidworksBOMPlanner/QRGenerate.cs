﻿using CadData;
using NPOI.HPSF;
using QRCoder;
using SolidWorks.Interop.sldworks;
using SolidWorks.Interop.swconst;
using System;
using System.Activities.Expressions;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace AddinSolidworksBOMPlanner
{
    public class QRGenerate
    {
        double height;
        double width;
        private string X;
        private string Y;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="str"></param>
        /// <param name="imgPath"></param>
        private void CreateQRCode(string str, string imgPath)
        {
            using (QRCodeGenerator qrGenerator = new QRCodeGenerator())
            {
                using (QRCodeData qrCodeData = qrGenerator.CreateQrCode(str, QRCodeGenerator.ECCLevel.Q))
                {
                    using (QRCode qrCode = new QRCode(qrCodeData))
                    {
                        var bb = new Bitmap(20, 20);
                        bb.Save(imgPath);
                        var bitmap = qrCode.GetGraphic(20, Color.Black, Color.White,
                          bb, 2);
                        bitmap.Save(imgPath,System.Drawing.Imaging.ImageFormat.Jpeg);
                    }
                }
            }
        }


  


        /// <summary>
        /// 
        /// </summary>
        /// <param name="path"></param>
        public void GenerateQRCode(AssemblyInfo baseInfo, string path, ISldWorks swApp)
        {
            var path1 = Path.GetDirectoryName(path);
            var dInfo = new DirectoryInfo(path1);

            var fileName = Path.GetFileNameWithoutExtension(path);
            var partPath = Path.GetDirectoryName(path);
            var type = (int)swDocumentTypes_e.swDocPART;

            var partFile = Path.Combine(partPath, fileName + ".sldprt");
            if (!File.Exists(partFile))
            {
                type = (int)swDocumentTypes_e.swDocASSEMBLY;
                partFile = Path.Combine(partPath, fileName + ".sldasm");
                if (!File.Exists(partFile))
                {
                    return; ;
                }
            }


            //SolidWorks.Interop.sldworks.SldWorks swApp = new SldWorks();
            //swApp.Visible = true;
            int longstatus = 0;
            int longwarnings = 0;
            var partDwg = swApp.OpenDoc6(partFile, type, 0, "", ref longstatus, ref longwarnings);

            var swModel = (ModelDoc2)partDwg;
            //var assodel = (AssemblyDoc)openDoc;
            var swCustPropMgr = swModel.Extension.CustomPropertyManager[""];


            //  var swConf = (Configuration)swConfMgr.ActiveConfiguration;
            //   var swCustPropMgr = swConf.CustomPropertyManager;

            SldWrksReader sldWrksReader = new SldWrksReader();
            var partInfo = sldWrksReader.GetCustomProperties(swCustPropMgr, partFile);

            var qrString = partInfo.PartNum + " - " + partInfo.ItemName + " - " + partInfo.Material;

            var imagePath = Path.Combine(Path.GetTempPath(), "img.jpeg");
            CreateQRCode(qrString, imagePath);
            //partDwg.Close();
            var name = Path.GetFileNameWithoutExtension(path);
            var drwingFile = Path.Combine(path1, name + ".SLDDRW");
            if (!File.Exists(drwingFile))
            {
        swApp.CloseDoc(path);
        return;
            }
            partDwg = swApp.OpenDoc6(drwingFile, (int)swDocumentTypes_e.swDocDRAWING, 0, "", ref longstatus, ref longwarnings);

            var swDraw = partDwg as DrawingDoc;

            swModel.ClearSelection2(true);
            var vSheetNames = (string[])swDraw.GetSheetNames();
            foreach (var vSheetName in vSheetNames)
            {
                var kk = swDraw.ActivateSheet(vSheetName);
                var swSheet = (Sheet)swDraw.GetCurrentSheet();
               // swSheet.SetScale(100, 100, true, true);
                var prop = swSheet.GetProperties2();

                

                X = (6 * (prop[3] / (100 * 10))).ToString();
                Y = (23 *(prop[3] / (100 * 10))).ToString();

                var H = (25 * (prop[3] / (100 * 10)));
                var W = (25 * (prop[3] / (100 * 10)));

                var sketchManager = partDwg.SketchManager;
                var location = new System.Windows.Point(double.Parse(X), double.Parse(Y));
                var sketchPicture = sketchManager.InsertSketchPicture(imagePath);
                var res = sketchPicture.SetOrigin(location.X, location.Y);
                var res1 = sketchPicture.SetSize(H, W, false);
                swSheet = null;
            }

            int err = 0;
            int war = 0;
            var ff = partDwg.Save3((int)swSaveAsOptions_e.swSaveAsOptions_Silent, ref err, ref war); ;
            //var resSave = partDwg.Save2(true);
            //partName.Save2(true);
            swApp.CloseDoc(drwingFile);
            swApp.CloseDoc(partFile);

            partDwg = null;
            swDraw = null;
            swModel = null;
        }
        


        /// <summary>
        /// 
        /// </summary>
        /// <param name="path"></param>
        /// <param name="baseInfo"></param>
        /// <param name="assmPath"></param>
        public void Traverser(string path, AssemblyInfo baseInfo, ISldWorks swApp)
        {
            if (baseInfo.ItemGroup.Equals("Hardware", StringComparison.CurrentCultureIgnoreCase) || baseInfo.ItemGroup.Equals("Bop", StringComparison.CurrentCultureIgnoreCase))
            {
                return;
            }

            GenerateQRCode(baseInfo, path, swApp);

            foreach (var childInfo in baseInfo.childs)
            {
                if (childInfo.childs.Count == 0)
                {
                    if (childInfo.ItemGroup.Equals("Hardware", StringComparison.CurrentCultureIgnoreCase) || childInfo.ItemGroup.Equals("Bop", StringComparison.CurrentCultureIgnoreCase))
                    {
                        continue;
                    }
                    GenerateQRCode(childInfo, childInfo.path, swApp);
                }
                else if (childInfo.childs.Count > 0)
                {
                    Traverser(childInfo.path, childInfo, swApp);
                }
            }
        }
    }
}
