﻿using System;
using CadData;
using SolidWorks.Interop.sldworks;
using System.Linq;
using SolidWorks.Interop.swconst;
using System.IO;
using CadData.Helpers;
using System.Windows;

namespace AddinSolidworksBOMPlanner
{
    /// <summary>
    /// 
    /// </summary>
    public class SldWrksReader : ICADDataReader
    {
        private SldWorks swApp;


        /// <summary>
        /// Function for Getting Information of Assemblies
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public AssemblyInfo GetCustomProperties(dynamic propertyMngr)
        {
            var swCustPropMgr = propertyMngr as CustomPropertyManager;
            var baseInfo = new AssemblyInfo();
            var vPropNames = (object[])swCustPropMgr.GetNames();
            if (vPropNames == null)
            {
                return baseInfo;
            }


            for (var iterator = 0; iterator <= vPropNames.Count() - 1; iterator++)
            {
                swCustPropMgr.Get2(vPropNames[iterator].ToString(), out var valOut, out var resolvedValOut);
                var pName = vPropNames[iterator] as string;

                foreach (var att in baseInfo.properties.GetAttributes())
                {
                    if (pName.Equals(att.Name,

                       StringComparison.InvariantCultureIgnoreCase))
                    {
                        att.StrValue = resolvedValOut;
                    }
                }

            }

            //if (IsTypeBoutOut(baseInfo))
            //{
            //    var mat = baseInfo.properties["MATERIAL"];
            //    //mat.strValue = "NA";
            //    baseInfo.properties["MATERIAL"] = mat;
            //}

            //if (string.IsNullOrEmpty(baseInfo.PartNum))
            //{
            //    var prp1 = baseInfo.properties["PART NUMBER"];
            //    prp1.StrValue = "-";
            //    baseInfo.properties["PART NUMBER"] = prp1;
            //}
            return baseInfo;
        }


        /// <summary>
        /// Overload Function for Getting Information of Assemblies
        /// </summary>
        /// <param name="propertyMngr"></param>
        /// <param name="path"></param>
        /// <param name="config"></param>
        /// <param name="app"></param>
        /// <returns></returns>
        public AssemblyInfo GetCustomProperties(dynamic propertyMngr, string path, Configuration config = null, SldWorks app = null)
        {
            var baseInfo = new AssemblyInfo();

            swApp = new SldWorks
            {
                Visible = true
            };
            baseInfo.path = path;
            var swCustPropMgr = propertyMngr as CustomPropertyManager;
            var vPropNames = (object[])swCustPropMgr.GetNames();
            if (vPropNames == null)
            {
                return baseInfo;
            }

            if (app != null)
            {
                try

                {
                    if (baseInfo.properties["IMAGE"] != null)
                    {
                        baseInfo.properties["IMAGE"].Img = PictureDispConverter.Convert(app.GetPreviewBitmap(path, (string)config.Name));
                    }
                   
                }
                catch (Exception e)
                {

                }

            }


            for (var iterator = 0; iterator <= vPropNames.Count() - 1; iterator++)
            {
                swCustPropMgr.Get2(vPropNames[iterator].ToString(), out var valOut, out var resolvedValOut);
                var pName = vPropNames[iterator] as string;

                foreach (var att in baseInfo.properties.GetAttributes())
                {
                    if (pName.Equals(att.Name,
                                     StringComparison.InvariantCultureIgnoreCase))
                    {
                        if (att.IsDesign)
                        {
                            if (double.TryParse(resolvedValOut, out var dVal))
                            {
                                att.DValue = dVal;
                            }
                            
                        }
                        if (att.DisplayName.Equals("Unit Weight(kg)", StringComparison.InvariantCultureIgnoreCase))
                        {
                            double numericValue;
                            bool isNumber = double.TryParse(resolvedValOut, out numericValue);
                            if (!isNumber)
                            {
                                MessageBox.Show("Custom Property(Weight) is invalid in Part Number : " + Path.GetFileNameWithoutExtension(path));
                                break;
                            }
                        }
                        att.StrValue = resolvedValOut;
                        
                    }
                }


                //foreach (var stocks in baseInfo.StockTypes)
                //{
                //    foreach (var st in stocks.stockAttributes)
                //    {

                //        if (pName.Equals(st.Name,
                //          StringComparison.InvariantCultureIgnoreCase))
                //        {
                //            if (double.TryParse(resolvedValOut, out var dVal))
                //            {
                //                st.DValue = dVal;
                //            }
                //        }
                //    }
                //}

                //for (var index = 0; index < baseInfo.StockTypes.Count; index++)
                //{
                //    var sType = baseInfo.StockTypes[index];
                //    sType["rawMaterialqty"].DValue = DataReader.DataReader.GetQuantity(sType.stockType, sType.stockAttributes[0].DValue, sType.stockAttributes[1].DValue,
                //                           sType.stockAttributes[2].DValue, sType.stockAttributes[3].DValue, sType.stockAttributes[4].DValue, out var rawMaterialdesc);
                //    sType["rawMaterialdesc"].StrValue = rawMaterialdesc;
                //}
            }

            //if (IsTypeBoutOut(baseInfo))
            //{
            //    var mat = baseInfo.properties["MATERIAL"];
            //    //mat.strValue = "NA";
            //    baseInfo.properties["MATERIAL"] = mat;
            //}

            //if (string.IsNullOrEmpty(baseInfo.PartNum))
            //{
            //    var prp1 = baseInfo.properties["PART NUMBER"];
            //    prp1.StrValue = "-";
            //    baseInfo.properties["PART NUMBER"] = prp1;
            //}
            return baseInfo;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        //private static bool IsTypeBoutOut(AssemblyInfo info)
        //{
        //    if (PropertyReader.boughtOuts.Any(b => b.Equals(info.Type, StringComparison.InvariantCultureIgnoreCase)))
        //    {
        //        return true;
        //    }
        //    return false;
        //}



        SwAddin addin = new SwAddin();

        /// <summary>
        /// Function For getting information of childs of Assemblies
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public void ReadAssembly(dynamic CADObj,
                                  AssemblyInfo parentInfo,
                                  int level)
        {
           
            object[] vChildComp;
            Component2 parentComp = CADObj as Component2;
            Component2 child = null;
            var step = 1.0;
            var pr = 0;
            
            CADObj.SetSuppression2((int)swComponentSuppressionState_e.swComponentResolved);
            vChildComp = (object[])CADObj.GetChildren();

            if (level == 1)
            {
                step = 100.0 / vChildComp.Length;
            }

            for (int index = 0; index < vChildComp.Length; index++)
            {
                pr += (int)step;
                if (level == 1)
                {
                    //var pData = new ProgressData  
                    //{
                    //  step = pr,
                    //  text = "Reading Assembly..."

                    //};


                    PropertyReader.bar.Value=(pr);
                }
                

                child = (Component2)vChildComp[index];

                if (child == null)
                {
                    continue;
                }
                child.SetSuppression2((int)(Int64)swComponentSuppressionState_e.swComponentResolved);

                var path1 = child.GetPathName();
                var extension1 = Path.GetExtension(path1);

                var childModel = child.GetModelDoc2() as ModelDoc2;
                if (childModel == null)
                {
                    continue;
                }
                var swConfMgr = (ConfigurationManager)childModel.ConfigurationManager;

                var swConf = (Configuration)swConfMgr.ActiveConfiguration;
                var childExt = childModel.Extension;
                var childInfo = GetCustomProperties(childExt.CustomPropertyManager[""], path1, swConf, swApp);
                childInfo.id = child.GetID();
                childInfo.path = path1;


                if (extension1.ToUpper() == ".SLDASM")
                {
                    childInfo.isAsm = true;
                    var topAsm = parentInfo.topLevelAsm.Count;
                    topAsm = topAsm + 1;
                } // if 
                else if (extension1.ToUpper() == ".SLDPRT")
                {
                    childInfo.isAsm = false;
                }
                else
                {
                    continue;
                }
               

                //if (!IsTypeBoutOut(childInfo) && !childInfo.isAsm && string.IsNullOrEmpty(childInfo.Material))
                //{
                //    var mat = childInfo.properties["MATERIAL"];
                //    mat.StrValue = "Oops";
                //    childInfo.properties["MATERIAL"] = mat;
                //}


                var existingInfo = parentInfo.childs.FirstOrDefault(info => info.PartNum.Equals(childInfo.PartNum));
               

                if (childInfo.PartNum.Count() < 1)
                {
                    continue;
                }

                if (existingInfo == null || childInfo.PartNum == "-")
                {
                    parentInfo.childs.Add(childInfo);
                    
                        ReadAssembly(child, childInfo, level + 1);
                    
                }
                else
                {
                    existingInfo.qty += 1;
                    
                        ReadAssembly(child, childInfo, level + 1);
                    
                }
            }

            if (level == 1)
            {
                PropertyReader.bar.Value = 100;
            }
            parentInfo.childs.Sort(new AlphanumComparatorFast());
            parentInfo.topLevelAsm.Sort(new AlphanumComparatorFast());
        }

    }

    /// <summary>
    /// 
    /// </summary>
    public class PictureDispConverter : System.Windows.Forms.AxHost
    {

        public PictureDispConverter()
          : base("56174C86-1546-4778-8EE6-B6AC606875E7")
        {
        }

        public static System.Drawing.Image Convert(object objIDispImage)
        {
            System.Drawing.Image objPicture = default(System.Drawing.Image);
            objPicture = (System.Drawing.Image)System.Windows.Forms.AxHost.GetPictureFromIPicture(objIDispImage);
            //objPicture.Save(@"D:\\trypic.bmp");
            return objPicture;
        }

    }
}
