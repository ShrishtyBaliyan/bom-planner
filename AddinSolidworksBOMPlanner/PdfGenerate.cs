﻿using CadData;
using Microsoft.Win32;
using SolidWorks.Interop.sldworks;
using SolidWorks.Interop.swconst;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Forms;

namespace AddinSolidworksBOMPlanner
{
    public class PdfGenerate
    {

        /// <summary>
        /// 
        /// </summary>
        public void pdf(AssemblyInfo baseInfo, string path, string assemblyPath, ISldWorks swApp)
        {
            ExportPdfData swExportPDFData = null;
            ModelDoc2 swModel = null;
            ModelDocExtension swModExt = null;
            DrawingDoc swDrawDoc = null;
            Sheet swSheet = null;

            bool boolstatus = false;
            int errors = 0;
            int warnings = 0;
            string[] obj = null;

            DispatchWrapper[] dispWrapArr = null;

            var name = Path.GetFileNameWithoutExtension(baseInfo.path);
            var drwingFile = Path.Combine(Path.GetDirectoryName(assemblyPath), name + ".SLDDRW");
            if (!File.Exists(drwingFile))
            {
                return;
            }

            var pdfName = Path.Combine(path, name + ".pdf");

            int longstatus = 0;
            int longwarnings = 0;

            swModel = swApp.OpenDoc6(drwingFile, (int)swDocumentTypes_e.swDocDRAWING, 0, "", ref longstatus, ref longwarnings);
            swModExt = (ModelDocExtension)swModel.Extension;
            swExportPDFData = (ExportPdfData)swApp.GetExportFileData((int)swExportDataFileType_e.swExportPdfData);

            // Get the names of the drawing sheets in the drawing
            // to get the size of the array of drawing sheets
            swDrawDoc = (DrawingDoc)swModel;

            obj = (string[])swDrawDoc.GetSheetNames();
            int count = 0;
            count = obj.Length;
            int i = 0;
            object[] objs = new object[count];

            // Activate each drawing sheet, except the last drawing sheet, for
            // demonstration purposes only and add each sheet to an array
            // of drawing sheets

            for (i = 0; i < count; i++)
            {
                boolstatus = swDrawDoc.ActivateSheet((obj[i]));
                swSheet = (Sheet)swDrawDoc.GetCurrentSheet();
                objs[i] = swSheet;
            }

            // Convert the .NET array of drawing sheets objects to IDispatch
            dispWrapArr = (DispatchWrapper[])ObjectArrayToDispatchWrapperArray((objs));

            // Save the drawings sheets to a PDF file
            boolstatus = swExportPDFData.SetSheets((int)swExportDataSheetsToExport_e.swExportData_ExportSpecifiedSheets, (dispWrapArr));
            swExportPDFData.ViewPdfAfterSaving = false;
            boolstatus = swModExt.SaveAs(pdfName, (int)swSaveAsVersion_e.swSaveAsCurrentVersion, (int)swSaveAsOptions_e.swSaveAsOptions_Silent, swExportPDFData, ref errors, ref warnings);
            
            if (!File.Exists(Path.Combine(Path.GetDirectoryName(assemblyPath), name + ".pdf"))) 
            { 
                File.Copy(pdfName, Path.Combine(Path.GetDirectoryName(assemblyPath), name + ".pdf")); 
            }
            
           // swModel.Save();
            swApp.CloseDoc(drwingFile);
            //swApp.ExitApp();
            //swApp = null;
        }



        /// <summary>
        /// 
        /// </summary>
        /// <param name="path"></param>
        /// <param name="baseInfo"></param>
        /// <param name="assmPath"></param>
        public void Traverser(string path, AssemblyInfo baseInfo, ISldWorks swApp)
        {
            if(baseInfo.ItemGroup.Equals("Hardware", StringComparison.CurrentCultureIgnoreCase) || baseInfo.ItemGroup.Equals("Bop", StringComparison.CurrentCultureIgnoreCase))
            {
                return;
            }

            Directory.CreateDirectory(Path.Combine(path, Path.GetFileNameWithoutExtension(baseInfo.path)));
            pdf(baseInfo, Path.Combine(path, Path.GetFileNameWithoutExtension(baseInfo.path)), baseInfo.path, swApp);

            foreach (var childInfo in baseInfo.childs)
            {
                if (childInfo.childs.Count == 0)
                {
                    if (childInfo.ItemGroup.Equals("Hardware", StringComparison.CurrentCultureIgnoreCase) || childInfo.ItemGroup.Equals("Bop", StringComparison.CurrentCultureIgnoreCase))
                    {
                        continue ;
                    }
                    pdf(childInfo, Path.Combine(path, Path.GetFileNameWithoutExtension(baseInfo.path)), childInfo.path, swApp);

                }
                else if (childInfo.childs.Count > 0)
                {
                    Traverser(Path.Combine(path, Path.GetFileNameWithoutExtension(baseInfo.path)), childInfo, swApp);
                }
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="baseInfo"></param>
        public void GetDirectory(AssemblyInfo baseInfo, ISldWorks swApp)
        {
            FolderBrowserDialog dlg = new FolderBrowserDialog();
            dlg.RootFolder = System.Environment.SpecialFolder.MyComputer;
            DialogResult dialog = dlg.ShowDialog();

            if (dialog.ToString() != "OK")
            {
                System.Windows.MessageBox.Show("Select Directory");
                return;
            }

            var path = dlg.SelectedPath;
            Traverser(path, baseInfo, swApp);
            System.Windows.MessageBox.Show("Pdf Generated Successfully");
        }



        /// <summary>
        /// 
        /// </summary>
        /// <param name="Objects"></param>
        /// <returns></returns>
        public DispatchWrapper[] ObjectArrayToDispatchWrapperArray(object[] Objects)
        {

            int ArraySize = 0;
            ArraySize = Objects.GetUpperBound(0);
            DispatchWrapper[] d = new DispatchWrapper[ArraySize + 1];
            int ArrayIndex = 0;
            for (ArrayIndex = 0; ArrayIndex <= ArraySize; ArrayIndex++)
            {
                d[ArrayIndex] = new DispatchWrapper(Objects[ArrayIndex]);
            }

            return d;
        }

        public SldWorks swApp;

    }
}

