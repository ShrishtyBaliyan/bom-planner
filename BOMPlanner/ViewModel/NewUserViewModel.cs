﻿using BOMPlanner.SQL;
using CadData;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;
using System.Windows.Input;

namespace BOMPlanner.ViewModel
{
    public class NewUserViewModel : INotifyPropertyChanged, ICloseWindow
    {

        /// <summary>
        /// 
        /// </summary>
        private List<string> grpBox = SQLQueries.GetGroups();
        public List<string> GrpBox
        {
            get => this.grpBox;
            set
            {
                this.grpBox = value;
                OnPropertyChanged("GrpBox");
            }
        }


        /// <summary>
        /// 
        /// </summary>
        private string selectedGrp;
        public string SelectedGrp
        {
            get => this.selectedGrp;
            set
            {
                this.selectedGrp = value;
                OnPropertyChanged("SelectedGrp");
            }
        }


        /// <summary>
        /// stores details of user ID
        /// </summary>
        private string userId;
        public string UserId
        {
            get => this.userId;
            set
            {
                this.userId = value;
                OnPropertyChanged("UserId");
            }
        }


        /// <summary>
        /// stores value of Password
        /// </summary>
        private string paswword;
        public string Password
        {
            get => this.paswword;
            set
            {
                this.paswword = value;
                OnPropertyChanged("Password");
            }
        }

        /// <summary>
        /// stores value of Password
        /// </summary>
        private string cnfrmPaswword;
        public string CnfrmPassword
        {
            get => this.cnfrmPaswword;
            set
            {
                this.cnfrmPaswword = value;
                OnPropertyChanged("CnfrmPassword");
            }
        }

        /// <summary>
        /// stores value of Name
        /// </summary>
        private string name;
        public string Name
        {
            get => this.name;
            set
            {
                this.name = value;
                OnPropertyChanged("Name");
            }
        }


        /// <summary>
        /// stores value of Email
        /// </summary>
        private string email;
        public string Email
        {
            get => this.email;
            set
            {
                this.email = value;
                OnPropertyChanged("Email");
            }
        }

        /// <summary>
        /// Property Change Event Handler
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string PropertyName)
        {
            if (this.PropertyChanged == null)
                return;
            this.PropertyChanged((object)this, new PropertyChangedEventArgs(PropertyName));
        }



        /// <summary>
        /// cmd for save the user info
        /// </summary>
        private ICommand save;
        public ICommand Save
        {
            get
            {
                if (this.save == null)
                    this.save = (ICommand)new ICommandInterface(new Action<object>(this.executeSave), new Func<object, bool>(this.canExecuteSave));
                return this.save;
            }
        }
        private bool canExecuteSave(object arg) => true || this.canExecuteSave(arg);
        private void executeSave(object parameter)
        {
            SaveInfo();
        }



        /// <summary>
        /// cmd for user Login
        /// </summary>
        private ICommand userLogin;
        public ICommand UserLogin
        {
            get
            {
                if (this.userLogin == null)
                    this.userLogin = (ICommand)new ICommandInterface(new Action<object>(this.executeUserLogin), new Func<object, bool>(this.canExecuteUserLogin));
                return this.userLogin;
            }
        }
        private bool canExecuteUserLogin(object arg) => true || this.canExecuteUserLogin(arg);
        private void executeUserLogin(object parameter)
        {
            CloseWindow();
        }



        /// <summary>
        /// function for save user info in database
        /// </summary>
        public void SaveInfo()
        {

            if (string.IsNullOrWhiteSpace(this.userId))
            {
                MessageBox.Show(@"User ID should not be empty", @"Error",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            else if (string.IsNullOrWhiteSpace(this.paswword))
            {
                MessageBox.Show(@"Password should not be empty", @"Error",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            else if (string.IsNullOrWhiteSpace(this.cnfrmPaswword)) 
            {
                MessageBox.Show(@"Confirm password should not be empty", @"Error",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            } 
            else if(string.IsNullOrWhiteSpace(this.name))
            {
                MessageBox.Show(@"Name should not be empty", @"Error",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            else if (string.IsNullOrWhiteSpace(this.selectedGrp))
            {
                MessageBox.Show(@"GroupName should not be empty", @"Error",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            else if (!this.paswword.Trim().Equals(this.cnfrmPaswword.Trim()))
            {
                MessageBox.Show(@"Password and cinform password should be equal", @"Error",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            SQLQueries.SaveUserInfo(this.userId, this.paswword, this.selectedGrp, this.name, this.email);
            CloseWindow();
        }

        /// <summary>
        /// func for closing window
        /// </summary>
        void CloseWindow()
        {
            Close?.Invoke();
        }

        public Action Close { get; set; }
    }

}
