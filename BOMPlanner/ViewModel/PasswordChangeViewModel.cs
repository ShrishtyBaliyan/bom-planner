﻿using BOMPlanner.SQL;
using CadData;
using System;
using System.ComponentModel;
using System.Windows.Forms;
using System.Windows.Input;

namespace BOMPlanner.ViewModel
{
    public class PasswordChangeViewModel : INotifyPropertyChanged, ICloseWindow
    {
        private string userId;



        public PasswordChangeViewModel(string userId)
        {
            this.userId = userId;
        }

        /// <summary>
        /// stores the details of NewPassword, entered by Admin
        /// </summary>
        private string newpassword;
        public string NewPassword
        {
            get => this.newpassword;
            set
            {
                this.newpassword = value;
                OnPropertyChanged("NewPassword");
            }
        }

        /// <summary>
        /// stores the details of ConfirmPassword, entered by Admin
        /// </summary>
        private string confirmpassword;
        public string ConfirmPassword
        {
            get => this.confirmpassword;
            set
            {
                this.confirmpassword = value;
                OnPropertyChanged("ConfirmPassword");
            }
        }


        /// <summary>
        /// cmd for 
        /// </summary>
        private ICommand passChange;
        public ICommand ChangePass
        {
            get
            {
                if (this.passChange == null)
                    this.passChange = (ICommand)new ICommandInterface(new Action<object>(this.executeChangePass), new Func<object, bool>(this.canExecuteChangePass));
                return this.passChange;
            }
        }
        private bool canExecuteChangePass(object arg) => true || this.canExecuteChangePass(arg);
        private void executeChangePass(object parameter)
        {
            if (string.IsNullOrWhiteSpace(this.newpassword))
            {
                MessageBox.Show(@"New Password should not be empty", @"Error",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (string.IsNullOrWhiteSpace(this.confirmpassword))
            {
                MessageBox.Show(@"Confirm password should not be empty", @"Error",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (!this.newpassword.Trim().Equals(this.confirmpassword.Trim()))
            {
                MessageBox.Show(@"Password and confirm password should be equal", @"Error",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                SQLQueries.UpdatePassword(this.confirmpassword, this.userId);
                MessageBox.Show(@"Password Updated Successfully");
                this.CloseWindow();
            }
        }


        /// <summary>
        /// Property Change Event Handler
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string PropertyName)
        {
            if (this.PropertyChanged == null)
                return;
            this.PropertyChanged((object)this, new PropertyChangedEventArgs(PropertyName));
        }

        /// <summary>
        /// func for closing window
        /// </summary>
        void CloseWindow()
        {
            Close?.Invoke();
        }

        public Action Close { get; set; }
    }
}
