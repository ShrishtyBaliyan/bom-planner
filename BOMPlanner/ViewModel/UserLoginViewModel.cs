﻿using BOMPlanner.SQL;
using BOMPlanner.View;
using CadData;
using System;
using System.ComponentModel;
using System.Windows.Input;

namespace BOMPlanner.ViewModel
{
    public class UserLoginViewModel : INotifyPropertyChanged, ICloseWindow
    {
        private bool loggedIn;
        

        public UserLoginViewModel()
        {
        }


        /// <summary>
        /// stores details of user ID
        /// </summary>
        private string userId;
        public string UserId
        {
            get
            {
                return this.userId;
            }
            set
            {
                this.userId = value;
                OnPropertyChanged("UserId");
            }
        }


        /// <summary>
        /// stores the details of password, entered by user
        /// </summary>
        private string password;
        public string Password
        {
            get => this.password;
            set
            {
                this.password = value;
                OnPropertyChanged("Password");
            }
        }


        /// <summary>
        /// Property Change Event Handler
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string PropertyName)
        {
            if (this.PropertyChanged == null)
                return;
            this.PropertyChanged((object)this, new PropertyChangedEventArgs(PropertyName));
        }




        /// <summary>
        /// cmd for login the page
        /// </summary>
        private ICommand login;

        public ICommand Login
        {
            get
            {
                if (this.login == null)
                    this.login = (ICommand)new ICommandInterface(new Action<object>(this.executeLogin), new Func<object, bool>(this.canExecuteLogin));
                return this.login;
            }
        }
        private bool canExecuteLogin(object arg) => true || this.canExecuteLogin(arg);
        private void executeLogin(object parameter)
        {
            if (SQLQueries.CheckUserInfo(this.userId, this.password))
            {
                SQLQueries.SaveLoggedIn(this.userId);
                CloseWindow();
            }
        }


        /// <summary>
        /// cmd for adding new user
        /// </summary>
        private ICommand newUser;
        public ICommand NewUser
        {
            get
            {
                if (this.newUser == null)
                    this.newUser = (ICommand)new ICommandInterface(new Action<object>(this.executeNewUser), new Func<object, bool>(this.canExecuteNewUser));
                return this.newUser;
            }
        }
        private bool canExecuteNewUser(object arg) => true || this.canExecuteNewUser(arg);
        private void executeNewUser(object parameter)
        {
            AddNewUser addNewUser = new AddNewUser();
            NewUserViewModel newUserViewModel = new NewUserViewModel();
            addNewUser.DataContext = newUserViewModel;
            addNewUser.ShowDialog();
        }


        /// <summary>
        /// cmd for 
        /// </summary>
        private ICommand changePass;
        public ICommand ChangePass
        {
            get
            {
                if (this.changePass == null)
                    this.changePass = (ICommand)new ICommandInterface(new Action<object>(this.executeChangePass), new Func<object, bool>(this.canExecuteChangePass));
                return this.changePass;
            }
        }
        private bool canExecuteChangePass(object arg) => true || this.canExecuteChangePass(arg);
        private void executeChangePass(object parameter)
        {
            if (SQLQueries.CheckUserInfo(this.userId, this.password))
            {
                var admin = SQLQueries.CheckAdmin();
                if (this.userId == admin)
                {
                    AdminView adminView = new AdminView();
                    AdminViewModel adminViewModel = new AdminViewModel();
                    adminView.DataContext = adminViewModel;
                    adminView.ShowDialog();
                }
                else
                {
                    PasswordChangeView passwordChangeView = new PasswordChangeView();
                    PasswordChangeViewModel passwordChangeViewModel = new PasswordChangeViewModel(this.userId);
                    passwordChangeView.DataContext = passwordChangeViewModel;
                    passwordChangeView.ShowDialog();
                }
            }
        }



        /// <summary>
        /// func for closing window
        /// </summary>
        void CloseWindow()
        {
            Close?.Invoke();
        }

        public Action Close { get; set; }
    }


}
