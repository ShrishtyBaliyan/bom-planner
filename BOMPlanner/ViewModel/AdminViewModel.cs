﻿using BOMPlanner.SQL;
using CadData;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;
using System.Windows.Input;

namespace BOMPlanner.ViewModel
{
    public class AdminViewModel : INotifyPropertyChanged, ICloseWindow
    {

        /// <summary>
        /// 
        /// </summary>
        private List<string> combobox = SQLQueries.GetUsers();
        public List<string> Combobox
        {
            get => this.combobox;
            set
            {
                this.combobox = value;
                OnPropertyChanged("Combobox");
            }
        }


        /// <summary>
        /// 
        /// </summary>
        private string slctdCmbBox;
        public string SlctdCmbBox
        {
            get => this.slctdCmbBox;
            set
            {
                this.slctdCmbBox = value;
                OnPropertyChanged("SlctdCmbBox");
            }
        }


        /// <summary>
        /// stores the details of NewPassword, entered by Admin
        /// </summary>
        private string newpassword;
        public string NewPassword
        {
            get => this.newpassword;
            set
            {
                this.newpassword = value;
                OnPropertyChanged("NewPassword");
            }
        }

        /// <summary>
        /// stores the details of ConfirmPassword, entered by Admin
        /// </summary>
        private string confirmpassword;
        public string ConfirmPassword
        {
            get => this.confirmpassword;
            set
            {
                this.confirmpassword = value;
                OnPropertyChanged("ConfirmPassword");
            }
        }


        /// <summary>
        /// cmd for 
        /// </summary>
        private ICommand changePass;
        public ICommand ChangePass
        {
            get
            {
                if (this.changePass == null)
                    this.changePass = (ICommand)new ICommandInterface(new Action<object>(this.executeChangePass), new Func<object, bool>(this.canExecuteChangePass));
                return this.changePass;
            }
        }
        private bool canExecuteChangePass(object arg) => true || this.canExecuteChangePass(arg);
        private void executeChangePass(object parameter)
        {
            if (string.IsNullOrWhiteSpace(this.slctdCmbBox))
            {
                MessageBox.Show(@"Select User", @"Error",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            else if (string.IsNullOrWhiteSpace(this.newpassword))
            {
                MessageBox.Show(@"New Password should not be empty", @"Error",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (string.IsNullOrWhiteSpace(this.confirmpassword))
            {
                MessageBox.Show(@"Confirm password should not be empty", @"Error",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (!this.newpassword.Trim().Equals(this.confirmpassword.Trim()))
            {
                MessageBox.Show(@"Password and confirm password should be equal", @"Error",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                var password = SaltedHelper.ComputeHash(this.confirmpassword.Trim(), "SHA512", null);
                SQLQueries.UpdatePassword(password, this.slctdCmbBox);
                MessageBox.Show(@"Password Updated Successfully");
                this.CloseWindow();
            }
        }


        /// <summary>
        /// Property Change Event Handler
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string PropertyName)
        {
            if (this.PropertyChanged == null)
                return;
            this.PropertyChanged((object)this, new PropertyChangedEventArgs(PropertyName));
        }

        /// <summary>
        /// func for closing window
        /// </summary>
        void CloseWindow()
        {
            Close?.Invoke();
        }

        public Action Close { get; set; }
    }
}
