﻿using CadData;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Windows.Forms;

namespace BOMPlanner.SQL
{
    public static class SQLQueries
    {
        private static SqlConnection con;
        private static SqlDataAdapter Adapter;
        private static DataTable dt;

        public static string userId;

        /// <summary>
        /// sql query for check login info
        /// </summary>
        /// <param name="userID"></param>
        /// <param name="password"></param>
        public static bool CheckUserInfo(string userID, string password)
        {
            if (string.IsNullOrWhiteSpace(userID) || string.IsNullOrWhiteSpace(password))
            {
                MessageBox.Show(@"Both fields should be entered", @"Error", MessageBoxButtons.OK,
                MessageBoxIcon.Error);
                return false;
            }

            string strSelect = @"SELECT [Id],[UserId],[Password],[Name],[Email]FROM [dbo].[UserLoginInfo] where UserId=@UserName";

            using (con = new SqlConnection(SQLConnection.connection_string))
            {
                using (SqlCommand cmd = new SqlCommand(strSelect, con))
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@UserName", userID);
                    using (Adapter = new SqlDataAdapter(cmd))
                    {
                        using (dt = new DataTable())
                        {
                            Adapter.Fill(dt);
                        }
                    }

                    if (dt.Rows.Count == 0)
                    {
                        MessageBox.Show(@"User not exists", @"Error", MessageBoxButtons.OK,
                        MessageBoxIcon.Error);
                        return false;
                    }
                } // using
            } // using

            DataRow row = dt.Rows[0];
            var id = Convert.ToInt32(row["Id"]);
            var loginId = row["UserId"].ToString();
            var savedpassWord = row["Password"].ToString();
            var name = row["Name"].ToString();

            if (!SaltedHelper.VerifyHash(password, "SHA512",
                savedpassWord))
            {
                MessageBox.Show(@"Incorrect user id or password", @"Error", MessageBoxButtons.OK,
                   MessageBoxIcon.Error);
                return false;
            }
            return true;
        }


        /// <summary>
        /// sql query for save user info in database
        /// </summary>
        /// <param name="userID"></param>
        /// <param name="txtPassword"></param>
        /// <param name="userName"></param>
        /// <param name="email"></param>
        public static void SaveUserInfo(string userID, string txtPassword, string GroupName, string userName, string email)
        {
            var password = SaltedHelper.ComputeHash(txtPassword.Trim(), "SHA512", null);
            string strInsert = @"INSERT INTO [dbo].[UserLoginInfo]([UserId],[Password],[Group_id],[Name],[Email]) VALUES ('" + userID + "','" + password + "',(select[Id] from[dbo].[Groups] where GroupName = '"+ GroupName +"'),'" + userName + "','" + email + "')";
            

            string strSelect = @"SELECT [Id],[UserId],[Password],[Name],[Email]FROM [dbo].[UserLoginInfo]where UserId=@UserId or Name=@Name";

            using (con = new SqlConnection(SQLConnection.connection_string))
            {
                using (var cmd = new SqlCommand(strSelect, con))
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@UserId", userID.Trim());
                    cmd.Parameters.AddWithValue("@Name", userName.Trim());
                    using (Adapter = new SqlDataAdapter(cmd))
                    {
                        using (dt = new DataTable())
                        {
                            Adapter.Fill(dt);
                        }
                    }

                    if (dt.Rows.Count > 0)
                    {
                        MessageBox.Show(@"User already exists", @"Error", MessageBoxButtons.OK,
                        MessageBoxIcon.Error);
                        return;
                    }
                } // using
            } // using

            try
            {
                using (con = new SqlConnection(SQLConnection.connection_string))
                {
                    using (SqlCommand cmd = new SqlCommand(strInsert, con))
                    {
                        cmd.CommandType = CommandType.Text;

                        con.Open();
                        cmd.ExecuteNonQuery();
                        con.Close();
                        MessageBox.Show(@"New user added successfully", @"Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, @"Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        /// <summary>
        /// save the id of user who has logged in
        /// </summary>
        /// <param name="userId"></param>
        public static void SaveLoggedIn(string userName)
        {
            userId = userName;
        }


        /// <summary>
        /// save the id of user who has logged in
        /// </summary>
        /// <param name="userId"></param>
        public static string UserInfo()
        {
           return userId;
        }




        /// <summary>
        /// save the record of user,who is uploading the file with date & time
        /// </summary>
        /// <param name="UserInfo"></param>
        public static void FileUserRecord(string UserInfo, string pname)
        {
            string saveRecord = @"insert into [dbo].[User_FileUpload_Record] values ((select Id from [dbo].[UserLoginInfo] where UserId = '" + UserInfo + "')," +
                " (select id from [dbo].[Assembly_info] where part_number='" + pname + "' ), '"+ DateTime.Now.ToString() +"')";

            using (con = new SqlConnection(SQLConnection.connection_string))
            {
                using (SqlCommand cmd = new SqlCommand(saveRecord, con))
                {
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();
                }
            }
        }



        /// <summary>
        /// save the record of Project with Assembly
        /// </summary>
        /// <param name="UserInfo"></param>
        public static void SaveProjectRecord(string pname, string path)
        {
            var getDate = @"select [design_date] from [dbo].[Asm_Version_Info] where AsmInfo_id = (select [id] from [dbo].[Assembly_info] where part_number = '" + pname + "') ";
            using (con = new SqlConnection(SQLConnection.connection_string))
            {
                using (SqlCommand cmd = new SqlCommand(getDate, con))
                {
                    cmd.CommandType = CommandType.Text;
                    using (Adapter = new SqlDataAdapter(cmd))
                    {
                        using (dt = new DataTable())
                        {
                            Adapter.Fill(dt);
                        }
                    }
                } // using
            } // using


            var Date_Time = dt.Rows[dt.Rows.Count-1][0].ToString();

            var date = Date_Time.Split('-', '/');
            var projectNum = Path.GetFileName(path).Split('-')[0];
            var Year = date[2];
            var month = date[1];

            var clientname = AsmClient(pname);
            string saveRecord = @"IF EXISTS ( select [id] from [dbo].[Projects] where Project_Num = '" + projectNum + "' and Assembly_id = (select [id] from [dbo].[Assembly_info] where part_number='" + pname + "' )" +
                " and ClientName in (select [client_name] from [dbo].[Asm_Version_Info] where AsmInfo_id = (select [id] from [dbo].[Assembly_info] where part_number='" + pname + "'))) " +
                "BEGIN " +
                 "select [client_name] from [dbo].[Asm_Version_Info] where AsmInfo_id in (select [id] from [dbo].[Assembly_info] where part_number='" + pname + "')" +
                "End" +
                " else" +
                " insert into [dbo].[Projects] values ('" + projectNum + "' , (select [id] from [dbo].[Assembly_info] where part_number='" + pname + "' ), '" + clientname + "', '" + Year + "', '" + month + "')";

            using (con = new SqlConnection(SQLConnection.connection_string))
            {
                using (SqlCommand cmd = new SqlCommand(saveRecord, con))
                {
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();
                }
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="pname"></param>
        /// <returns></returns>
        public static string AsmClient(string pname)
        {
            string select = @"select [client_name] from [dbo].[Asm_Version_Info] where AsmInfo_id = (select id from [dbo].[Assembly_info] where part_number='" + pname + "')";
            using (con = new SqlConnection(SQLConnection.connection_string))
            {
                using (SqlCommand cmd = new SqlCommand(select, con))
                {
                    cmd.CommandType = CommandType.Text;
                    using (Adapter = new SqlDataAdapter(cmd))
                    {
                        using (dt = new DataTable())
                        {
                            Adapter.Fill(dt);
                        }
                    }

                    if (dt.Rows.Count == 0)
                    {
                        return null;
                    }
                    else
                    {
                        return dt.Rows[dt.Rows.Count-1][0].ToString();
                    }
                } // using
            } // using
        }


        /// <summary>
        /// find the list of admins
        /// </summary>
        /// <returns></returns>
        public static string CheckAdmin()
        {
            string Admin = "Admin";
            string checkAdmin = @"select [UserId] from [dbo].[UserLoginInfo] where Group_id = (select [Id] from [dbo].[Groups] where GroupName = '"+ Admin +"')";
            using (con = new SqlConnection(SQLConnection.connection_string))
            {
                using (SqlCommand cmd = new SqlCommand(checkAdmin, con))
                {
                    cmd.CommandType = CommandType.Text;
                    using (Adapter = new SqlDataAdapter(cmd))
                    {
                        using (dt = new DataTable())
                        {
                            Adapter.Fill(dt);
                        }
                    }

                    if (dt.Rows.Count == 0)
                    {
                        return null;
                    }
                    else
                    {
                        return dt.Rows[0].ToString();
                    }
                } // using
            } // using
        }



        /// <summary>
        /// Find the list of users other than Admin
        /// </summary>
        /// <returns></returns>
        public static List<string> GetUsers()
        {
            List<string> users = new List<string>();
            string checkUser = @"select [UserId] from [dbo].[UserLoginInfo]";
            using (con = new SqlConnection(SQLConnection.connection_string))
            {
                using (SqlCommand cmd = new SqlCommand(checkUser, con))
                {
                    cmd.CommandType = CommandType.Text;
                    using (Adapter = new SqlDataAdapter(cmd))
                    {
                        using (dt = new DataTable())
                        {
                            Adapter.Fill(dt);
                        }
                    }

                    foreach (DataRow user in dt.Rows)
                    {
                        users.Add(user[0].ToString());
                    }
                    return users;
                } // using
            } // using
        }


        /// <summary>
        /// find the list of total groups of users
        /// </summary>
        /// <returns></returns>
        public static List<string> GetGroups()
        {
            List<string> users = new List<string>();
            string checkUser = @"select [GroupName] from [dbo].[Groups]";
            using (con = new SqlConnection(SQLConnection.connection_string))
            {
                using (SqlCommand cmd = new SqlCommand(checkUser, con))
                {
                    cmd.CommandType = CommandType.Text;
                    using (Adapter = new SqlDataAdapter(cmd))
                    {
                        using (dt = new DataTable())
                        {
                            Adapter.Fill(dt);
                        }
                    }

                    foreach (DataRow user in dt.Rows)
                    {
                        users.Add(user[0].ToString());
                    }
                    return users;
                } // using
            } // using
        }


        /// <summary>
        /// for updating password
        /// </summary>
        /// <param name="NewPassword"></param>
        /// <param name="selectedUser"></param>
        public static void UpdatePassword(string NewPassword, string selectedUser)
        {
            string updatePass = @"Update [dbo].[UserLoginInfo] set Password = '" + NewPassword + "' where UserId = '" + selectedUser + "' ";

            using (con = new SqlConnection(SQLConnection.connection_string))
            {
                using (SqlCommand cmd = new SqlCommand(updatePass, con))
                {
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();
                }
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static string getUserGrpId(string userId)
        {
            string grpId = @"select [GroupName] from [dbo].[Groups] where Id = (select [Group_id] from [dbo].[UserLoginInfo] where UserId = '" + userId + "') ";
            using (con = new SqlConnection(SQLConnection.connection_string))
            {
                using (SqlCommand cmd = new SqlCommand(grpId, con))
                {
                    cmd.CommandType = CommandType.Text;
                    using (Adapter = new SqlDataAdapter(cmd))
                    {
                        using (dt = new DataTable())
                        {
                            Adapter.Fill(dt);
                        }
                    }


                    if (dt.Rows.Count == 0)
                    {
                        return null;
                    }
                    else
                    {
                        return dt.Rows[0][0].ToString();
                    }
                } // using
            } // using
        }



        /// <summary>
        /// 
        /// </summary>
        /// <param name="asm_id"></param>
        /// <returns></returns>
        public static DataTable GetLatestVersion(int asm_id)
        {
            string getVer = @"select [id],[Version],[IsStandard] from [dbo].[Versions] where Asm_id = '" + asm_id + "' ";

            using (SqlConnection con = new SqlConnection(SQLConnection.connection_string))
            {
                DataTable dt = new DataTable();
                using (SqlCommand cmd = new SqlCommand(getVer, con))
                {
                    cmd.CommandType = CommandType.Text;
                    using (SqlDataAdapter Adapter = new SqlDataAdapter(cmd))
                    {
                        using (dt = new DataTable())
                        {
                            Adapter.Fill(dt);
                        }
                    }

                    if (dt.Rows.Count == 0)
                    {
                        return null;
                    }
                    else
                    {
                        return dt;
                    }
                } // using
            } // using
        }




        /// <summary>
        /// 
        /// </summary>
        /// <param name="pName"></param>
        /// <returns></returns>
        public static int GetAsm_id(string pName)
        {
            string sqlSelect = @"SELECT [id] FROM [dbo].[Assembly_info] where  part_number = '" + pName + "' ";
            using (SqlConnection con = new SqlConnection(SQLConnection.connection_string))
            {
                DataTable dt = new DataTable();
                using (SqlCommand cmd = new SqlCommand(sqlSelect, con))
                {
                    cmd.CommandType = CommandType.Text;
                    using (SqlDataAdapter Adapter = new SqlDataAdapter(cmd))
                    {
                        using (dt = new DataTable())
                        {
                            Adapter.Fill(dt);
                        }
                    }

                    if (dt.Rows.Count == 0)
                    {
                        return 0;
                    }
                    else
                    {
                        return Convert.ToInt32(dt.Rows[0][0]);
                    }
                } // using
            } // using
        }



    }
}