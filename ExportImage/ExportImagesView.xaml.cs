﻿using CadData;
using System.Windows;

namespace ExportImage
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class ExportImagesView : Window
    {
        public ExportImagesView()
        {
            InitializeComponent();
            Loaded += ComparisionView2_Loaded;
        }

        private void ComparisionView2_Loaded(object sender, RoutedEventArgs e)
        {
            if (DataContext is ICloseWindow vm)
            {
                vm.Close += () =>
                {
                    this.Close();
                };
            }
        }
    }
}
