﻿using CadData;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace ExportImage.ViewModel
{
    public class ExportImagesViewModel : INotifyPropertyChanged, ICloseWindow
    {
        /// <summary>
        /// stores true & false value for export images
        /// </summary>
        private bool cbExportImages = Convert.ToBoolean(SQLConnection.export_images);
        public bool CbExportImages
        {
            get => this.cbExportImages;
            set
            {
                BOMSettings_Load();
                this.cbExportImages = value;
                this.OnPropertyChanged(nameof(CbExportImages));
            }
        }


        /// <summary>
        /// Property Changed Event Handler
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string PropertyName)
        {
            if (this.PropertyChanged == null)
                return;
            this.PropertyChanged((object)this, new PropertyChangedEventArgs(PropertyName));
        }


        /// <summary>
        /// for getting info for export images(true/false)
        /// </summary>
        public void BOMSettings_Load()
        {
            PropertyReader.ReadBOMSettings();
            SQLConnection image = new SQLConnection();
            image.SQLConnectionExprtImg("True", true);
            this.cbExportImages = Convert.ToBoolean(SQLConnection.export_images);
        }


        /// <summary>
        /// save selected value for export images
        /// </summary>
        private ICommand saveCommand;
        public ICommand SaveCommand
        {
            get
            {
                if (this.saveCommand == null)
                    this.saveCommand = (ICommand)new ICommandInterface(new Action<object>(this.executeSave), new Func<object, bool>(this.canExecuteSave));
                return this.saveCommand;
            }
        }

        private bool canExecuteSave(object arg) => true || this.canExecuteSave(arg);

        private void executeSave(object parameter)
        {
            SQLConnection image = new SQLConnection();
            image.SQLConnectionExprtImg(this.cbExportImages.ToString(), true);
            CloseWindow();
        }



        /// <summary>
        /// cmd for closing window
        /// </summary>
        private ICommand closeCommand;
        public ICommand CloseCommand
        {
            get
            {
                if (this.closeCommand == null)
                    this.closeCommand = (ICommand)new ICommandInterface(new Action<object>(this.executeClose), new Func<object, bool>(this.canExecuteClose));
                return this.closeCommand;
            }
        }
        private bool canExecuteClose(object arg) => true || this.canExecuteClose(arg);

        private void executeClose(object parameter)
        {
            CloseWindow();
        }



        /// <summary>
        /// func for closing window
        /// </summary>
        void CloseWindow()
        {
            Close?.Invoke();
        }

        public Action Close { get; set; }

    }

}
